<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pesan extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('M_pesan');
		$this->load->helper('form','url','number');
		$this->load->library('form_validation');
        $x = $this->session->userdata('username');
    if(empty($x)) {
            $this->session->set_flashdata('isLogin',false);
            redirect('home');
        }
    }  
	public function index()
	{
		$x['data'] 		= $this->M_pesan->getAll();		
		$this->load->view('layout/header');
		$this->load->view('layout/sidebar');
		$this->load->view('v_pesan');
		$this->load->view('layout/footer');
	}
	public function ajax_list()
    {
        $list = $this->M_pesan->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $pesan) {
            $no++;
            $row = array();
            $row[] = '<center>'.$no.'</center>';
            $row[] = $pesan->pesan_waktu;
            $row[] = $pesan->pesan_ip;
            $row[] = $pesan->pesan_alamat;
            $row[] = $pesan->pesan_nama;
            $row[] = $pesan->pesan_email;
            $row[] = $pesan->pesan_subject;
            $row[] = $pesan->pesan_isi;
            $row[] = '<center>'.'<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('."'".$pesan->pesan_id."'".')"><i class="glyphicon glyphicon-trash"></i></a>'.'</center>';
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->M_pesan->count_all(),
                        "recordsFiltered" => $this->M_pesan->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
    public function ajax_delete($id)
    {
        $this->M_pesan->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }
	function input(){
		if(isset($_POST['btnsimpan']))
		{	
									
			$pesan_nama   	= $this->input->post('pesan_nama'); 
			$pesan_ip   	= $this->input->post('pesan_ip');
			$pesan_alamat   = $this->input->post('pesan_alamat'); 			
			$pesan_judul    = $this->input->post('pesan_judul');
			$pesan_isi 		= $this->input->post('pesan_isi');
			$pesan_waktu 	= $this->input->post('pesan_waktu');
			

			$data = array(

				'pesan_nama'    => $pesan_nama,
				'pesan_ip'      => $pesan_ip,
				'pesan_alamat'  => $pesan_alamat,
				'pesan_judul'   => $pesan_judul,
				'pesan_isi'   	=> $pesan_isi,
				'pesan_waktu'   => $pesan_waktu
			);
			
			$this->M_pesan->insert($data);
			redirect(site_url('pesan'));
			/*var_dump($data);*/
		}
		else {
			redirect(site_url('pesan'));

		}
	}
	function edit(){
		if(isset($_POST['btnedit'])){

			$pesan_nama   	= $this->input->post('pesan_nama'); 
			$pesan_ip   	= $this->input->post('pesan_ip');
			$pesan_alamat   = $this->input->post('pesan_alamat'); 			
			$pesan_judul    = $this->input->post('pesan_judul');
			$pesan_isi 		= $this->input->post('pesan_isi');
			$pesan_waktu 	= $this->input->post('pesan_waktu');

			$data = array(
				'pesan_nama'    => $pesan_nama,
				'pesan_ip'      => $pesan_ip,
				'pesan_alamat'  => $pesan_alamat,
				'pesan_judul'   => $pesan_judul,
				'pesan_isi'   	=> $pesan_isi,
				'pesan_waktu'   => $pesan_waktu
			);
			
			$id = $this->input->post('pesan_id');
			$this->M_pesan->edit($data,$id);
			redirect(site_url('pesan'));
			/*var_dump($data);*/
			

		}else {
			redirect(site_url('pesan'));
		}
	}

	 function delete(){
        if(isset($_POST['btndelete'])){
            $id = $this->input->post('pesan_id');
            $this->M_pesan->delete($id);
            redirect(site_url('pesan'));
        }else {
            redirect(site_url('pesan'));
        }
    }

   
}
