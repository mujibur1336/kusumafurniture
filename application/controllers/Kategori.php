<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('M_kategori');
		$this->load->helper('form','url','number');
		$this->load->library('form_validation');
		$x = $this->session->userdata('username');
    if(empty($x)) {
            $this->session->set_flashdata('isLogin',false);
            redirect('home');
        }
   }  
	public function index()
	{
		$x['data'] = $this->M_kategori->getAll();
		$this->session->set_flashdata("pesan1", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"mdi mdi-information\"></i> data kategori tidak bisa dihapus</div>");
		$this->load->view('layout/header');
		$this->load->view('layout/sidebar');
		$this->load->view('v_kategori', $x);
		$this->load->view('layout/footer');
	}
	function input(){
		if(isset($_POST['btnsimpan']))
		{	

			$kategori_nama   = $this->input->post('kategori_nama'); 
			$kategori_seo    = $this->input->post('kategori_seo'); 			

			$data = array(
				'kategori_nama'     => $kategori_nama,
				'kategori_seo'      => str_replace(' ', '-', strtolower($kategori_nama))
			);

			$this->M_kategori->insert($data);
			redirect(site_url('Kategori'));
		}
		else {
			redirect(site_url('Kategori'));

		}
	}
	function edit(){
		if(isset($_POST['btnedit'])){


			$kategori_nama   = $this->input->post('kategori_nama'); 
			$kategori_seo    = $this->input->post('kategori_seo'); 			
			

			$data = array(
				'kategori_nama'     => $kategori_nama,
				'kategori_seo'      => str_replace(' ', '-', strtolower($kategori_nama))
			);

			$id = $this->input->post('kategori_id');
			$this->M_kategori->edit($data,$id);
			redirect(site_url('Kategori'));
			

		}else {
			redirect(site_url('Kategori'));
		}
	}

	    function delete(){
        if(isset($_POST['btndelete'])){
            $id = $this->input->post('kategori_id');
            $this->M_kategori->delete($id);
            redirect(site_url('Kategori'));
        }else {
            redirect(site_url('Kategori'));
        }
    }

    function profil()
    {
    	$x['data'] = $this->M_kategori->profil();
		$this->load->view('layout/header');
		$this->load->view('layout/sidebar');
		$this->load->view('v_profil', $x);
		$this->load->view('layout/footer');
    }

    function profil_edit()
    {
    	if(isset($_POST['btnedit'])){


			$profil_nama   			= $this->input->post('profil_nama'); 
			$profil_alamat    		= $this->input->post('profil_alamat'); 			
			$profil_alamat_pabrik   = $this->input->post('profil_alamat_pabrik');
			$profil_telp		    = $this->input->post('profil_telp'); 
			$profil_web			    = $this->input->post('profil_web'); 			
			$profil_material	    = $this->input->post('profil_material');
			$profil_grade		    = $this->input->post('profil_grade'); 
			$profil_sertifikat	    = $this->input->post('profil_sertifikat'); 
			$profil_kapasitas	    = $this->input->post('profil_kapasitas'); 
			$profil_packing		    = $this->input->post('profil_packing');
			$profil_faq			    = $this->input->post('profil_faq');
			$profil_alamat_register = $this->input->post('profil_alamat_register');
			

			$data = array(
				'profil_nama'     		=> $profil_nama,
				'profil_alamat'     	=> $profil_alamat,
				'profil_alamat_pabrik'  => $profil_alamat_pabrik,
				'profil_telp'     		=> $profil_telp,
				'profil_web'     		=> $profil_web,
				'profil_material'     	=> $profil_material,
				'profil_grade'     		=> $profil_grade,
				'profil_sertifikat'     => $profil_sertifikat,
				'profil_kapasitas'     	=> $profil_kapasitas,
				'profil_alamat_register'  => $profil_alamat_register,
				'profil_faq'     	=> $profil_faq,
				'profil_packing'     	=> $profil_packing
				
			);

			$id = $this->input->post('profil_id');
			$this->M_kategori->profil_edit($data,$id);
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\" id=\"alert\"><i class=\"mdi mdi-information\"></i> Perubahan data berhasil</div>");
			redirect(site_url('Kategori/profil'));
			

		}else {
			redirect(site_url('Kategori/profil'));
		}
    }

   
}
