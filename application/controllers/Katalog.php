<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Katalog extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('M_katalog');
		$this->load->helper('form','url','number');
		$this->load->library('form_validation');
        $x = $this->session->userdata('username');
    if(empty($x)) {
            $this->session->set_flashdata('isLogin',false);
            redirect('home');
        }
    }  
	public function index()
	{
		$x['data'] = $this->M_katalog->getAll();
		$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"mdi mdi-information\"></i> data katalog tidak bisa dihapus</div>");
		$this->load->view('layout/header');
		$this->load->view('layout/sidebar');
		$this->load->view('v_katalog', $x);
		$this->load->view('layout/footer');
	}
	function input(){
		if(isset($_POST['btnsimpan']))
		{	

			$katalog_nama   = $this->input->post('katalog_nama'); 
			$katalog_seo    = $this->input->post('katalog_seo');
			$katalog_quote    = $this->input->post('katalog_quote'); 			

			$data = array(
				'katalog_nama'     => $katalog_nama,
				'katalog_seo'      => str_replace(' ', '-', strtolower($katalog_nama)),
				'katalog_quote'    => $katalog_quote
			);

			$this->M_katalog->insert($data);
			redirect(site_url('katalog'));
		}
		else {
			redirect(site_url('katalog'));

		}
	}
	function edit(){
		if(isset($_POST['btnedit'])){


			$katalog_nama   = $this->input->post('katalog_nama'); 
			$katalog_seo    = $this->input->post('katalog_seo'); 			
			$katalog_quote  = $this->input->post('katalog_quote'); 			

			$data = array(
				'katalog_nama'     => $katalog_nama,
				'katalog_seo'      => str_replace(' ', '-', strtolower($katalog_nama)),
				'katalog_quote'    => $katalog_quote
			);

			$id = $this->input->post('katalog_id');
			$this->M_katalog->edit($data,$id);
			redirect(site_url('katalog'));
			

		}else {
			redirect(site_url('katalog'));
		}
	}

	    function delete(){
        if(isset($_POST['btndelete'])){
            $id = $this->input->post('katalog_id');
            $this->M_katalog->delete($id);
            redirect(site_url('katalog'));
        }else {
            redirect(site_url('katalog'));
        }
    }

   
}
