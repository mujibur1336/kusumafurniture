<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('M_user');
		$this->load->helper('form','url','number');
		$this->load->library('form_validation');
		$x = $this->session->userdata('username');
    if(empty($x)) {
            $this->session->set_flashdata('isLogin',false);
            redirect('home');
        }
    }  
	public function index()
	{
		$x['data'] = $this->M_user->getAll();
		$this->load->view('layout/header');
		$this->load->view('layout/sidebar');
		$this->load->view('v_user', $x);
		$this->load->view('layout/footer');
	}
	function input(){
		if(isset($_POST['btnsimpan']))
		{	

			$user_nama   = $this->input->post('user_nama'); 
			$username    = $this->input->post('username'); 
			$password	 = md5($this->input->post('password')); 
			$level    	 = $this->input->post('level');
			

			$data = array(
				'user_nama'     => $user_nama,
				'username'      => $username,
				'password'		=> $password,
				'level'			=> $level
			);

			$this->M_user->insert($data);
			redirect(site_url('user'));
		}
		else {
			redirect(site_url('user'));

		}
	}
	function edit(){
		if(isset($_POST['btnedit'])){


			$user_nama   = $this->input->post('user_nama'); 
			$username    = $this->input->post('username'); 
			$password	 = md5($this->input->post('password')); 
			$level    	 = $this->input->post('level');
			

			$data = array(
				'user_nama'     => $user_nama,
				'username'      => $username,
				'password'		=> $password,
				'level'			=> $level
			);

			$id = $this->input->post('user_id');
			$this->M_user->edit($data,$id);
			redirect(site_url('user'));
		}else {
			redirect(site_url('user'));
		}
	}

	    function delete(){
        if(isset($_POST['btndelete'])){
            $id = $this->input->post('user_id');
            $this->M_user->delete($id);
            redirect(site_url('user'));
        }else {
            redirect(site_url('user'));
        }
    }

   
}
