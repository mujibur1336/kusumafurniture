<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
 public function __construct()
 {
  parent::__construct();    
  $this->load->model('M_login');
  $this->load->library(array('form_validation','session'));    
  $this->load->database();    
  $this->load->helper('url');    
}

function index(){
 $session = $this->session->userdata('isLogin');
 if($session == FALSE)
 {
  $this->load->view('layout/header');
  redirect('admin-cms');
}else
{
  redirect('login');
}
}

function cek_login(){
  $this->form_validation->set_rules('username', 'Username', 'required');
  $this->form_validation->set_rules('password', 'Password', 'required');
  $this->form_validation->set_error_delimiters('<span class="error">', '</span>');

  if($this->form_validation->run()==FALSE)
  {
    $this->load->view('v_login');
    /*$this->load->view('layout/footer');*/
  }else
  {
   $username = $this->input->post('username');
   $password = $this->input->post('password');

   $cek = $this->M_login->get_user($username, $password);
   // var_dump($cek->num_rows());
   if($cek->num_rows() <> 0){
    $data = $cek->row();     
    $id = $data->user_id ;     
    $this->session->set_userdata('isLogin', TRUE);
    $this->session->set_userdata('username',$username);          
    $this->session->set_userdata('user_id', $id);
    if ($data->level == 1) {
      $this->session->set_userdata('level','1');
      redirect('Dashboard');
    }
    elseif ($data->level==2) {
      $this->session->set_userdata('level','2');
      redirect('dashboard_peserta');    
    }
    //elseif ($data->level_user==3) {
    //   $this->session->set_userdata('level_user','3');
    //   redirect('purchase_order');    
    // }elseif ($data->level_user==4) {
    //   $this->session->set_userdata('level_user','4');
    //   redirect('purchase_order');    
    // }elseif ($data->level_user==5) {
    //   $this->session->set_userdata('level_user','5');
    //   redirect('dashboard');    
    // }elseif ($data->level_user==6) {
    //   $this->session->set_userdata('level_user','6');
    //   redirect('purchase_order');    
    // }elseif ($data->level_user==7) {
    //   $this->session->set_userdata('level_user','7');
    //   redirect('work_order');    
    // }elseif ($data->level_user==8) {
    //   $this->session->set_userdata('level_user','8');
    //   redirect('dashboard');    
    // }
  }else{
     echo " <script>
     alert('Login Gagal: Cek Kembali username dan password yang anda masukkan . . ');
     history.go(-1);
     </script>";        
   
 }  
}
}
function logout(){
  $this->session->sess_destroy();
  $url=base_url('');
  redirect('Login');
}

}

