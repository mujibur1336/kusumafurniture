<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Info extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('M_info');
		$this->load->helper('form','url','number');
		$this->load->library('form_validation');
        $x = $this->session->userdata('username');
        if(empty($x)) {
            $this->session->set_flashdata('isLogin',false);
            redirect('home');
        }
    }  
	public function index()
	{
		$x['data'] 		= $this->M_info->getAll();		
		$x['kategori']	= $this->M_info->get_kategori();	
		$this->load->view('layout/header');
		$this->load->view('layout/sidebar');
		$this->load->view('v_info', $x);
		$this->load->view('layout/footer');
	}
	public function v_input()
	{
		$this->load->view('layout/header');
		$this->load->view('layout/sidebar');
		$this->load->view('v_addinfo');
		$this->load->view('layout/footer');
	}
	function input(){
		if(isset($_POST['btnsimpan']))
		{	
			$this->load->library('upload');			
			$info_judul  = $this->input->post('info_judul');
			$kategori_id = $this->input->post('kategori_id');
			$info_isi    = $this->input->post('info_isi'); 			
			$info_status = $this->input->post('info_status');			

			$fileUpload = array();            
            $isUpload   = false;          
            $config  = array(
                'upload_path'   => './assets/upload_gambar/',
                'allowed_types' => 'PNG|png|JPG|jpg|JPEG|JPG',
                'file_name'    =>  'teak_patio_garden_furniture_'.$info_judul.'_teak_info'
            );

            $this->upload->initialize($config);
            if ($this->upload->do_upload('userfile')) {
                $fileUpload = $this->upload->data();
                $isUpload = true;
            }

			$data = array(
				'info_judul'    => $info_judul,				
				'info_isi'      => $info_isi,
				'kategori_id'	=> $kategori_id,
				'info_status'	=> $info_status
			);
			if ($isUpload==true){ 
                $data=array('info_img'=> $fileUpload['file_name'])+$data; 
            }

			$this->M_info->insert($data);
			redirect(site_url('info'));
			/*var_dump($data);*/
		}
		else {
			redirect(site_url('info'));

		}
	}
	function edit(){
		if(isset($_POST['btnedit'])){

			$this->load->library('upload');			
			$info_judul  = $this->input->post('info_judul');
			$info_isi    = $this->input->post('info_isi');			
			$kategori_id = $this->input->post('kategori_id'); 			

			$fileUpload = array();            
            $isUpload   = false;          
            $config  = array(
                'upload_path'   => './assets/upload_gambar/',
                'allowed_types' => 'PNG|png|JPG|jpg|JPEG|JPG',
                'file_name'    =>  'teak_patio_garden_furniture_'.$info_judul.'_teak_info'

            );

            $this->upload->initialize($config);
            if ($this->upload->do_upload('userfile')) {
                $fileUpload = $this->upload->data();
                $isUpload = true;
            }

			$data = array(
				'info_judul'    => $info_judul,				
				'info_isi'      => $info_isi,
				'kategori_id'	=> $kategori_id
			);
			if ($isUpload==true){ 
                $data=array('info_img'=> $fileUpload['file_name'])+$data; 
            }
			$id = $this->input->post('info_id');
			$this->M_info->edit($data,$id);
			redirect(site_url('info'));
			/*var_dump($data);*/
			

		}else {
			redirect(site_url('info'));
		}
	}

	 function delete(){
        if(isset($_POST['btndelete'])){
            $id = $this->input->post('info_id');
            $this->M_info->delete($id);
            redirect(site_url('info'));
        }else {
            redirect(site_url('info'));
        }
    }
    function status_info(){
		if(isset($_POST['btnstatus'])){
			$id = $this->input->post('info_id');
			$this->M_info->status_update($id);
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	function status_info2(){
		if(isset($_POST['btnstatus'])){
			$id = $this->input->post('info_id');
			$this->M_info->status_update2($id);
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

   
}
