<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('M_produk');
		$this->load->helper('form','url','number');
		$this->load->library('form_validation');
        $x = $this->session->userdata('username');
    if(empty($x)) {
            $this->session->set_flashdata('isLogin',false);
            redirect('home');
        }
    }
    function index()
      {
      	$x['data'] 		= $this->M_produk->produk();		
		$this->load->view('layout/header');
		$this->load->view('layout/sidebar');
		$this->load->view('v_kproduk', $x);
		$this->load->view('layout/footer');
      }  
	public function daftar()
	{
		$idu            = $this->uri->segment(3);
		$x['data'] 		= $this->M_produk->getAll($idu);
		$x['katalog']   = $this->M_produk->get_katalog();
		$this->load->view('layout/header');
		$this->load->view('layout/sidebar');
		$this->load->view('v_produk', $x);
		$this->load->view('layout/footer');
	}
	function input(){
		if(isset($_POST['btnsimpan']))
		{	
			$this->load->library('upload');
			$katalog_id    = $this->input->post('katalog_id');
			$produk_kode   = $this->input->post('produk_kode');
			$produk_nama   = $this->input->post('produk_nama'); 
			$produk_size   = $this->input->post('produk_size'); 			
			$produk_seo    = $this->input->post('produk_seo');
			$produk_status = $this->input->post('produk_status');

			$fileUpload = array();            
            $isUpload   = false;          
            $config  = array(
                'upload_path'   => './assets/upload_gambar/',
                'allowed_types' => 'PNG|png|JPG|jpg|JPEG|JPG',
                'file_name'    =>  'teak_patio_garden_furniture_'.$produk_nama.'_'.$produk_kode, 
            );

            $this->upload->initialize($config);
            if ($this->upload->do_upload('userfile')) {
                $fileUpload = $this->upload->data();
                $isUpload = true;
            }

			$data = array(
				'produk_kode'     => $produk_kode,
				'katalog_id'	  => $katalog_id,
				'produk_nama'     => $produk_nama,
				'produk_size'     => $produk_size,
				'produk_status'   => $produk_status,
				'produk_seo'      => str_replace(' ', '-', strtolower($produk_nama)),
			);
			if ($isUpload==true){ 
                $data=array('produk_img'=> $fileUpload['file_name'])+$data; 
            }

			$this->M_produk->insert($data);
			redirect($_SERVER['HTTP_REFERER']);
			/*var_dump($data);*/
		}
		else {
			redirect($_SERVER['HTTP_REFERER']);

		}
	}
	function edit(){
		if(isset($_POST['btnedit'])){

			$this->load->library('upload');
			$katalog_id    = $this->input->post('katalog_id');
			$produk_kode   = $this->input->post('produk_kode');
			$produk_nama   = $this->input->post('produk_nama'); 
			$produk_size   = $this->input->post('produk_size'); 			
			$produk_seo    = $this->input->post('produk_seo');
			$produk_status = $this->input->post('produk_status');

			$fileUpload = array();            
            $isUpload   = false;          
            $config  = array(
                'upload_path'   => './assets/upload_gambar/',
                'allowed_types' => 'PNG|png|JPG|jpg|JPEG|JPG',
                'file_name'    =>  'teak_patio_garden_furniture_'.$produk_nama.'_'.$produk_kode, 
            );

            $this->upload->initialize($config);
            if ($this->upload->do_upload('userfile')) {
                $fileUpload = $this->upload->data();
                $isUpload = true;
            }

			$data = array(
				'produk_kode'     => $produk_kode,
				'katalog_id'	  => $katalog_id,
				'produk_nama'     => $produk_nama,
				'produk_size'     => $produk_size,
				'produk_status'   => $produk_status,
				'produk_seo'      => str_replace(' ', '-', strtolower($produk_nama)),
			);
			if ($isUpload==true){ 
                $data=array('produk_img'=> $fileUpload['file_name'])+$data; 
            }
			$id = $this->input->post('produk_id');
			$this->M_produk->edit($data,$id);
			redirect($_SERVER['HTTP_REFERER']);
			/*var_dump($data);*/
			

		}else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	 function delete(){
        if(isset($_POST['btndelete'])){
            $id = $this->input->post('produk_id');
            $this->M_produk->delete($id);
            redirect($_SERVER['HTTP_REFERER']);
        }else {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }
    
      public function spesial($value='')
    {
    	$x['data'] 		= $this->M_produk->spesial();		
		$this->load->view('layout/header');
		$this->load->view('layout/sidebar');
		$this->load->view('v_spesial', $x);
		$this->load->view('layout/footer');
    }

    function input_spesial(){
		if(isset($_POST['btnsimpan']))
		{	
			$this->load->library('upload');			

			$spesial_judul = $this->input->post('spesial_judul'); 
			$spesial_isi   = $this->input->post('spesial_isi');
			$spesial_seo   = $this->input->post('spesial_seo');
			

			$fileUpload = array();            
            $isUpload   = false;          
            $config  = array(
                'upload_path'   => './assets/upload_gambar/',
                'allowed_types' => 'PNG|png|JPG|jpg|JPEG|JPG',
                'file_name'    =>  'teak_patio_garden_furniture_'.$spesial_judul, 
            );

            $this->upload->initialize($config);
            if ($this->upload->do_upload('userfile')) {
                $fileUpload = $this->upload->data();
                $isUpload = true;
            }

			$data = array(
				
				'spesial_judul'     => $spesial_judul,
				'spesial_isi'     => $spesial_isi,				
				'spesial_seo'      => str_replace(' ', '-', strtolower($spesial_judul))

			);
			if ($isUpload==true){ 
                $data=array('spesial_img'=> $fileUpload['file_name'])+$data; 
            }

			$this->M_produk->insert_spesial($data);
			redirect($_SERVER['HTTP_REFERER']);
			/*var_dump($data);*/
		}
		else {
			redirect($_SERVER['HTTP_REFERER']);

		}
	}
	function edit_spesial(){
		if(isset($_POST['btnedit'])){

			$this->load->library('upload');
			
			$spesial_judul = $this->input->post('spesial_judul'); 
			$spesial_isi   = $this->input->post('spesial_isi');
			$spesial_seo   = $this->input->post('spesial_seo');

			$fileUpload = array();            
            $isUpload   = false;          
            $config  = array(
                'upload_path'   => './assets/upload_gambar/',
                'allowed_types' => 'PNG|png|JPG|jpg|JPEG|JPG',
                'file_name'    =>  'teak_patio_garden_furniture_'.$spesial_judul, 
            );

            $this->upload->initialize($config);
            if ($this->upload->do_upload('userfile')) {
                $fileUpload = $this->upload->data();
                $isUpload = true;
            }

			$data = array(
				'spesial_judul'     => $spesial_judul,
				'spesial_isi'     => $spesial_isi,				
				'spesial_seo'      => str_replace(' ', '-', strtolower($spesial_judul))
			);
			if ($isUpload==true){ 
                $data=array('spesial_img'=> $fileUpload['file_name'])+$data; 
            }
			$id = $this->input->post('spesial_id');
			$this->M_produk->edit_spesial($data,$id);
			redirect($_SERVER['HTTP_REFERER']);
			/*var_dump($data);*/
			

		}else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	 function delete_spesial(){
        if(isset($_POST['btndelete'])){
            $id = $this->input->post('spesial_id');
            $this->M_produk->delete_spesial($id);
            redirect($_SERVER['HTTP_REFERER']);
        }else {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

   
}
