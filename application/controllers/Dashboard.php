<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function __construct() {
    parent::__construct();
    $this->load->model('M_dashboard');
    $this->load->helper('form','url','number');
    $this->load->library('form_validation');
    $x = $this->session->userdata('username');
    if(empty($x)) {
            $this->session->set_flashdata('isLogin',false);
            redirect('home');
        }
    }    
	public function index()
	{
        $data['produk']      = $this->M_dashboard->tot_produk();
        $data['blog']        = $this->M_dashboard->tot_blog();
        $data['katalog']     = $this->M_dashboard->tot_katalog();
        $data['pesan']       = $this->M_dashboard->tot_pesan();
        $data['user']        = $this->M_dashboard->tot_pengguna();
        $data['spesial']     = $this->M_dashboard->tot_spesial();
        
		$this->load->view('layout/header');
		$this->load->view('layout/sidebar');
		$this->load->view('v_dashboard',$data);
		$this->load->view('layout/footer');
	}
}