<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('M_blog');
		$this->load->helper('form','url','number');
		$this->load->library('form_validation');
		$x = $this->session->userdata('username');
    if(empty($x)) {
            $this->session->set_flashdata('isLogin',false);
            redirect('home');
        }
    }  
	public function index()
	{
		$x['data'] 		= $this->M_blog->getAll();
		$x['kategori']	= $this->M_blog->getAll_kategori();		
		$this->load->view('layout/header');
		$this->load->view('layout/sidebar');
		$this->load->view('v_blog', $x);
		$this->load->view('layout/footer');
	}
	
		public function tampil()
	{
		$x['data'] 		= $this->M_blog->getAll2();
		$x['kategori']	= $this->M_blog->getAll_kategori();		
		$this->load->view('layout/header');
		$this->load->view('layout/sidebar');
		$this->load->view('v_blog', $x);
		$this->load->view('layout/footer');
	}
		public function v_input()
	{
		$x['kategori']	= $this->M_blog->getAll_kategori();	
		$this->load->view('layout/header');
		$this->load->view('layout/sidebar');
		$this->load->view('v_addblog',$x);
		$this->load->view('layout/footer');
	}

	 function v_edit()
    {
        $idu          = $this->uri->segment(3);            
        $x['data'] 	  = $this->M_blog->getAll_id($idu);   
        $x['kategori']	= $this->M_blog->getAll_kategori();	     
        $this->load->view('layout/header');
		$this->load->view('layout/sidebar');
		$this->load->view('v_editblog',$x);
		$this->load->view('layout/footer');
    }

	function input(){
		if(isset($_POST['btnsimpan']))
		{	
			$this->load->library('upload');			
			$blog_judul  		= $this->input->post('blog_judul');
			$blog_isi    		= $this->input->post('blog_isi');
			$blog_meta    		= $this->input->post('blog_meta');
			$blog_status 		= $this->input->post('blog_status');
			$blog_seo 	 		= $this->input->post('blog_seo');
			$blog_kategori_id 	= $this->input->post('blog_kategori_id');

			$fileUpload = array();            
            $isUpload   = false;          
            $config  = array(
                'upload_path'   => './assets/upload_gambar/',
                'allowed_types' => 'PNG|png|JPG|jpg|jpeg|JPEG',
                'file_name'    =>  'teak_patio_garden_furniture_'.$blog_seo.'_teak_blog'
            );

            $this->upload->initialize($config);
            if ($this->upload->do_upload('userfile')) {
                $fileUpload = $this->upload->data();
                $isUpload = true;
            }

			$data = array(
				'blog_judul'    		=> $blog_judul,				
				'blog_isi'      		=> $blog_isi,
				'blog_meta'      		=> $blog_meta,
				'blog_kategori_id'      => $blog_kategori_id,
				'blog_seo'      		=> str_replace(' ', '-', strtolower($blog_judul)),
				'blog_status'			=> $blog_status
			);
			if ($isUpload==true){ 
                $data=array('blog_img'=> $fileUpload['file_name'])+$data; 
            }

			$this->M_blog->insert($data);
			redirect(site_url('blog'));
			/*var_dump($data);*/
		}
		else {
			redirect(site_url('blog'));

		}
	}
	function edit(){
		if(isset($_POST['btnedit'])){

			$this->load->library('upload');			
			$blog_judul  		= $this->input->post('blog_judul');
			$blog_isi    		= $this->input->post('blog_isi');
			$blog_meta    		= $this->input->post('blog_meta');
			$blog_kategori_id 	= $this->input->post('blog_kategori_id');
			$blog_seo 	 		= $this->input->post('blog_seo'); 			

			$fileUpload = array();            
            $isUpload   = false;          
            $config  = array(
                'upload_path'   => './assets/upload_gambar/',
                'allowed_types' => 'PNG|png|JPG|jpg|jpeg|JPEG',
                'file_name'    =>  'teak_patio_garden_furniture_'.$blog_seo.'_teak_blog'

            );

            $this->upload->initialize($config);
            if ($this->upload->do_upload('userfile')) {
                $fileUpload = $this->upload->data();
                $isUpload = true;
            }

			$data = array(
				'blog_judul'   		=> $blog_judul,				
				'blog_isi'     		=> $blog_isi,	
				'blog_meta'      		=> $blog_meta,
				'blog_kategori_id'  => $blog_kategori_id,
				'blog_seo'     		=> str_replace(' ', '-', strtolower($blog_judul))
			);
			if ($isUpload==true){ 
                $data=array('blog_img'=> $fileUpload['file_name'])+$data; 
            }
			$id = $this->input->post('blog_id');
			$this->M_blog->edit($data,$id);
			redirect(site_url('blog'));
			/*var_dump($data);*/
			

		}else {
			redirect(site_url('blog'));
		}
	}

	 function delete(){
        if(isset($_POST['btndelete'])){
            $id = $this->input->post('blog_id');
            $this->M_blog->delete($id);
            redirect(site_url('blog'));
        }else {
            redirect(site_url('blog'));
        }
    }
    function status_blog(){
		if(isset($_POST['btnstatus'])){
			$id = $this->input->post('blog_id');
			$this->M_blog->status_update($id);
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	function status_blog2(){
		if(isset($_POST['btnstatus'])){
			$id = $this->input->post('blog_id');
			$this->M_blog->status_update2($id);
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function kategori()
	{

		$x['data'] 		= $this->M_blog->getAll_kategori();
		$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"mdi mdi-information\"></i> data kategori tidak bisa dihapus</div>");		
		$this->load->view('layout/header');
		$this->load->view('layout/sidebar');
		$this->load->view('v_kategori_blog', $x);
		$this->load->view('layout/footer');
	}

	function inputkategori(){
		if(isset($_POST['btnsimpan']))
		{	
			
			$blog_kategori_nama  = $this->input->post('blog_kategori_nama');
			$blog_kategori_seo	 = $this->input->post('blog_kategori_seo');

            

			$data = array(
				'blog_kategori_nama'    => $blog_kategori_nama,								
				'blog_kategori_seo'     => str_replace(' ', '-', strtolower($blog_kategori_nama))
			);
			
			$this->M_blog->insert_kategori($data);
			redirect(site_url('blog/kategori'));
			/*var_dump($data);*/
		}
		else {
			redirect(site_url('blog/kategori'));

		}
	}
	function editkategori(){
		if(isset($_POST['btnedit']))
		{	
			
			$blog_kategori_nama  = $this->input->post('blog_kategori_nama');
			$blog_kategori_seo	 = $this->input->post('blog_kategori_seo');

            

			$data = array(
				'blog_kategori_nama'    => $blog_kategori_nama,								
				'blog_kategori_seo'     => str_replace(' ', '-', strtolower($blog_kategori_nama))
			);
			
			$id = $this->input->post('blog_kategori_id');
			$this->M_blog->edit_kategori($data,$id);
			redirect(site_url('blog/kategori'));
			
		}
		else {
			redirect(site_url('blog/kategori'));

		}
	}
	
	public function upload($value=''){
    	$x['data'] 		= $this->M_blog->upload();		
		$this->load->view('layout/header');
		$this->load->view('layout/sidebar');
		$this->load->view('v_upload',$x);
		$this->load->view('layout/footer');
    }
    
    function input_gambar(){
		if(isset($_POST['btnsimpan']))
		{	
			$this->load->library('upload');			
	        $img =$this->input->post('img');

			$fileUpload = array();            
            $isUpload   = false;          
            $config  = array(
                'upload_path'   => './assets/upload_gambar/',
                'allowed_types' => 'PNG|png|JPG|jpg|JPEG|JPG',
                'file_name'    =>  'teak_patio_garden_furniture_'.$img, 
            );

            $this->upload->initialize($config);
            if ($this->upload->do_upload('userfile')) {
                $fileUpload = $this->upload->data();
                $isUpload = true;
            }

			$data = array(
				
				'img'     => $img,
				// 'spesial_isi'     => $spesial_isi,				
				// 'spesial_seo'      => str_replace(' ', '-', strtolower($nama_img))

			);
			if ($isUpload==true){ 
                $data=array('img'=> $fileUpload['file_name'])+$data; 
            }

			$this->M_blog->insert_img($data);
			redirect($_SERVER['HTTP_REFERER']);
			/*var_dump($data);*/
		}
		else {
			redirect($_SERVER['HTTP_REFERER']);

		}
	}
	
	function edit_gambar(){
		if(isset($_POST['btnedit'])){

			$this->load->library('upload');
			$img =$this->input->post('img');

			$fileUpload = array();            
            $isUpload   = false;          
            $config  = array(
                'upload_path'   => './assets/upload_gambar/',
                'allowed_types' => 'PNG|png|JPG|jpg|JPEG|JPG',
                'file_name'    =>  'teak_patio_garden_furniture_', 
            );

            $this->upload->initialize($config);
            if ($this->upload->do_upload('userfile')) {
                $fileUpload = $this->upload->data();
                $isUpload = true;
            }

			$data = array(
				'img'     => $img,
			);
			if ($isUpload==true){ 
                $data=array('img'=> $fileUpload['file_name'])+$data; 
            }
			$id = $this->input->post('id_img');
			$this->M_blog->edit_img($data,$id);
			redirect($_SERVER['HTTP_REFERER']);
			/*var_dump($data);*/
			

		}else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	
	function delete_gambar(){
        if(isset($_POST['btndelete'])){
            $id = $this->input->post('id_img');
            $this->M_blog->delete_img($id);
            redirect($_SERVER['HTTP_REFERER']);
        }else {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

   
}
