<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SitemapController extends CI_Controller {

	public function index(){
		$this->load->model('SitemapModel');
		 $data['post'] = $this->SitemapModel->get_artikel();
		 $data['produk'] = $this->SitemapModel->get_produk();
        $this->load->view('sitemap', $data);
	}

}
?>