<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('M_home');
		$this->load->model('M_kategori');
		$this->load->model('M_blog');
		$this->load->helper('form','url','number');
		$this->load->helper(array('string', 'text'));
		$this->load->library('form_validation');
	}    
	
	// public function index()
	// {
	//     $x['data'] = $this->M_home->getPopuler();
	// 	$x['data1'] = $this->M_home->produk();
	// 	$x['data2'] = $this->M_kategori->profil();
	// 	$this->load->view('layout/header_front',$x);
	// 	$this->load->view('frontend_baru/home',$x);
	// 	$this->load->view('layout/footer_front',$x);
	// }

	public function index()
	{
		$config['base_url'] = site_url('article/page'); 
        $config['total_rows'] = $this->db->count_all('v_blog');
        $config['per_page'] = 10;  
        $config["uri_segment"] = 3;  
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = 'Next';
        $config['prev_link']        = 'Prev';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
        $this->pagination->initialize($config);
        $x['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $x['data'] = $this->M_home->data($config["per_page"], $x['page']);
        $x['pagination'] = $this->pagination->create_links();
		$x['data3'] 	= $this->M_home->blog();

	    $x['data_pop'] = $this->M_home->getPopuler();
		$x['data1'] = $this->M_home->produk();
		$x['data2'] = $this->M_kategori->profil();
		$this->load->view('layout/header_baru',$x);
		$this->load->view('frontend_baru/home',$x);
		$this->load->view('layout/footer_baru',$x);
	}
	public function about()
	{
		$x['data1'] = $this->M_home->produk();
		$x['data2'] = $this->M_kategori->profil();
		$x['data3'] = $this->M_home->getAbout1();
		$x['data4'] = $this->M_home->getAbout2();
		$x['data5'] = $this->M_home->getAbout3();
		$x['data6'] = $this->M_home->getAbout4();
		$this->load->view('layout/header_about',$x);
		$this->load->view('frontend_baru/about',$x);
		$this->load->view('layout/footer_baru',$x);
	}

	public function produk()
	{
		// $x['data1'] = $this->M_home->produk();
		// $x['data2'] = $this->M_kategori->profil();
		// $x['data3'] = $this->M_home->getAbout1();
		// $x['data4'] = $this->M_home->getAbout2();
		// $x['data5'] = $this->M_home->getAbout3();
		// $x['data6'] = $this->M_home->getAbout4();
		$this->load->view('layout/header_baru');
		$this->load->view('frontend_baru/product');
		$this->load->view('layout/footer_baru');
	}
	public function blog()
	{
		$config['base_url'] = site_url('article/page'); 
        $config['total_rows'] = $this->db->count_all('v_blog');
        $config['per_page'] = 25;  
        $config["uri_segment"] = 3;  
        // $choice = $config["total_rows"] / $config["per_page"];
        // $config["num_links"] = floor($choice);
        $config["num_links"] = 2;
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = 'Next';
        $config['prev_link']        = 'Prev';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
        
        $this->pagination->initialize($config);
        $x['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 2;
        $x['data'] = $this->M_home->data($config["per_page"], $x['page']);
        $x['pagination'] = $this->pagination->create_links();
		$x['data1'] = $this->M_home->produk();
		$x['data2'] = $this->M_kategori->profil();
		$x['data3'] 	= $this->M_home->blog();
		$this->load->view('layout/header_article',$x);
		$this->load->view('frontend_baru/blog',$x);
		$this->load->view('layout/footer_baru',$x);
	}
	public function blog_detail()
	{
		$blog_seo   	= $this->uri->segment(3);
		$x['data']      = $this->M_blog->getByseo($blog_seo);
		$x['data1'] 	= $this->M_home->produk();
		$x['data2']     = $this->M_kategori->profil();
		$x['data3'] 	= $this->M_home->blog();
		$this->load->view('layout/header_single_article',$x);
		$this->load->view('frontend_baru/blog_detail',$x);
		$this->load->view('layout/footer_baru',$x);
	}
	public function product()
	{
		$katalog_seo   	= $this->uri->segment(3);
		$x['data'] 		= $this->M_home->getByKategori($katalog_seo);
		$x['data1'] 	= $this->M_home->produk();
		$x['data2']     = $this->M_kategori->profil();
		$x['katalog']     = $this->M_home->katalog($katalog_seo);
		$this->load->view('layout/header_product',$x);
		$this->load->view('frontend_baru/product',$x);
		$this->load->view('layout/footer_baru',$x);
	}
	public function contact()
	{
	    $this->load->helper('captcha');
		$vals=array(
			'img_path' 		=> './captcha/',
			'img_url' 		=> base_url().'captcha/',
			'img_width' 	=> '200',
			'img_height' 	=> 30,
			'border'	 	=> 0,
			'expiration' 	=> 3600
		);
		$cap = create_captcha($vals);
		$x['image'] = $cap['image'];
		$x['word'] 	= $cap['word'];
		$x['data1'] = $this->M_home->produk();
		$x['data2'] = $this->M_kategori->profil();
		$this->session->set_userdata('mycaptcha',$cap['word']);
		$this->load->view('layout/header_contact',$x);
		$this->load->view('frontend/v_contact',$x);
		$this->load->view('layout/footer_front',$x);
	}

	public function contact1()
	{
	    $this->load->helper('captcha');
		$vals=array(
			'img_path' 		=> './captcha/',
			'img_url' 		=> base_url().'captcha/',
			'img_width' 	=> '200',
			'img_height' 	=> 30,
			'border'	 	=> 0,
			'expiration' 	=> 3600
		);
		$cap = create_captcha($vals);
		$x['image'] = $cap['image'];
		$x['word'] 	= $cap['word'];
		$x['data1'] = $this->M_home->produk();
		$x['data2'] = $this->M_kategori->profil();
		$this->session->set_userdata('mycaptcha',$cap['word']);
		$this->load->view('layout/header_contact',$x);
		$this->load->view('frontend_baru/contact',$x);
		$this->load->view('layout/footer_baru',$x);
	}

	public function special_offer()
	{
		$x['data'] 	= $this->M_home->getSpesial();
		$x['data1'] = $this->M_home->produk();
		$x['data2'] = $this->M_kategori->profil();
		$this->load->view('layout/header_special',$x);
		$this->load->view('frontend_baru/special_offer',$x);
		$this->load->view('layout/footer_baru',$x);
	}
	function input(){
		function get_client_ip() {
			$ipaddress = '';
			if (getenv('HTTP_CLIENT_IP'))
				$ipaddress = getenv('HTTP_CLIENT_IP');
			else if(getenv('HTTP_X_FORWARDED_FOR'))
				$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
			else if(getenv('HTTP_X_FORWARDED'))
				$ipaddress = getenv('HTTP_X_FORWARDED');
			else if(getenv('HTTP_FORWARDED_FOR'))
				$ipaddress = getenv('HTTP_FORWARDED_FOR');
			else if(getenv('HTTP_FORWARDED'))
				$ipaddress = getenv('HTTP_FORWARDED');
			else if(getenv('REMOTE_ADDR'))
				$ipaddress = getenv('REMOTE_ADDR');
			else
				$ipaddress = 'UNKNOWN';
			return $ipaddress;
		}

		function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE) {
			$output = NULL;
			if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
				$ip = $_SERVER["REMOTE_ADDR"];
				if ($deep_detect) {
					if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
						$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
					if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
						$ip = $_SERVER['HTTP_CLIENT_IP'];
				}
			}
			$purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
			$support    = array("country", "countrycode", "state", "region", "city", "location", "address");
			$continents = array(
				"AF" => "Africa",
				"AN" => "Antarctica",
				"AS" => "Asia",
				"EU" => "Europe",
				"OC" => "Australia (Oceania)",
				"NA" => "North America",
				"SA" => "South America"
			);
			if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
				$ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
				if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
					switch ($purpose) {
						case "location":
						$output = array(
							"city"           => @$ipdat->geoplugin_city,
							"state"          => @$ipdat->geoplugin_regionName,
							"country"        => @$ipdat->geoplugin_countryName,
							"country_code"   => @$ipdat->geoplugin_countryCode,
							"continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
							"continent_code" => @$ipdat->geoplugin_continentCode
						);
						break;
						case "address":
						$address = array($ipdat->geoplugin_countryName);
						if (@strlen($ipdat->geoplugin_regionName) >= 1)
							$address[] = $ipdat->geoplugin_regionName;
						if (@strlen($ipdat->geoplugin_city) >= 1)
							$address[] = $ipdat->geoplugin_city;
						$output = implode(", ", array_reverse($address));
						break;
						case "city":
						$output = @$ipdat->geoplugin_city;
						break;
						case "state":
						$output = @$ipdat->geoplugin_regionName;
						break;
						case "region":
						$output = @$ipdat->geoplugin_regionName;
						break;
						case "country":
						$output = @$ipdat->geoplugin_countryName;
						break;
						case "countrycode":
						$output = @$ipdat->geoplugin_countryCode;
						break;
					}
				}
			}
			return $output;
		}
		if(isset($_POST['btnsimpan']))
		{	
			$ipaddress		 = get_client_ip();
			$location  		 = ip_info(get_client_ip(), "Address");	
			$pesan_nama   	 = $this->input->post('pesan_nama'); 
			$pesan_email  	 = $this->input->post('pesan_email'); 
			$pesan_subject   = $this->input->post('pesan_subject'); 
			$pesan_isi   	 = $this->input->post('pesan_isi'); 	
			$data = array(
				'pesan_ip'      => $ipaddress,
				'pesan_alamat'  => $location,
				'pesan_nama'	=> $pesan_nama,
				'pesan_email'	=> $pesan_email,
				'pesan_subject'	=> $pesan_subject,
				'pesan_isi'		=> $pesan_isi

			);
			$this->M_home->insert($data);
			$pesan = '<html><body>
			<h4>Hai ada pesan dari, '.$pesan_nama.' ( '.$pesan_email.')</h4>
			Berikut isi dari pesannya : <br> 
			Subject : '.$pesan_subject.' <br>
			Isi : '.$pesan_isi.' <br>
			Lokasi : '.$location.' <br>
			Ip Address : '.$ipaddress.'
			</body></html>';
			$this->load->library('email');
			$config['protocol'] = "smtp";
			$config['smtp_host'] = "ssl://mail.kusumafurniture.com";
			$config['smtp_port'] = "465";
			$config['smtp_timeout'] = "20";
			$config['smtp_user'] = "admin2020@kusumafurniture.com";
			$config['smtp_pass'] = "6YpCgx899e";
			$config['smtp_secure'] = "tls";
			$config['charset'] = "UTF-8";
			$config['mailtype'] = "html";
			$config['wordwrap'] = TRUE ;

			$this->email->initialize($config);
			$this->email->set_newline("\r\n");
			$this->email->from('admin2020@kusumafurniture.com', 'Admin Kusuma Furniture');
			$list = 'kusuma.furniture@gmail.com';
			$this->email->to($list);
			$this->email->subject('Notifikasi Email dari '.$pesan_nama);
			$this->email->message($pesan);
			if ($this->email->send()) { ?>
				<script type="text/javascript">
					alert('Berhasil mengirim email !');
				</script>
			<?php } else {
				show_error($this->email->print_debugger());
			}
			redirect(site_url('home/contact'));
		}
		else {
			redirect(site_url('home/contact'));

		}
	}
}
