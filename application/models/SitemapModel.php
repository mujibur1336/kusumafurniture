<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SitemapModel extends CI_Model {
    
    private static $_table = 'tb_blog';

	function create() {
		$this->db->select('blog_seo,blog_tgl');
// 		$this->db->where('status','publish');
	    return $this->db->order_by('blog_tgl', 'desc')->get('tb_blog')->result_array();
	}
	
	public function get_artikel(){
    $query = $this->db->select('*')->from(self::$_table)->order_by('blog_tgl', 'DESC')->get();
    $results = array();
        if ($query->num_rows() > 0) {
         $results = $query->result_array();
        }
        return $results;
	    
	}
	
	public function get_produk(){
    $query = $this->db->select('*')->from('tb_katalog')->get();
    $results = array();
        if ($query->num_rows() > 0) {
         $results = $query->result_array();
        }
        return $results;
	    
	}
}
?>
