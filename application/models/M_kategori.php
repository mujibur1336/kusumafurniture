<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem..!!');

class M_kategori extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  } 
  function getAll(){
   $this->db->select('*');
   $this->db->from('tb_kategori');
   $this->db->order_by('kategori_id', 'DESC');
   $query = $this->db->get();
   return $query->result();
 } 
 function insert($data){
  $this->db->insert('tb_kategori',$data);
}
function edit($data,$id){
  $this->db->where('kategori_id',$id);
  $this->db->update('tb_kategori',$data);
}

function delete($id) {
  $this->db->where('kategori_id', $id);
  $this->db->delete('tb_kategori');
  if ($this->db->affected_rows() == 1) {
    return TRUE;
  }
  return FALSE;
}
function profil()
{
   $this->db->select('*');
   $this->db->from('tb_profil');   
   $query = $this->db->get();
   return $query->result();
}
function profil_edit($data,$id){
  $this->db->where('profil_id',$id);
  $this->db->update('tb_profil',$data);
}
}
?>
