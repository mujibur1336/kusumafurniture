<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_dashboard extends CI_Model {        
    function __construct() {
        parent::__construct();
    }

     function tot_produk() {        
        $sql="select count(produk_id) as total
                from 
                    tb_produk
            ";
        return $this->db->query($sql)->result();
    }

     function tot_blog() {        
        $sql="select count(blog_id) as total
                from 
                    tb_blog
            ";
        return $this->db->query($sql)->result();
    }

     function tot_katalog() {        
        $sql="select count(katalog_id) as total
                from 
                    tb_katalog
            ";
        return $this->db->query($sql)->result();
    }

    function tot_pesan() {        
        $sql="select count(pesan_id) as total
                from 
                    tb_pesan
            ";
        return $this->db->query($sql)->result();
    }

     function tot_pengguna() {        
        $sql="select count(user_id) as total
                from 
                    tb_user
                    ";
        return $this->db->query($sql)->result();
    }  
    function tot_spesial() {        
        $sql="select count(spesial_id) as total
                from 
                    tb_spesial
                    ";
        return $this->db->query($sql)->result();
    } 

}