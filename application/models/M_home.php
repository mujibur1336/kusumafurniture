<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem..!!');

class M_home extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  } 
  
  function insert($data){
    $this->db->insert('tb_pesan',$data);
  }
  function produk()
  {
    $sql="
    SELECT
    katalog_id,
    katalog_nama,
    katalog_seo,
    count(katalog_id) as total
    FROM
    v_produk
    GROUP BY katalog_id ";
    return $this->db->query($sql)->result();
  } 
  function getPopuler(){
   $this->db->select('*');
   $this->db->from('v_produk');
   $this->db->where('produk_status',2);
   $this->db->order_by('katalog_id', 'ASC');
   $query = $this->db->get();
   return $query->result();
 }
 function getAbout1(){
   $this->db->select('*');
   $this->db->from('v_info');
   $this->db->where('info_status',2);
   $this->db->where('kategori_id',1);
   $query = $this->db->get();
   return $query->result();
 }
 function getAbout2(){
   $this->db->select('*');
   $this->db->from('v_info');
   $this->db->where('info_status',2);
   $this->db->where('kategori_id',2);
   $query = $this->db->get();
   return $query->result();
 }
 function getAbout3(){
   $this->db->select('*');
   $this->db->from('v_info');
   $this->db->where('info_status',2);
   $this->db->where('kategori_id',3);
   $query = $this->db->get();
   return $query->result();
 }
 function getAbout4(){
   $this->db->select('*');
   $this->db->from('v_info');
   $this->db->where('info_status',2);
   $this->db->where('kategori_id',4);
   $query = $this->db->get();
   return $query->result();
 }
 function getByKategori($katalog_seo){
   $this->db->select('*');
   $this->db->from('v_produk');
   $this->db->where('katalog_seo',$katalog_seo);
   $this->db->order_by('produk_kode', 'DESC');
   $query = $this->db->get();
   return $query->result();
 } 
    function getSpesial(){
   $this->db->select('*');
   $this->db->from('tb_spesial');
   $query = $this->db->get();
   return $query->result();
 }
   function data($number,$offset){
    $this->db->order_by('blog_id', 'DESC');
    return $query = $this->db->get('v_blog',$number,$offset)->result();   
  }
   function blog()
  {
    $sql="
    SELECT * FROM v_blog ORDER BY blog_tgl DESC LIMIT 3 ";
    return $this->db->query($sql)->result();
  } 
  
  function katalog($katalog_seo){
        $this->db->select('*');
       $this->db->from('tb_katalog');
       $this->db->where('katalog_seo',$katalog_seo);
       $query = $this->db->get();
       return $query->result(); 
  }
}
