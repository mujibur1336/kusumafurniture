<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem..!!');

class M_blog extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  } 
function getAll(){
   $this->db->select('*');
   $this->db->from('tb_blog');
   $this->db->order_by('blog_id', 'DESC');
   $query = $this->db->get();
   return $query->result();
 }
 
 function getAll2(){
   $this->db->select('blog_id,blog_judul, blog_img,blog_seo');
   $this->db->from('tb_blog');
   $this->db->order_by('blog_id', 'DESC');
   $query = $this->db->get();
   return $query->result();
 }
 
 function getByseo($blog_seo){
   $this->db->select('*');
   $this->db->from('v_blog');
   $this->db->where('blog_seo',$blog_seo);
   $query = $this->db->get();
   return $query->result();
 }
  function getAll_id($idu){
   $this->db->select('*');
   $this->db->from('v_blog');
   $this->db->where('blog_id',$idu);
   $query = $this->db->get();
   return $query->result();
 }
 function getAll_kategori(){
   $this->db->select('*');
   $this->db->from('tb_blog_kategori');
   $this->db->order_by('blog_kategori_id', 'DESC');
   $query = $this->db->get();
   return $query->result();
 } 
 public function insert($data)
  {
    $this->db->insert('tb_blog', $data);
    return $this->db->insert_id(); 
  }
  public function insert_kategori($data)
  {
    $this->db->insert('tb_blog_kategori', $data);
    return $this->db->insert_id(); 
  }
function edit($data,$idu){
  $this->db->where('blog_id',$idu);
  $this->db->update('tb_blog',$data);
}

function edit_kategori($data,$idu){
  $this->db->where('blog_kategori_id',$idu);
  $this->db->update('tb_blog_kategori',$data);
}
  function delete($id) {
  $this->db->where('blog_id', $id);
  $query = $this->db->get('tb_blog');
  $row = $query->row();
  unlink("./assets/upload_gambar/$row->blog_img");
  /*$this->db->delete('tb_blog');*/
   $this->db->delete('tb_blog', array('blog_id' => $id));
 }
 
function status_update($id)
{
  $sql = "UPDATE tb_blog
  SET blog_status = '2'
  WHERE blog_id = $id";
  return $this->db->query($sql);
}
function status_update2($id)
{
  $sql = "UPDATE tb_blog
  SET blog_status = '1'
  WHERE blog_id = $id";
  return $this->db->query($sql);
}

function upload(){
   $this->db->select('*');
   $this->db->from('tb_gambar');   
   $this->db->order_by('id_img', 'DESC');
   $query = $this->db->get();
   return $query->result();
 } 
 
 public function insert_img($data)
  {
    $this->db->insert('tb_gambar', $data);
    return $this->db->insert_id(); 
  }

function delete_img($id) {
  $this->db->where('id_img', $id);
  $query = $this->db->get('tb_gambar');
  $row = $query->row();
  unlink("./assets/upload_gambar/$row->upload_img");
  /*$this->db->delete('tb_produk');*/
   $this->db->delete('tb_gambar', array('id_img' => $id));
 }
 
 function edit_img($data,$idu){
  $this->db->where('id_img',$idu);
  $this->db->update('tb_gambar',$data);
} 


}
?>
