<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem..!!');

class M_info extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  } 
function getAll(){
   $this->db->select('*');
   $this->db->from('v_info');
   $this->db->order_by('info_id', 'DESC');
   $query = $this->db->get();
   return $query->result();
 } 
 public function insert($data)
  {
    $this->db->insert('tb_info', $data);
    return $this->db->insert_id(); 
  }

  function delete($id) {
  $this->db->where('info_id', $id);
  $query = $this->db->get('tb_info');
  $row = $query->row();
  unlink("./assets/upload_gambar/$row->info_img");
  /*$this->db->delete('tb_info');*/
   $this->db->delete('tb_info', array('info_id' => $id));
 }
 function edit($data,$idu){
  $this->db->where('info_id',$idu);
  $this->db->update('tb_info',$data);
}
function status_update($id)
{
  $sql = "UPDATE tb_info
  SET info_status = '2'
  WHERE info_id = $id";
  return $this->db->query($sql);
}
function status_update2($id)
{
  $sql = "UPDATE tb_info
  SET info_status = '1'
  WHERE info_id = $id";
  return $this->db->query($sql);
}
function get_kategori(){
 $this->db->select('*');
 $this->db->from('tb_kategori');
 $query = $this->db->get();
 return $query->result();
} 

}
?>
