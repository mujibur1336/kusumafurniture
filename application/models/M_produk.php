<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem..!!');

class M_produk extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  }

  function produk()
   {
      $sql="
        SELECT
            katalog_id,
            katalog_nama,
        count(katalog_id) as total
        FROM
            v_produk
        GROUP BY katalog_id ";
        return $this->db->query($sql)->result();
   } 
function getAll($idu){
   $this->db->select('*');
   $this->db->from('v_produk');
   $this->db->where('katalog_id',$idu);
   $this->db->order_by('produk_kode', 'DESC');
   $query = $this->db->get();
   return $query->result();
 } 
 
 public function insert($data)
  {
    $this->db->insert('tb_produk', $data);
    return $this->db->insert_id(); 
  }

  function delete($id) {
  $this->db->where('produk_id', $id);
  $query = $this->db->get('tb_produk');
  $row = $query->row();
  unlink("./assets/upload_gambar/$row->produk_img");
  /*$this->db->delete('tb_produk');*/
   $this->db->delete('tb_produk', array('produk_id' => $id));
 }
 
 function edit($data,$idu){
  $this->db->where('produk_id',$idu);
  $this->db->update('tb_produk',$data);
}
function get_katalog(){
 $this->db->select('*');
 $this->db->from('tb_katalog');
 $query = $this->db->get();
 return $query->result();
} 
function spesial(){
   $this->db->select('*');
   $this->db->from('tb_spesial');   
   $this->db->order_by('spesial_id', 'DESC');
   $query = $this->db->get();
   return $query->result();
 } 
 public function insert_spesial($data)
  {
    $this->db->insert('tb_spesial', $data);
    return $this->db->insert_id(); 
  }

  function delete_spesial($id) {
  $this->db->where('spesial_id', $id);
  $query = $this->db->get('tb_spesial');
  $row = $query->row();
  unlink("./assets/upload_gambar/$row->spesial_img");
  /*$this->db->delete('tb_produk');*/
   $this->db->delete('tb_spesial', array('spesial_id' => $id));
 }
 
 function edit_spesial($data,$idu){
  $this->db->where('spesial_id',$idu);
  $this->db->update('tb_spesial',$data);
} 
}
?>
