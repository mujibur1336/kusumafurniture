<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem..!!');

class M_katalog extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  } 
  function getAll(){
   $this->db->select('*');
   $this->db->from('tb_katalog');
   $this->db->order_by('katalog_id', 'ASC');
   $query = $this->db->get();
   return $query->result();
 } 
 function insert($data){
  $this->db->insert('tb_katalog',$data);
}
function edit($data,$id){
  $this->db->where('katalog_id',$id);
  $this->db->update('tb_katalog',$data);
}
function delete($id) {
  $this->db->where('katalog_id', $id);
  $this->db->delete('tb_katalog');
  if ($this->db->affected_rows() == 1) {
    return TRUE;
  }
  return FALSE;
}
}
?>
