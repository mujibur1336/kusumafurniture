<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem..!!');

class M_user extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  } 
  function getAll(){
   $this->db->select('*');
   $this->db->from('tb_user');
   $this->db->order_by('user_id', 'DESC');
   $query = $this->db->get();
   return $query->result();
 } 
 function insert($data){
  $this->db->insert('tb_user',$data);
}
function edit($data,$id){
  $this->db->where('user_id',$id);
  $this->db->update('tb_user',$data);
}
function delete($id) {
  $this->db->where('user_id', $id);
  $this->db->delete('tb_user');
  if ($this->db->affected_rows() == 1) {
    return TRUE;
  }
  return FALSE;
}
}
?>
