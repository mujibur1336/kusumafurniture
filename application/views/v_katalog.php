            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">DAFTAR PRODUK KATALOG</h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Dashboard </a>
                                        </li>
                                        <li class="active">
                                            Daftar Produk Katalog
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                              <form method="post" action="<?php echo site_url('Katalog/input');?>" enctype='multipart/form-data'>
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel">TAMBAH PRODUK KATEGORI</h4>
                              </div>
                              <div class="modal-body">
                                <div class="col-lg-12">
                                    <label for="txtname">Nama katalog</label>
                                </div>
                                <div class="col-lg-12" style="margin-bottom: 10px">
                                    <input type="text" class="form-control" id="katalog_nama" name="katalog_nama" placeholder="Masukkan Nama katalog">
                                    <input type="hidden" class="form-control" id="katalog_seo" name="katalog_seo">
                                </div>
                                <div class="col-lg-12">
                                    <label for="txtname">Deskripsi katalog</label>
                                </div>
                                <div class="col-lg-12" style="margin-bottom: 10px">
                                    <textarea name="katalog_quote" id="summ" rows="5" class="form-control" placeholder="Masukkan Deskripsi Katalog"></textarea>
                                </div>
                         </div>
                         <div class="modal-footer" style="margin-top: 32%">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="submit" id="btnsimpan" name="btnsimpan" class="btn btn-success">Save</button>
                      </div>
                  </form>
              </div>
          </div>
      </div>
      <!-- end row -->
      <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <table id="tb_katalog" class="table table-striped table-bordered dt-responsive nowrap">
                    <?php echo  $this->session->flashdata('pesan'); ?>
                    <thead>
                        <tr>
                            <th style="text-align: center; width: 1%">No</th>
                            <th style="text-align: center; width: 5%">Nama Kategori</th>
                            <th style="text-align: center; width: 10%">Deskripsi Kategori</th>                                                    
                            <th style="text-align: center; width: 5%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 0 ;
                        foreach ($data as $rowdata) {   
                            $no++;              
                            ?> 
                            <tr>
                                <td style="text-align: center;"><?php echo $no ?></td>
                                <td><?php echo $rowdata->katalog_nama ?></td>
                                <td style="white-space: pre-wrap; word-wrap: break-word;"><?php echo $rowdata->katalog_quote ?></td>
                                <td align="center">
                                    <span class="btn btn-warning btn-sm" style="font-family: serif;" data-toggle="modal" data-target="#ModalEdit<?php echo $rowdata->katalog_id; ?>"><i class="fa fa-pencil"></i></span>
                                    <!-- <button type="button" class="btn btn-danger btn-sm " data-toggle="modal" data-target="#ModalDelete<?php echo $rowdata->katalog_id; ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button> -->
                                </td>
                                <!-- Modal Delete-->
                                <!-- <div class="modal fade" id="ModalDelete<?php echo $rowdata->user_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                      <div class="modal-content">
                                        <form method="post" action="<?php echo site_url('user/delete');?>">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel" style="font-family: sans-serif;">Konfirmasi Hapus Data</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                              <input type="hidden" id="user_id" name="user_id" value="<?php echo $rowdata->user_id; ?>">
                                              <label style="font-family: sans-serif; font-size: 16px;">Apakah anda yakin, akan menghapus data : <?php echo "'<u>".$rowdata->username."</u>'";?> ?</label>
                                          </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                        <button type="submit" id="btndelete" name="btndelete" class="btn btn-danger">Hapus</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div> -->
                    <!-- Modal Edit-->
                    <div class="modal fade" id="ModalEdit<?php echo $rowdata->katalog_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog modal-lg" role="document">
                          <div class="modal-content">
                            <form method="post" action="<?php echo site_url('katalog/edit');?>" enctype='multipart/form-data'>
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Edit PRODUK KATEGORI</h4>
                              </div>
                              <div class="modal-body">
                                
                               <div class="col-lg-12" align="left">
                                <label for="txtemail">Nama katalog</label>
                              </div>
                              <div class="col-lg-12">
                                <input type="text" class="form-control" id="katalog_nama" name="katalog_nama" required="required" value="<?php echo $rowdata->katalog_nama; ?>">                                
                                <input type="hidden" class="form-control" id="katalog_seo" name="katalog_seo">
                                <input type="hidden" class="form-control" id="katalog_id" name="katalog_id" value="<?php echo $rowdata->katalog_id; ?>">
                              </div>
                                                   
                            
                              <div class="col-lg-12">
                                    <label for="txtname">Deskripsi katalog</label>
                                </div>
                                <div class="col-lg-12" style="margin-bottom: 10px">
                                    <textarea id="summernote<?php echo $rowdata->katalog_id; ?>" name="katalog_quote" rows="5" class="form-control" placeholder="Masukkan Deskripsi Katalog"><?php echo $rowdata->katalog_quote; ?>
                                    </textarea>
                                </div>
                       </div>
                       <div class="modal-footer" style="margin-top: 25%">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                        <button type="submit" id="btnedit" name="btnedit" class="btn btn-success">Simpan</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
                </tr>
                
                <script>
            $(document).ready(function () {
                if($("#elm<?php echo $rowdata->katalog_id; ?>").length > 0){
                    tinymce.init({
                        selector: "textarea#elm<?php echo $rowdata->katalog_id; ?>",
                        theme: "modern",
                        height:250,
                        plugins: [
                            "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                            "save table contextmenu directionality emoticons template paste textcolor"
                        ],
                        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
                        style_formats: [
                            {title: 'Bold text', inline: 'b'},
                            {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                            {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                            {title: 'Example 1', inline: 'span', classes: 'example1'},
                            {title: 'Example 2', inline: 'span', classes: 'example2'},
                            {title: 'Table styles'},
                            {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
                        ]
                    });
                }
            });
        </script>
         <script>
            $(document).ready(function() {
                $('#summernote<?php echo $rowdata->katalog_id; ?>').summernote({
                    height: 400
                });
            });
          </script>
            <?php } ?>
        </tbody>
    </table>
</div>
</div>
</div>
</div> <!-- container -->

</div> <!-- content -->

</div>

</div>
<!-- END wrapper -->
