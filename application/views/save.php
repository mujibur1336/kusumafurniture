<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Tambah Kelas </h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="#">Home</a>
                            </li>
                            <li>
                                <a href="#">Kelas </a>
                            </li>
                            <li class="active">
                                Tambah Kelas
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="col-md-12">
                <div class="card-box">
                    <ul class="nav nav-tabs tabs-bordered nav-justified">
                        <li class="active">
                            <a href="#home-b2" data-toggle="tab" aria-expanded="false">
                                <span><i class="fa fa-info-circle"></i> | Informasi Umum</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="#profile-b2" data-toggle="tab" aria-expanded="true">
                                <span><i class="mdi mdi-book-open"></i> | Kelola Modul</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="#messages-b2" data-toggle="tab" aria-expanded="false">
                                <span><i class="mdi mdi-clipboard-text"></i> | Kelola Tes</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="#settings-b2" data-toggle="tab" aria-expanded="false">
                                <span><i class="fa fa-users"></i> | Peserta Kelas</span>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="home-b2">
                            <div class="row">
                                <div class="col-sm-3">
                                    <h4>Informasi Umum</h4>
                                    <hr>
                                </div>
                                <div class="col-sm-9" align="right">
                                    <a href="<?php echo base_url()?>anggota/form/add" class="btn btn-sm btn-success">
                                        <i class="fa fa-plus-circle"></i> | SIMPAN DATA</a>  
                                        <hr>
                                    </div>
                                    <?php foreach ($data_kelas as $rowdata) {  ?>
                                        <div class="col-lg-4">
                                            <label for="txtname">Nama Kelas</label>
                                        </div>
                                        <div class="col-lg-8" style="margin-bottom: 10px">
                                            <input type="text" class="form-control" id="nama_lengkap" name="nama_lengkap" placeholder="Masukkan Nama Kelas" value="<?php echo $rowdata->nama_kelas ?>">
                                        </div>
                                        <div class="col-lg-4">
                                            <label for="txtname">Deskripsi Kelas</label>
                                        </div>
                                        <div class="col-lg-8" style="margin-bottom: 10px">
                                            <textarea name="deskripsi_kelas" rows="5" class="form-control" placeholder="Masukkan Deskripsi Kelas"><?php echo $rowdata->deskripsi_kelas ?></textarea>
                                        </div>
                                        <div class="col-lg-4">
                                            <label for="txtname">Email PIC</label>
                                        </div>
                                        <div class="col-lg-8" style="margin-bottom: 10px">
                                            <input type="email" class="form-control" id="email" name="email" placeholder="Masukkan Email" value="<?php echo $rowdata->email_pic ?>">
                                        </div>
                                        <div class="col-lg-4">
                                            <label for="txtpesan">Trainer</label>
                                        </div>
                                        <div class="col-lg-8" style="margin-bottom: 10em">
                                            <select  id="trainer" name="trainer" class="selectpicker form-control" data-live-search="true">
                                                <option value="<?php echo $rowdata->id_trainer ?>"><?php echo $rowdata->id_trainer ?> | <?php echo $rowdata->nama_trainer ?></option>
                                                <?php foreach ($data_trainer as $rowtrainer) { ?>
                                                    <option value="<?php echo $rowtrainer->id_trainer ?>"><?php echo $rowtrainer->id_trainer ?> | <?php echo $rowtrainer->nama_trainer ?></option>
                                                <?php }?>
                                            </select> 
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="tab-pane" id="profile-b2">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <h4>Kelola Modul</h4>
                                        <hr>
                                    </div>
                                    <div class="col-sm-9" align="right">
                                        <a class="btn btn-sm btn-success" data-toggle="modal" data-target="#ModalTambahModul"><span class="fa fa-plus-circle" aria-hidden="true"></span><b> | TAMBAH MODUL </b></a>
                                        <hr>
                                    </div>
                                    <?php 
                                    $nomor_modul = 0;
                                    foreach ($data_modul as $rowmodul) {  
                                        $nomor_modul++;
                                        ?>
                                        <div class="sortable">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="portlet card-draggable">
                                                        <div class="portlet-heading bg-primary">
                                                            <h3 class="portlet-title">
                                                                Modul <?php echo $nomor_modul.' - '.$rowmodul->judul_modul ?>
                                                            </h3>
                                                            <div class="portlet-widgets">
                                                                <a data-toggle="modal" data-target="#ModalTambahBab<?php echo $rowmodul->id_modul; ?>" style="cursor: pointer; padding-right: 10px"><span class="fa fa-plus-circle" aria-hidden="true"></span><b> | TAMBAH BAB </b></a>

                                                                <a type="button" data-toggle="modal" data-target="#ModalHapusModul<?php echo $rowmodul->id_modul; ?>" style="cursor: pointer;"><span class="fa fa-times-circle" aria-hidden="true"></span><b> | HAPUS MODUL </b></a>

                                                                <a data-toggle="collapse" data-parent="#accordion1" href="#<?php echo 'modul'.$rowmodul->id_modul ?>" style="padding-left:0.5em"><i class="ion-minus-round"></i></a>
                                                                <span class="divider"></span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <div id="<?php echo 'modul'.$rowmodul->id_modul ?>" class="panel-collapse collapse in">
                                                            <div class="portlet-body" align="center">
                                                                <?php 
                                                                $nomor_bab = 0;
                                                                foreach ($data_bab as $rowbab) {  
                                                                    if ($rowbab->id_modul == $rowmodul->id_modul) {
                                                                        $nomor_bab++;
                                                                        ?>
                                                                        <div class="portlet card">
                                                                            <div class="portlet-heading bg-info">
                                                                                <h3 class="portlet-title">
                                                                                    BAB <?php echo $nomor_bab.' - '.$rowbab->judul_bab ?>
                                                                                </h3>
                                                                                <div class="portlet-widgets">
                                                                                    <a data-toggle="modal" data-target="#ModalTambahMateri<?php echo $rowbab->id_bab; ?>" style="cursor: pointer; padding-right: 10px"><span class="fa fa-plus-circle" aria-hidden="true"></span><b> | TAMBAH MATERI </b></a>

                                                                                    <a type="button" data-toggle="modal" data-target="#ModalHapusBAB<?php echo $rowbab->id_bab; ?>" style="cursor: pointer;"><span class="fa fa-times-circle" aria-hidden="true"></span><b> | HAPUS BAB </b></a>

                                                                                    <a data-toggle="collapse" data-parent="#accordion1" href="#<?php echo $rowbab->id_bab ?>" style="padding-left:0.5em"><i class="ion-minus-round"></i></a>
                                                                                    <span class="divider"></span>
                                                                                </div>
                                                                                <div class="clearfix"></div>
                                                                            </div>
                                                                            <div id="<?php echo $rowbab->id_bab ?>" class="panel-collapse collapse in">
                                                                                <div class="portlet-body">
                                                                                    <table id="tb_bab" class="table table-striped table-bordered dt-responsive nowrap">
                                                                                        <tr>
                                                                                            <th style="text-align: center;">Judul Materi</th>
                                                                                            <th style="text-align: center;">File</th>
                                                                                            <th style="text-align: center;">Aksi</th>
                                                                                        </tr>
                                                                                        <?php foreach ($data_materi as $rowmateri) {
                                                                                            if ($rowbab->id_bab == $rowmateri->id_bab) { ?>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <?php echo $rowmateri->judul_materi ?>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <?php echo $rowmateri->file ?>
                                                                                                    </td>
                                                                                                    <td align="center">
                                                                                                        <span class="btn btn-warning btn-sm" style="font-family: serif;" data-toggle="modal" data-target="#ModalEdit<?php echo $rowmateri->id_materi; ?>"><i class="fa fa-pencil"></i></span>
                                                                                                        <button type="button" class="btn btn-danger btn-sm " data-toggle="modal" data-target="#ModalHapusMateri<?php echo $rowmateri->id_materi; ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <!-- Modal Hapus MATERI -->
                                                                                                <div class="modal fade" id="ModalHapusMateri<?php echo $rowmateri->id_materi; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                                                                    <div class="modal-dialog" role="document">
                                                                                                        <div class="modal-content">
                                                                                                            <form method="post" action="#">
                                                                                                                <div class="modal-header">
                                                                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                                                                    <h4 class="modal-title" id="myModalLabel" style="font-family: sans-serif;">HAPUS MATERI</h4>
                                                                                                                </div>
                                                                                                                <div class="modal-body">
                                                                                                                    <div class="form-group">
                                                                                                                      <input type="hidden" id="id_materi" name="id_materi" value="<?php echo $rowmateri->id_materi; ?>">
                                                                                                                      <label style="font-family: sans-serif; font-size: 16px;">Apakah anda yakin, akan menghapus materi : <?php echo "'<u>".$rowmateri->judul_materi."</u>'";?> ?</label>
                                                                                                                  </div>
                                                                                                              </div>
                                                                                                              <div class="modal-footer">
                                                                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                                                                                                <button type="submit" id="btndelete" name="btndelete" class="btn btn-danger">Hapus</button>
                                                                                                            </div>
                                                                                                        </form>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        <?php }
                                                                                    } ?>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <br>
                                                                    <!-- Modal Hapus BAB -->
                                                                    <div class="modal fade" id="ModalHapusBAB<?php echo $rowbab->id_bab; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                                        <div class="modal-dialog" role="document">
                                                                            <div class="modal-content">
                                                                                <form method="post" action="#">
                                                                                    <div class="modal-header">
                                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                                        <h4 class="modal-title" id="myModalLabel" style="font-family: sans-serif;">HAPUS BAB <?php echo $rowbab->id_bab; ?></h4>
                                                                                    </div>
                                                                                    <div class="modal-body" align="left">
                                                                                        <div class="col-lg-4">
                                                                                            <label for="txtname">Judul Modul</label>
                                                                                        </div>
                                                                                        <div class="col-lg-8" style="margin-bottom: 10px">
                                                                                            <input type="text" class="form-control" id="judul_bab" name="judul_bab" placeholder="Masukkan Judul Modul">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="modal-footer" style="margin-top: 7%">
                                                                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                                      <button type="submit" id="btnsimpan" name="btnsimpan" class="btn btn-success">Save</button>
                                                                                  </div>
                                                                              </form>
                                                                          </div>
                                                                      </div>
                                                                  </div>
                                                                  <!-- Modal Tambah Materi -->
                                                                  <div class="modal fade" id="ModalTambahMateri<?php echo $rowbab->id_bab; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                                    <div class="modal-dialog" role="document">
                                                                        <div class="modal-content">
                                                                            <form method="post" action="<?php echo site_url('kelas/tambah_materi');?>" enctype='multipart/form-data'>
                                                                                <div class="modal-header">
                                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                                    <h4 class="modal-title" id="myModalLabel" style="font-family: sans-serif;">TAMBAH MATERI <?php echo $rowbab->id_bab; ?></h4>
                                                                                </div>
                                                                                <div class="modal-body" align="left">
                                                                                    <div class="col-lg-4">
                                                                                        <label for="txtname">Judul Materi</label>
                                                                                    </div>
                                                                                    <div class="col-lg-8" style="margin-bottom: 10px">
                                                                                        <input type="text" class="form-control" id="judul_materi" name="judul_materi" placeholder="Masukkan Judul Materi">
                                                                                        <input type="hidden" class="form-control" id="id_bab" name="id_bab" value="<?php echo $rowbab->id_bab ?>">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="modal-body" align="left">
                                                                                    <div class="col-lg-4">
                                                                                        <label for="txtname">File Materi</label>
                                                                                    </div>
                                                                                    <div class="col-lg-8" style="margin-bottom: 10px">
                                                                                     <input type="file" name="file_materi" class="form-control" value="">
                                                                                 </div>
                                                                             </div>
                                                                             <div class="modal-footer" style="margin-top: 7%">
                                                                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                              <button type="submit" id="btnsimpan" name="btnsimpan" class="btn btn-success">Save</button>
                                                                          </div>
                                                                      </form>
                                                                  </div>
                                                              </div>
                                                          </div> 
                                                      <?php }
                                                  } ?>
                                                  <!-- Modal Tambah BAB -->
                                                  <div class="modal fade" id="ModalTambahBab<?php echo $rowmodul->id_modul; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <form method="post" action="<?php echo site_url('kelas/tambah_bab');?>">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                    <h4 class="modal-title" id="myModalLabel" style="font-family: sans-serif;">TAMBAH BAB <?php echo $rowmodul->id_modul; ?></h4>
                                                                </div>
                                                                <div class="modal-body" align="left">
                                                                    <div class="col-lg-4">
                                                                        <label for="txtname">Judul BAB</label>
                                                                    </div>
                                                                    <div class="col-lg-8" style="margin-bottom: 10px">
                                                                        <input type="text" class="form-control" id="judul_bab" name="judul_bab" placeholder="Masukkan Judul BAB">
                                                                        <input type="hidden" class="form-control" id="id_modul" name="id_modul" value="<?php echo $rowmodul->id_modul ?>">
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer" style="margin-top: 7%">
                                                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                  <button type="submit" id="btnsimpan" name="btnsimpan" class="btn btn-success">Save</button>
                                                              </div>
                                                          </form>
                                                      </div>
                                                  </div>
                                              </div> 
                                              <!-- Modal Tambah MODUL -->
                                              <div class="modal fade" id="ModalTambahModul" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <form method="post" action="<?php echo site_url('kelas/tambah_modul');?>">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                <h4 class="modal-title" id="myModalLabel" style="font-family: sans-serif;">TAMBAH MODUL</h4>
                                                            </div>
                                                            <div class="modal-body" align="left">
                                                                <div class="col-lg-4">
                                                                    <label for="txtname">Judul Modul</label>
                                                                </div>
                                                                <div class="col-lg-8" style="margin-bottom: 10px">
                                                                    <input type="text" class="form-control" id="judul_modul" name="judul_modul" placeholder="Masukkan Judul Modul">
                                                                    <input type="hidden" class="form-control" id="id_kelas" name="id_kelas" value="<?php echo $rowmodul->id_kelas ?>">
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer" style="margin-top: 7%">
                                                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                              <button type="submit" id="btnsimpan" name="btnsimpan" class="btn btn-success">Save</button>
                                                          </div>
                                                      </form>
                                                  </div>
                                              </div>
                                          </div> 
                                          <!-- Modal Hapus MODUL -->
                                          <div class="modal fade" id="ModalHapusModul<?php echo $rowmodul->id_modul; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <form method="post" action="#">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title" id="myModalLabel" style="font-family: sans-serif;">HAPUS MODUL<?php echo $rowmodul->id_modul; ?></h4>
                                                        </div>
                                                        <div class="modal-body" align="left">
                                                            <div class="col-lg-4">
                                                                <label for="txtname">Judul Modul</label>
                                                            </div>
                                                            <div class="col-lg-8" style="margin-bottom: 10px">
                                                                <input type="text" class="form-control" id="judul_bab" name="judul_bab" placeholder="Masukkan Judul Modul">
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer" style="margin-top: 7%">
                                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                          <button type="submit" id="btnsimpan" name="btnsimpan" class="btn btn-success">Save</button>
                                                      </div>
                                                  </form>
                                              </div>
                                          </div>
                                      </div>                                                      
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div> <!-- End row -->
              </div>
              <!-- end sortable -->
          <?php } ?>
      </div>
  </div>
  <div class="tab-pane" id="messages-b2">
    <div class="row">
        <div class="col-sm-12">
            <h4>Kelola Test</h4>
            <hr>
        </div>
        <div class="sortable">
            <div class="row">
                <div class="col-lg-12">
                    <div class="portlet card">
                        <div class="portlet-heading bg-info">
                            <h3 class="portlet-title">
                                PRE-TEST
                            </h3>
                            <div class="portlet-widgets">
                                <a data-toggle="modal" data-target="#ModalTambahPretest" style="cursor: pointer; padding-right: 10px"><span class="fa fa-plus-circle" aria-hidden="true"></span><b> | TAMBAH PERTANYAAN </b></a>
                                <a data-toggle="modal" data-target="#" style="cursor: pointer; padding-right: 10px"><span class="mdi mdi-timer" aria-hidden="true"></span><b> | DEADLINE PRE TEST </b></a>

                                <a data-toggle="collapse" data-parent="#accordion1" href="#tes" style="padding-left:0.5em"><i class="ion-minus-round"></i></a>
                                <span class="divider"></span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div id="tes" class="panel-collapse collapse in">
                            <div class="portlet-body">
                                <div class="card-box table-responsive">
                                    <table id="" class="table display table-striped table-bordered dt-responsive nowrap">
                                        <thead>
                                            <tr>
                                                <th style="text-align: center; width: 1%">No</th>
                                                <th style="text-align: center;">Pertanyaan</th>
                                                <th style="text-align: center;">Kunci Jawaban</th>
                                                <th style="text-align: center;">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $no = 0 ;
                                            foreach ($data_pre_test as $rowmateri) { 
                                                $no++;              
                                                ?> 
                                                <tr>
                                                    <td style="text-align: center;"><?php echo $no ?></td>
                                                    <td><?php echo $rowmateri->pertanyaan ?></td>
                                                    <td><?php echo $rowmateri->kunci_jawaban ?></td>
                                                    <td align="center">
                                                        <span class="btn btn-warning btn-sm" style="font-family: serif;" data-toggle="modal" data-target="#ModalEdit<?php echo $rowmateri->id_pre_test; ?>"><i class="fa fa-pencil"></i></span>
                                                        <button type="button" class="btn btn-danger btn-sm " data-toggle="modal" data-target="#ModalDelete<?php echo $rowmateri->id_pre_test; ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <!-- Modal Tambah Pretest -->
                        <div class="modal fade" id="ModalTambahPretest" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form method="post" action="<?php echo site_url('kelas/pre_test');?>">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel" style="font-family: sans-serif;">TAMBAH PERTANYAAN PRE TEST</h4>
                                        </div>
                                        <div class="modal-body" align="left">
                                            <?php $id_max = $max_soal->maxid+1; 
                                            $id_kelas = $rowmodul->id_kelas;
                                            ?>
                                            <input type="hidden" class="form-control" id="id_soal" name="id_soal" value="<?php echo $id_max ?>">
                                            <input type="hidden" class="form-control" id="id_kelas" name="id_kelas" value="<?php echo $id_kelas ?>">
                                            <div class="col-lg-4">
                                                <label for="txtname">Pertanyaan</label>
                                            </div>
                                            <div class="col-lg-8" style="margin-bottom: 10px">
                                                <textarea type="text" id="pertanyaan" name="pertanyaan" placeholder=" Tulis Pertanyaan" rows="4" cols="40"></textarea>
                                            </div>
                                            <div class="col-lg-4">
                                                <label for="txtname">Jawaban A</label>
                                            </div>
                                            <div class="col-lg-8" style="margin-bottom: 10px">
                                                <textarea type="text" id="jawab_a" name="jawab_a" placeholder=" Tulis Jawaban" rows="2" cols="40"></textarea>
                                            </div>
                                            <div class="col-lg-4">
                                                <label for="txtname">Jawaban B</label>
                                            </div>
                                            <div class="col-lg-8" style="margin-bottom: 10px">
                                                <textarea type="text" id="jawab_b" name="jawab_b" placeholder=" Tulis Jawaban" rows="2" cols="40"></textarea>
                                            </div>
                                            <div class="col-lg-4">
                                                <label for="txtname">Jawaban C</label>
                                            </div>
                                            <div class="col-lg-8" style="margin-bottom: 10px">
                                                <textarea type="text" id="jawab_c" name="jawab_c" placeholder=" Tulis Jawaban" rows="2" cols="40"></textarea>
                                            </div>
                                            <div class="col-lg-4">
                                                <label for="txtname">Jawaban D</label>
                                            </div>
                                            <div class="col-lg-8" style="margin-bottom: 10px">
                                                <textarea type="text" id="jawab_d" name="jawab_d" placeholder=" Tulis Jawaban" rows="2" cols="40"></textarea>
                                            </div>
                                            <div class="col-lg-4">
                                                <label for="txtname">Jawaban E</label>
                                            </div>
                                            <div class="col-lg-8" style="margin-bottom: 10px">
                                                <textarea type="text" id="jawab_e" name="jawab_e" placeholder=" Tulis Jawaban" rows="2" cols="40"></textarea>
                                            </div>
                                            <div class="col-lg-4">
                                                <label for="txtname">Kunci Jawaban</label>
                                            </div>
                                            <div class="col-lg-8" style="margin-bottom: 10px">
                                                <select class="form-control" id="kunci_jawaban" name="kunci_jawaban">
                                                    <option>- Kunci Jawaban -</option>
                                                    <option value="A">A</option>
                                                    <option value="B">B</option>
                                                    <option value="C">C</option>
                                                    <option value="D">D</option>
                                                    <option value="E">E</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="modal-footer" style="margin-top: 82%">
                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                          <button type="submit" id="btnsimpan" name="btnsimpan" class="btn btn-success">Save</button>
                                      </div>
                                  </form>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="col-lg-12">
                <div class="portlet card">
                    <div class="portlet-heading bg-info">
                        <h3 class="portlet-title">
                            POST-TEST
                        </h3>
                        <div class="portlet-widgets">
                            <a data-toggle="modal" data-target="#ModalTambahPosttest" style="cursor: pointer; padding-right: 10px"><span class="fa fa-plus-circle" aria-hidden="true"></span><b> | TAMBAH PERTANYAAN </b></a>
                            <a data-toggle="modal" data-target="#" style="cursor: pointer; padding-right: 10px"><span class="mdi mdi-timer" aria-hidden="true"></span><b> | DEADLINE POST TEST </b></a>

                            <a data-toggle="collapse" data-parent="#accordion1" href="#tes" style="padding-left:0.5em"><i class="ion-minus-round"></i></a>
                            <span class="divider"></span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div id="drag-portlets2" class="panel-collapse collapse in">
                        <div class="portlet-body">
                            <div class="card-box table-responsive">
                                <table id="" class="table display table-striped table-bordered dt-responsive nowrap">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center; width: 1%">No</th>
                                            <th style="text-align: center;">Pertanyaan</th>
                                            <th style="text-align: center;">Kunci Jawaban</th>
                                            <th style="text-align: center;">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 0 ;
                                        foreach ($data_post_test as $rowmateri) { 
                                            $no++;              
                                            ?> 
                                            <tr>
                                                <td style="text-align: center;"><?php echo $no ?></td>
                                                <td><?php echo $rowmateri->pertanyaan ?></td>
                                                <td><?php echo $rowmateri->kunci_jawaban ?></td>
                                                <td align="center">
                                                    <span class="btn btn-warning btn-sm" style="font-family: serif;" data-toggle="modal" data-target="#ModalEdit<?php echo $rowmateri->id_post_test; ?>"><i class="fa fa-pencil"></i></span>
                                                    <button type="button" class="btn btn-danger btn-sm " data-toggle="modal" data-target="#ModalDelete<?php echo $rowmateri->id_post_test; ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- Modal Tambah Posttest -->
                    <div class="modal fade" id="ModalTambahPosttest" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <form method="post" action="<?php echo site_url('kelas/post_test');?>">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel" style="font-family: sans-serif;">TAMBAH PERTANYAAN POST TEST</h4>
                                    </div>
                                    <div class="modal-body" align="left">
                                        <?php $id_max = $max_soal->maxid+1; 
                                        $id_kelas = $rowmodul->id_kelas;
                                        ?>
                                        <input type="hidden" class="form-control" id="id_soal" name="id_soal" value="<?php echo $id_max ?>">
                                        <input type="hidden" class="form-control" id="id_kelas" name="id_kelas" value="<?php echo $id_kelas ?>">
                                        <div class="col-lg-4">
                                            <label for="txtname">Pertanyaan</label>
                                        </div>
                                        <div class="col-lg-8" style="margin-bottom: 10px">
                                            <textarea type="text" id="pertanyaan" name="pertanyaan" placeholder=" Tulis Pertanyaan" rows="4" cols="40"></textarea>
                                        </div>
                                        <div class="col-lg-4">
                                            <label for="txtname">Jawaban A</label>
                                        </div>
                                        <div class="col-lg-8" style="margin-bottom: 10px">
                                            <textarea type="text" id="jawab_a" name="jawab_a" placeholder=" Tulis Jawaban" rows="2" cols="40"></textarea>
                                        </div>
                                        <div class="col-lg-4">
                                            <label for="txtname">Jawaban B</label>
                                        </div>
                                        <div class="col-lg-8" style="margin-bottom: 10px">
                                            <textarea type="text" id="jawab_b" name="jawab_b" placeholder=" Tulis Jawaban" rows="2" cols="40"></textarea>
                                        </div>
                                        <div class="col-lg-4">
                                            <label for="txtname">Jawaban C</label>
                                        </div>
                                        <div class="col-lg-8" style="margin-bottom: 10px">
                                            <textarea type="text" id="jawab_c" name="jawab_c" placeholder=" Tulis Jawaban" rows="2" cols="40"></textarea>
                                        </div>
                                        <div class="col-lg-4">
                                            <label for="txtname">Jawaban D</label>
                                        </div>
                                        <div class="col-lg-8" style="margin-bottom: 10px">
                                            <textarea type="text" id="jawab_d" name="jawab_d" placeholder=" Tulis Jawaban" rows="2" cols="40"></textarea>
                                        </div>
                                        <div class="col-lg-4">
                                            <label for="txtname">Jawaban E</label>
                                        </div>
                                        <div class="col-lg-8" style="margin-bottom: 10px">
                                            <textarea type="text" id="jawab_e" name="jawab_e" placeholder=" Tulis Jawaban" rows="2" cols="40"></textarea>
                                        </div>
                                        <div class="col-lg-4">
                                            <label for="txtname">Kunci Jawaban</label>
                                        </div>
                                        <div class="col-lg-8" style="margin-bottom: 10px">
                                            <select class="form-control" id="kunci_jawaban" name="kunci_jawaban">
                                                <option>- Kunci Jawaban -</option>
                                                <option value="A">A</option>
                                                <option value="B">B</option>
                                                <option value="C">C</option>
                                                <option value="D">D</option>
                                                <option value="E">E</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="modal-footer" style="margin-top: 82%">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      <button type="submit" id="btnsimpan" name="btnsimpan" class="btn btn-success">Save</button>
                                  </div>
                              </form>
                          </div>
                      </div>
                  </div>

              </div>
          </div>
      </div> <!-- End row -->
  </div>
  <!-- end sortable -->
</div>
</div>
<div class="tab-pane" id="settings-b2">
    <div class="row">
        <div class="col-sm-3">
            <h4>Kelola Peserta Kelas</h4>
            <hr>
        </div>
        <div class="col-sm-9" align="right">
            <a data-toggle="modal" data-target="#ModalTambahPeserta" class="btn btn-sm btn-success">
                <i class="fa fa-plus-circle"></i> | TAMBAH PESERTA</a>  
                <hr>
            </div>
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <table id="tb_peserta" class="table table-striped table-bordered dt-responsive nowrap">
                        <thead>
                            <tr>
                                <th style="text-align: center; width: 1%">No</th>
                                <th style="text-align: center; width: 20%">Nama Lengkap</th>
                                <th style="text-align: center; width: 10%">Username</th>
                                <th style="text-align: center; width: 10%">Email</th>
                                <th style="text-align: center; width: 5%">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 0 ;
                            foreach ($data_user as $rowdata) {   
                                $no++;              
                                ?> 
                                <tr>
                                    <td style="text-align: center;"><?php echo $no ?></td>
                                    <td><?php echo $rowdata->nama_lengkap ?></td>
                                    <td><?php echo $rowdata->username ?></td>
                                    <td><?php echo $rowdata->email ?></td>
                                    <td align="center">
                                                    <span class="btn btn-warning btn-sm" style="font-family: serif;" data-toggle="modal" data-target="#ModalEdit<?php echo $rowmateri->id_post_test; ?>"><i class="fa fa-pencil"></i></span>
                                                    <button type="button" class="btn btn-danger btn-sm " data-toggle="modal" data-target="#ModalDelete<?php echo $rowmateri->id_post_test; ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
                                                </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- Modal Tambah Peserta -->
            <div class="modal fade" id="ModalTambahPeserta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <!-- <form method="post" action="<?php echo site_url('kelas/');?>"> -->
                            <form id="frm-example" action="#" method="POST">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel" style="font-family: sans-serif;">TAMBAH PESERTA</h4>
                            </div>
                            <div class="modal-body" align="left">
                                <div class="col-md-12">
                                    <table id="example" class="table table-striped table-bordered dt-responsive nowrap">
                                        <thead>
                                            <tr>
                                                <th style="text-align: center; width: 1%">Pilih</th>
                                                <th style="text-align: center; width: 1%">No</th>
                                                <th style="text-align: center; width: 20%">Nama Lengkap</th>
                                                <th style="text-align: center; width: 10%">Username</th>
                                                <th style="text-align: center; width: 10%">Email</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $no = 0 ;
                                            foreach ($data_user as $rowdata) {   
                                                $no++;              
                                                ?> 
                                                <tr>
                                                    <td></td>
                                                    <td style="text-align: center;"><?php echo $no ?></td>
                                                    <td><?php echo $rowdata->nama_lengkap ?></td>
                                                    <td><?php echo $rowdata->username ?></td>
                                                    <td><?php echo $rowdata->email ?></td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                    <p><b>Selected rows data:</b></p>
<pre id="example-console-rows"></pre>

<p><b>Form data as submitted to the server:</b></p>
<pre id="example-console-form"></pre>
                                    <input type="text" name="xsxs" id="xsxs" class="form-control" value="" readonly > 
                                </div>
                            </div>
                            <div class="modal-footer" style="margin-top: 50%">
                              <button type="button" class="btn btn-default">Close</button>
                              <button type="submit" id="btnsimpan" name="btnsimpan" class="btn btn-success">Save</button>
                          </div>
                      </form>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>
</div> <!-- end col -->
</div>
<!-- end row -->
<script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
        var table = $('#example').DataTable( {
            dom: 
            "<'row'<'col-md-7 left'B><'col-md-3'f><'col-md-2'l>>" +
            "<'row'<'col-md-12't>>" +
            "<'row'<'col-md-5'i><'col-md-7'p>>",
            buttons: [
            {
                extend : 'selectAll',
                text : '<i class="fa fa-check-square-o"></i> Check All',
                className : 'btn btn-default btn-sm'
            },
            {
                extend : 'selectNone',
                text : '<i class="fa fa-square-o"></i> Uncheck All',
                className : 'btn btn-default btn-sm'
            }
        ],
            language: {
                buttons: {
                    className: "btn-sm btn-success"
                },
                "lengthMenu": '_MENU_ Baris',
                "search": '_INPUT_',
                "searchPlaceholder": 'Pencarian . . .'
            },
            columnDefs: [ {
                orderable: false,
                className: 'select-checkbox',
                targets:   0
            } ],
            select: {
                style:    'multi',
                selector: 'td:first-child'
            },
            order: [[ 1, 'asc']]
        } );

     $('#frm-example').on('click', function(e){
      // var form = this;
      var rows_selected = table.column(0).checkboxes.selected();

      // Iterate over all selected checkboxes
      $.each(rows_selected, function(index, rowId){
         // Create a hidden element
         $('#frm-example').append(
             $('<input>')
                .attr('type', 'hidden')
                .attr('name', 'id[]')
                .val(rowId)
         );

      });
       $('#example-console-rows').text(rows_selected.join(","));
      
      // Output form data to a console     
      $('#example-console-form').text($('#frm-example').serialize());
 $('input[name="id\[\]"]', $('#frm-example')).remove();
      });
});

</script>

<!-- <?php 
$_SESSION['word'] = 'countDownDate';
echo $_SESSION['word'];
?> -->
<!-- <script>
   var time = new Date();
   function show(id) {
     if (id == 1) {
        document.getElementById('john').value=time;
     }

     if(id == 2) {
        document.getElementById('amber').value=time;
     }
   }
</script>

</head>
<body>

John: <input type='text' id="john" value=""> 
    <button id='1' onClick="show(this.id)">Click john</button>
        <br>
Amber: <input type='text' id="amber" value=""> 
    <button id='2' onClick="show(this.id)">Click Amber</button>

</body>
</html> -->