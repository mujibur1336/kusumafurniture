            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
              <!-- Start content -->
              <div class="content">
                <div class="container">
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="page-title-box">
                        <h4 class="page-title">Dashboard</h4>
                        <ol class="breadcrumb p-0 m-0">
                              <li class="active">
                                Dashboard
                              </li>
                            </ol>
                            <div class="clearfix"></div>
                          </div>
                        </div>
                      </div>
                      <!-- end row -->
                      
            <div class="row">

                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="card-box widget-box-one">
                        <i class="mdi mdi-certificate widget-one-icon"></i>
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">
                            <a href="<?php echo base_url() ?>Produk">Total Produk</a></p>
                            <?php
                            foreach ($produk as $rowdata) {
                                $total       = $rowdata->total;                   
                            }
                            ?>
                            <h2><?php echo number_format($total); ?></h2>                            
                        </div>
                    </div>
                </div><!-- end col -->

 <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="card-box widget-box-one">
                        <i class="mdi  mdi-bookmark-plus widget-one-icon"></i>
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Total Users"><a href="<?php echo base_url() ?>Produk/spesial">Spesial Offer</a></p>
                            <?php
                            foreach ($spesial as $rowdata) {
                                $total       = $rowdata->total;                   
                            }
                            ?>
                            <h2><?php echo number_format($total); ?></h2>
                        </div>
                    </div>
                </div>

                 <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="card-box widget-box-one">
                        <i class="mdi mdi-layers widget-one-icon"></i>
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="User This Month">  
                            <a href="<?php echo base_url() ?>Katalog">Total Katalog</a></p>
                            <?php
                            foreach ($katalog as $rowdata) {
                                $total       = $rowdata->total;                   
                            }
                            ?>
                            <h2><?php echo number_format($total); ?></h2>
                        </div>
                    </div>
                </div><!-- end col -->

                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="card-box widget-box-one">
                        <i class="mdi mdi-book-open-page-variant widget-one-icon"></i>                        
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="User Today">
                            <a href="<?php echo base_url() ?>Blog">Total Artikel</a></p>
                            <?php
                            foreach ($blog as $rowdata) {
                                $total       = $rowdata->total;                   
                            }
                            ?>
                            <h2><?php echo number_format($total); ?></h2>
                        </div>
                    </div>
                </div><!-- end col -->               

               <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="card-box widget-box-one">
                        <i class=" mdi mdi-message widget-one-icon"></i>
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Request Per Minute"><a href="<?php echo base_url() ?>Pesan">Pesan Masuk</a></p>
                            <?php 
                            foreach ($pesan as $rowdata) { 
                                $total       = $rowdata->total;                   
                            }
                            ?>
                            <h2><?php echo number_format($total); ?></h2>
                        </div>
                    </div>
                </div>
                
                <!-- end col -->

                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="card-box widget-box-one">
                        <i class="mdi mdi-account-multiple widget-one-icon"></i>
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Total Users"><a href="<?php echo base_url() ?>user">Total Pengguna</a></p>
                            <?php
                            foreach ($user as $rowdata) {
                                $total       = $rowdata->total;                   
                            }
                            ?>
                            <h2><?php echo number_format($total); ?></h2>
                        </div>
                    </div>
                </div>

                            </div>
                                <!-- end row -->
                              </div> <!-- container -->

                            </div> <!-- content -->

                          </div>

                        </div>
                        <!-- END wrapper -->
                        <script type="text/javascript">
                          $(document).ready(function() {
                            $('#dashboard').DataTable();
                          } );
                        </script>
