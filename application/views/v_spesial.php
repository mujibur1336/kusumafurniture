            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
              <!-- Start content -->
              <div class="content">
                <div class="container">
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="page-title-box">
                        <h4 class="page-title">DAFTAR PRODUK </h4>
                        <ol class="breadcrumb p-0 m-0">
                          <li>
                            <a href="#">Dashboard </a>
                          </li>
                          <li class="active">
                            Daftar Produk Spesial Offer
                          </li>
                        </ol>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                  <div class="modal fade" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <form method="post" action="<?php echo site_url('produk/input_spesial');?>" enctype='multipart/form-data'>
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">TAMBAH PRODUK Spesial Offer</h4>
                          </div>
                          <div class="modal-body">
                           <!-- <?php date_default_timezone_set('Asia/Jakarta');
                           $date = new DateTime('now');?>
                           <input type="hidden" name="tgl_upload" class="form-control" value="<?php echo $date->format('Y-m-d H:i:s') ?>"> -->
                           <div class="col-lg-4">
                            <label for="txtname">Nama Produk</label>
                          </div>
                          <div class="col-lg-8" style="margin-bottom: 10px">
                            <input type="text" class="form-control" id="spesial_judul" name="spesial_judul" placeholder="Masukkan Nama Produk" required>
                            <input type="hidden" class="form-control" id="spesial_seo" name="spesial_seo">
                          </div>
                           <div class="col-lg-4">
                            <label for="txtname">Deskripsi Produk</label>
                          </div>
                          <div class="col-lg-8" style="margin-bottom: 10px">
                           <textarea class="form-control" id="spesial_isi" name="spesial_isi" rows="4"></textarea>
                          </div>                         
                        <div class="col-lg-4">
                            <label for="txtname">File GAMBAR</label>
                          </div>
                          <div class="col-lg-8" style="margin-bottom: 10px">
                            <input type="file" class="form-control" name="userfile" value="">
                          </div>
                    </div>
                    <div class="modal-footer" style="margin-top: 50%">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button type="submit" id="btnsimpan" name="btnsimpan" class="btn btn-success">Save</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
                <!-- end row -->
                <div class="row">
                  <div class="col-sm-12">
                    <div class="card-box table-responsive">
                      <table id="tb_spesial" class="table table-striped table-bordered dt-responsive nowrap">
                        <thead>
                          <tr>
                            <th style="text-align: center; width: 1%">No</th>
                            <th style="text-align: center; width: 20%">Gambar Produk</th>                            
                            <th style="text-align: center; width: 10%">Nama Produk</th>
                            <th style="text-align: center; width: 10%">Deskripsi Produk</th>                            
                            <th style="text-align: center; width: 5%">Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          $no = 0 ;
                          foreach ($data as $rowdata) {   
                            $no++;              
                            ?> 
                            <tr>
                              <td style="text-align: center;"><?php echo $no ?></td>
                              <td><img src="<?php echo base_url();?>assets/upload_gambar/<?php echo $rowdata->spesial_img;?>" width=40% high=40% alt="Kusuma Furniture"></td>
                              <td><?php echo $rowdata->spesial_judul ?></td>
                              <td style="white-space: pre-wrap; word-wrap: break-word"><?php echo $rowdata->spesial_isi ?></td>                              
                              <td align="center">                                
                                <span class="btn btn-warning btn-sm" style="font-family: serif;" data-toggle="modal" data-target="#ModalEdit<?php echo $rowdata->spesial_id; ?>"><i class="fa fa-pencil"></i></span>

                                <button type="button" class="btn btn-danger btn-sm " data-toggle="modal" data-target="#ModalDelete<?php echo $rowdata->spesial_id; ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
                              </td>
                              <!-- Modal Delete-->
                              <div class="modal fade" id="ModalDelete<?php echo $rowdata->spesial_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <form method="post" action="<?php echo site_url('Produk/delete_spesial');?>">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel" style="font-family: sans-serif;">Konfirmasi Hapus Data</h4>
                                      </div>
                                      <div class="modal-body">
                                        <div class="form-group">
                                          <input type="hidden" id="spesial_id" name="spesial_id" value="<?php echo $rowdata->spesial_id; ?>">
                                          <label style="font-family: sans-serif; font-size: 16px;">Apakah anda yakin, akan menghapus data : <?php echo "'<u>".$rowdata->spesial_judul."</u>'";?> ?</label>
                                        </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                        <button type="submit" id="btndelete" name="btndelete" class="btn btn-danger">Hapus</button>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                              <!-- Modal EDIT-->
                              <div class="modal fade" id="ModalEdit<?php echo $rowdata->spesial_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <form method="post" action="<?php echo site_url('Produk/edit_spesial');?>" enctype='multipart/form-data'>
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">EDIT DATA PRODUK</h4>
                                      </div>
                                      <div class="modal-body">                                       
                                       <input type="hidden" id="spesial_id" name="spesial_id" value="<?php echo $rowdata->spesial_id; ?>">
                                      <div class="col-lg-4">
                                        <label for="txtname">Nama Produk</label>
                                      </div>
                                      <div class="col-lg-8" style="margin-bottom: 10px">
                                        <input type="text" class="form-control" id="spesial_judul" name="spesial_judul" value="<?php echo $rowdata->spesial_judul; ?>" required>
                                        <input type="hidden" class="form-control" id="spesial_seo" name="spesial_seo">
                                      </div>
                                       <div class="col-lg-4">
                                      <label for="txtname">Ukuran Produk</label>
                                    </div>
                                    <div class="col-lg-8" style="margin-bottom: 10px">
                                      <textarea class="form-control" id="spesial_isi" name="spesial_isi" rows="4"><?php echo $rowdata->spesial_isi; ?></textarea>
                                    </div>                                                                   
                                    <div class="col-lg-4">
                                      <label for="txtname">File GAMBAR</label>
                                    </div>
                                    <div class="col-lg-8" style="margin-bottom: 10px">
                                      <img src="<?php echo base_url();?>assets/upload_gambar/<?php echo $rowdata->spesial_img;?>" width=50% high=50% alt="Kusuma Furniture">
                                    </div>
                                    <div class="col-lg-4">
                                      <label for="txtname">File Upload Gambar</label>
                                    </div>
                                    <div class="col-lg-8" style="margin-bottom: 10px">
                                      <input type="file" class="form-control" name="userfile" value="">
                                    </div>
                                    <div class="modal-footer" style="margin-top: 95%">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      <button type="submit" id="btnedit" name="btnedit" class="btn btn-success">Simpan</button>
                                    </div>
                                  </form>
                                </div>
                              </div>
                            </div>                                                        
                          </tr>
                            <?php } ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div> <!-- container -->
              </div> <!-- content -->

            </div>

          </div>
          <!-- END wrapper -->
          <script type="text/javascript">
            $(document).ready(function(){
              $('#btnsimpan').click(function() {
                $('#spinner').show();
              });
            });
          </script>
