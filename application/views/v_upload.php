            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
              <!-- Start content -->
              <div class="content">
                <div class="container">
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="page-title-box">
                        <h4 class="page-title">DAFTAR GAMBAR </h4>
                        <ol class="breadcrumb p-0 m-0">
                          <li>
                            <a href="#">Dashboard </a>
                          </li>
                          <li class="active">
                            Daftar Gambar Artikel
                          </li>
                        </ol>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                  <div class="modal fade" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <form method="post" action="<?php echo site_url('blog/input_gambar');?>" enctype='multipart/form-data'>
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">TAMBAH GAMBAR</h4>
                          </div>
                          <div class="modal-body">
                           <!-- <?php date_default_timezone_set('Asia/Jakarta');
                           $date = new DateTime('now');?>
                           <input type="hidden" name="tgl_upload" class="form-control" value="<?php echo $date->format('Y-m-d H:i:s') ?>"> -->
                        <div class="col-lg-4">
                            <label for="txtname">File Gambar</label>
                          </div>
                          <div class="col-lg-8" style="margin-bottom: 10px">
                            <input type="file" class="form-control" name="userfile" value="">
                          </div>
                    </div>
                    <div class="modal-footer" style="margin-top: 20%">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button type="submit" id="btnsimpan" name="btnsimpan" class="btn btn-success">Save</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
                <!-- end row -->
                <div class="row">
                  <div class="col-sm-12">
                    <div class="card-box table-responsive">
                      <table id="tb_spesial" class="table table-striped table-bordered dt-responsive nowrap">
                        <thead>
                          <tr>
                            <th style="text-align: center; width: 1%">No</th>
                            <th style="text-align: center; width: 15%">Gambar Artikel</th>
                            <th style="text-align: center; width: 15%">Link Gambar</th>
                            <th style="text-align: center; width: 5%">Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          $no = 0 ;
                          foreach ($data as $rowdata) {   
                            $no++;              
                            ?> 
                            <tr>
                              <td style="text-align: center;"><?php echo $no ?></td>
                              <td><img src="<?php echo base_url();?>assets/upload_gambar/<?php echo $rowdata->img;?>" width=40% high=40% alt="Kusuma Furniture"></td>
                              <td><?php echo base_url();?>assets/upload_gambar/<?php echo $rowdata->img;?></td>
                              <td align="center">                                
                                <span class="btn btn-warning btn-sm" style="font-family: serif;" data-toggle="modal" data-target="#ModalEdit<?php echo $rowdata->id_img; ?>"><i class="fa fa-pencil"></i></span>

                                <button type="button" class="btn btn-danger btn-sm " data-toggle="modal" data-target="#ModalDelete<?php echo $rowdata->id_img; ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
                              </td>
                              <!-- Modal Delete-->
                              <div class="modal fade" id="ModalDelete<?php echo $rowdata->id_img; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <form method="post" action="<?php echo site_url('Blog/delete_gambar');?>">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel" style="font-family: sans-serif;">Konfirmasi Hapus Data</h4>
                                      </div>
                                      <div class="modal-body">
                                        <div class="form-group">
                                          <input type="hidden" id="id_img" name="id_img" value="<?php echo $rowdata->id_img; ?>">
                                          <label style="font-family: sans-serif; font-size: 16px;">Apakah anda yakin, akan menghapus data : <?php echo "'<u>".$rowdata->id_img."</u>'";?> ?</label>
                                        </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                        <button type="submit" id="btndelete" name="btndelete" class="btn btn-danger">Hapus</button>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                              <!-- Modal EDIT-->
                              <div class="modal fade" id="ModalEdit<?php echo $rowdata->id_img; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <form method="post" action="<?php echo site_url('Blog/edit_gambar');?>" enctype='multipart/form-data'>
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">EDIT GAMBAR</h4>
                                      </div>
                                      <div class="modal-body">                                       
                                       <input type="hidden" id="id_img" name="id_img" value="<?php echo $rowdata->id_img; ?>">
                                    <div class="col-lg-4" style="margin-bottom: 10px">
                                      <img src="<?php echo base_url();?>assets/upload_gambar/<?php echo $rowdata->img;?>" width=80% high=80% alt="Kusuma Furniture">
                                    </div>
                                   <div class="col-lg-4">
                            <label for="txtname">File Gambar</label>
                          </div>
                          <div class="col-lg-8" style="margin-bottom: 10px">
                            <input type="file" class="form-control" name="userfile" value="">
                          </div>
                    </div>
                    <div class="modal-footer" style="margin-top: 20%">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      <button type="submit" id="btnedit" name="btnedit" class="btn btn-success">Simpan</button>
                                    </div>
                                  </form>
                                </div>
                              </div>
                            </div>                                                        
                          </tr>
                            <?php } ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div> <!-- container -->
              </div> <!-- content -->

            </div>

          </div>
          <!-- END wrapper -->
          <script type="text/javascript">
            $(document).ready(function(){
              $('#btnsimpan').click(function() {
                $('#spinner').show();
              });
            });
          </script>
