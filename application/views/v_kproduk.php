            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
              <!-- Start content -->
              <div class="content">
                <div class="container">
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="page-title-box">
                        <h4 class="page-title">DAFTAR PRODUK </h4>
                        <ol class="breadcrumb p-0 m-0">
                          <li>
                            <a href="#">Dashboard </a>
                          </li>
                          <li class="active">
                            Daftar Produk
                          </li>
                        </ol>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                <!-- end row -->
                <div class="row">
                  <div class="col-sm-12">
                    <div class="card-box table-responsive">
                      <table id="tb_list" class="table table-striped table-bordered dt-responsive nowrap">
                        <thead>
                          <tr>
                            <th style="text-align: center; width: 1%">No</th>
                            <th style="text-align: center; width: 10%">Katalog</th>                                                        
                            <th style="text-align: center; width: 5%">Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          $no = 0 ;
                          foreach ($data as $rowdata) {   
                            $no++;              
                            ?> 
                            <tr>
                              <td style="text-align: center;"><?php echo $no ?></td>
                              <td><center><?php echo $rowdata->katalog_nama ?> <b>(<?php echo $rowdata->total ?>)</b></center></td>                                                            
                              <td align="center">                                
                                <a href="<?php echo base_url() ?>Produk/daftar/<?php echo $rowdata->katalog_id ?>" class="btn btn-info btn-sm"><i class="glyphicon glyphicon-search"></i> | Detail Produk</a>
                              </td>
                            <?php } ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div> <!-- container -->
              </div> <!-- content -->

            </div>

          </div>
          <!-- END wrapper -->
          <script type="text/javascript">
            $(document).ready(function(){
              $('#btnsimpan').click(function() {
                $('#spinner').show();
              });
            });
          </script>
