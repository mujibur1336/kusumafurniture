            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
              <!-- Start content -->
              <div class="content">
                <div class="container">
                  <div class="row">
                    <div class="col-xs-12">
                    <?php foreach ($data as $rowdata) {
                      $nama       = $rowdata->katalog_nama;
                      $katalog_id = $rowdata->katalog_id;
                    } ?>
                      <div class="page-title-box">
                        <h4 class="page-title">DAFTAR PRODUK </h4>
                        <ol class="breadcrumb p-0 m-0">
                          <li>
                            <a href="#">Dashboard </a>
                          </li>
                          <li class="active">
                            Daftar Produk <?php echo $nama; ?>
                          </li>
                        </ol>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                  <div class="modal fade" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <form method="post" action="<?php echo site_url('produk/input');?>" enctype='multipart/form-data'>
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">TAMBAH PRODUK <?php echo $nama; ?></h4>
                          </div>
                          <div class="modal-body">
                           <!-- <?php date_default_timezone_set('Asia/Jakarta');
                           $date = new DateTime('now');?>
                           <input type="hidden" name="tgl_upload" class="form-control" value="<?php echo $date->format('Y-m-d H:i:s') ?>"> -->
                           <div class="col-lg-4">
                            <label for="txtname">Kode Produk</label>
                          </div>
                          <div class="col-lg-8" style="margin-bottom: 10px">
                            <input type="text" class="form-control" id="produk_kode" name="produk_kode" placeholder="Masukkan Kode Produk" required>
                          </div>
                           <div class="col-lg-4">
                            <label for="txtname">Nama Produk</label>
                          </div>
                          <div class="col-lg-8" style="margin-bottom: 10px">
                            <input type="text" class="form-control" id="produk_nama" name="produk_nama" placeholder="Masukkan Nama Produk" required>
                            <input type="hidden" class="form-control" id="produk_seo" name="produk_seo">
                          </div>                         
                        <div class="col-lg-4">
                            <label for="txtname">Ukuran Produk</label>
                          </div>
                          <div class="col-lg-8" style="margin-bottom: 10px">
                            <input type="text" class="form-control" id="produk_size" name="produk_size" placeholder="Masukkan Ukuran Produk" required>
                          </div>
                        <div class="col-lg-4">
                            <label for="txtname">File GAMBAR</label>
                          </div>
                          <div class="col-lg-8" style="margin-bottom: 10px">
                            <input type="file" class="form-control" name="userfile" value="">
                          </div>
                          <div class="col-lg-4">
                            <label for="txtname">Status Produk</label>
                          </div>
                          <div class="col-lg-8" style="margin-bottom: 10px">
                            <select  id="produk_status" name="produk_status" class="selectpicker form-control" required>
                             <option>-- Pilih --</option>
                             <option value="1">Ordinary</option>
                             <option value="2">Most Popular</option>  
                             <!--<option value="3">Special Offer</option>  -->
                           </select> 
                         </div>
                         <div class="col-lg-4">
                          <label for="txtname">Kategori Produk</label>
                        </div>
                        <div class="col-lg-8" style="margin-bottom: 10px">
                        <input type="text" name="" class="form-control" value="<?php echo $nama;?>" readonly>
                        <input type="hidden" name="katalog_id" class="form-control" value="<?php echo $katalog_id;?>" readonly>
                          <!-- <select  id="katalog_id" name="katalog_id" class="selectpicker form-control" data-live-search="true">
                           <option>PILIH</option>
                           <?php foreach ($katalog as $rowdata) { ?>
                           <option value="<?php echo $rowdata->katalog_id ?>"><?php echo $rowdata->katalog_id ?> | <?php echo $rowdata->katalog_nama ?></option>
                           <?php }?>
                         </select>  -->
                       </div>
                     <!--  <div class="col-lg-4">
                        <label for="txtname">Loading</label>
                      </div>
                      <div id="spinner" class="col-lg-8 spinner" style="display:none; margin-bottom: 15px">
                        <img id="img-spinner" src="<?php echo base_url() ?>assets/images/loading.gif" alt="Loading" height='15px' width='345px'/>
                      </div>-->
                    </div>
                    <div class="modal-footer" style="margin-top: 50%">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button type="submit" id="btnsimpan" name="btnsimpan" class="btn btn-success">Save</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
                <!-- end row -->
                <div class="row">
                  <div class="col-sm-12">
                    <div class="card-box table-responsive">
                      <table id="tb_produk" class="table table-striped table-bordered dt-responsive nowrap">
                        <thead>
                          <tr>
                            <th style="text-align: center; width: 1%">No</th>
                            <th style="text-align: center; width: 20%">Gambar Produk</th>
                            <th style="text-align: center; width: 10%">Kode Produk</th>
                            <th style="text-align: center; width: 10%">Nama Produk</th>
                            <th style="text-align: center; width: 10%">Ukuran Produk</th>
                            <th style="text-align: center; width: 10%">Status Produk</th>
                            <!--<th style="text-align: center; width: 10%">Produk Kategori</th>-->
                            <th style="text-align: center; width: 5%">Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          $no = 0 ;
                          foreach ($data as $rowdata) {   
                            $no++;              
                            ?> 
                            <tr>
                              <td style="text-align: center;"><?php echo $no ?></td>
                              <td><img src="<?php echo base_url();?>assets/upload_gambar/<?php echo $rowdata->produk_img;?>" width=40% high=40% alt="Kusuma Furniture"></td>
                              <td><?php echo $rowdata->produk_kode ?></td>                              
                              <td><?php echo $rowdata->produk_nama ?></td>
                              <td style="white-space: pre-wrap; word-wrap: break-word;"><?php echo $rowdata->produk_size ?></td>
                              <td> 
                              <?php if ($rowdata->produk_status==1) { ?>
                                Ordinary
                              <?php } elseif ($rowdata->produk_status==2) { ?>
                                Most Popular
                              <?php } elseif ($rowdata->produk_status==3) {?>
                                Special Offer
                              <?php } ?> 
                              </td>
                              <!--<td><?php echo $rowdata->katalog_nama ?></td>-->
                              <td align="center">                                
                                <span class="btn btn-warning btn-sm" style="font-family: serif;" data-toggle="modal" data-target="#ModalEdit<?php echo $rowdata->produk_id; ?>"><i class="fa fa-pencil"></i></span>

                                <button type="button" class="btn btn-danger btn-sm " data-toggle="modal" data-target="#ModalDelete<?php echo $rowdata->produk_id; ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
                              </td>
                              <!-- Modal Delete-->
                              <div class="modal fade" id="ModalDelete<?php echo $rowdata->produk_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <form method="post" action="<?php echo site_url('Produk/delete');?>">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel" style="font-family: sans-serif;">Konfirmasi Hapus Data</h4>
                                      </div>
                                      <div class="modal-body">
                                        <div class="form-group">
                                          <input type="hidden" id="produk_id" name="produk_id" value="<?php echo $rowdata->produk_id; ?>">
                                          <label style="font-family: sans-serif; font-size: 16px;">Apakah anda yakin, akan menghapus data : <?php echo "'<u>".$rowdata->produk_nama."</u>'";?> ?</label>
                                        </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                        <button type="submit" id="btndelete" name="btndelete" class="btn btn-danger">Hapus</button>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                              <!-- Modal EDIT-->
                              <div class="modal fade" id="ModalEdit<?php echo $rowdata->produk_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <form method="post" action="<?php echo site_url('Produk/edit');?>" enctype='multipart/form-data'>
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">EDIT DATA PRODUK</h4>
                                      </div>
                                      <div class="modal-body">                                       
                                       <input type="hidden" id="produk_id" name="produk_id" value="<?php echo $rowdata->produk_id; ?>">
                                       <div class="col-lg-4">
                                        <label for="txtname">Kode Produk</label>
                                      </div>
                                      <div class="col-lg-8" style="margin-bottom: 10px">
                                        <input type="text" class="form-control" id="produk_kode" name="produk_kode" value="<?php echo $rowdata->produk_kode; ?>" required>
                                      </div>
                                      <div class="col-lg-4">
                                        <label for="txtname">Nama Produk</label>
                                      </div>
                                      <div class="col-lg-8" style="margin-bottom: 10px">
                                        <input type="text" class="form-control" id="produk_nama" name="produk_nama" value="<?php echo $rowdata->produk_nama; ?>" required>
                                        <input type="hidden" class="form-control" id="produk_seo" name="produk_seo">
                                      </div>
                                       <div class="col-lg-4">
                                      <label for="txtname">Ukuran Produk</label>
                                    </div>
                                    <div class="col-lg-8" style="margin-bottom: 10px">
                                      <input type="text" class="form-control" id="produk_size" name="produk_size" 
                                      value="<?php echo $rowdata->produk_size; ?>" required>
                                    </div>                                                                   
                                    <div class="col-lg-4">
                                      <label for="txtname">File GAMBAR</label>
                                    </div>
                                    <div class="col-lg-8" style="margin-bottom: 10px">
                                      <img src="<?php echo base_url();?>assets/upload_gambar/<?php echo $rowdata->produk_img;?>" width=50% high=50% alt="Kusuma Furniture">
                                    </div>
                                    <div class="col-lg-4">
                                      <label for="txtname">File Upload Gambar</label>
                                    </div>
                                    <div class="col-lg-8" style="margin-bottom: 10px">
                                      <input type="file" class="form-control" name="userfile" value="">
                                    </div>
                                    <div class="col-lg-4">
                                      <label for="txtname">Status Produk</label>
                                    </div>
                                    <div class="col-lg-8" style="margin-bottom: 10px">
                                      <select id="produk_status" name="produk_status" class="form-control" data-live-search="true" value="<?php echo $rowdata->produk_status; ?>">
                                       <option value="<?php echo $rowdata->produk_status; ?>"><?php echo $rowdata->produk_status; ?></option>
                                       <option value="1">Ordinary</option>
                                       <option value="2">Most Popular</option>  
                                       <!--<option value="3">Special Offer</option>  -->
                                     </select> 
                                   </div> 
                                   <div class="col-lg-4">
                                    <label for="txtname">Kategori Produk</label>
                                  </div>
                                  <div class="col-lg-8" style="margin-bottom: 10px">
                                  <input type="text" name="" class="form-control" value="<?php echo $rowdata->katalog_nama ?>" readonly>
                                  <input type="hidden" name="katalog_id" class="form-control" value="<?php echo $rowdata->katalog_id;?>" readonly>
                                  <!-- <select  id="katalog_id" name="katalog_id" class="form-control" data-live-search="true">
                                     <option value="<?php echo $rowdata->katalog_id ?>"><?php echo $rowdata->katalog_id ?></option>
                                     <?php foreach ($katalog as $rowdata) { ?>
                                     <option value="<?php echo $rowdata->katalog_id ?>"><?php echo $rowdata->katalog_id ?> | <?php echo $rowdata->katalog_nama ?></option>
                                     <?php }?>
                                   </select>  -->
                                 </div>                                  
                                <!-- <div class="col-lg-4">
                                  <label for="txtname">Loading</label>
                                </div>
                                <div id="spinner" class="col-lg-8 spinner" style="display:none; margin-bottom: 15px">
                                  <img id="img-spinner" src="<?php echo base_url() ?>assets/images/loading.gif" alt="Loading" height='15px' width='345px'/>
                                    </div>-->
                                    <div class="modal-footer" style="margin-top: 95%">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      <button type="submit" id="btnedit" name="btnedit" class="btn btn-success">Simpan</button>
                                    </div>
                                  </form>
                                </div>
                              </div>
                            </div>                                                        
                          </tr>
                            <?php } ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div> <!-- container -->
              </div> <!-- content -->

            </div>

          </div>
          <!-- END wrapper -->
          <script type="text/javascript">
            $(document).ready(function(){
              $('#btnsimpan').click(function() {
                $('#spinner').show();
              });
            });
          </script>
