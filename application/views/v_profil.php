            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
              <!-- Start content -->
              <div class="content">
                <div class="container">
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="page-title-box">
                        <h4 class="page-title">DAFTAR PROFIL PERUSAHAAN</h4>
                        <ol class="breadcrumb p-0 m-0">
                          <li>
                            <a href="#">Dashboard </a>
                          </li>
                          <li class="active">
                            Daftar Profil Perusahaan
                          </li>
                        </ol>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                    
                    <form id="formInfo" action="<?php echo site_url('Kategori/profil_edit');?>" method="POST">                    
                     <div class="col-sm-12" align="right">                     
                      <button type="submit" id="btnedit" name="btnedit" class="btn btn-sm btn-success"><i class="fa fa-plus-circle"></i> | SIMPAN DATA</button> 
                    </div>
                    <div class="col-sm-12">
                      <?php echo  $this->session->flashdata('pesan'); ?>
                    </div>
                    <?php foreach ($data as $rowdata) {  ?>
                      <div class="col-lg-2">
                         <label for="txtname">Nama Perusahaan</label>
                       </div>
                       <div class="col-lg-4" style="margin-bottom: 10px">
                         <input type="text" class="form-control" id="profil_nama" name="profil_nama" value="<?php echo $rowdata->profil_nama ?>">
                         <input type="hidden" class="form-control" id="profil_id" name="profil_id" value="<?php echo $rowdata->profil_id ?>">
                       </div>

                       <div class="col-lg-2">
                         <label for="txtname">Alamat Perusahaan</label>
                       </div>
                       <div class="col-lg-4" style="margin-bottom: 10px">
                         <input type="text" class="form-control" id="profil_alamat" name="profil_alamat" value="<?php echo $rowdata->profil_alamat ?>"></div>
                         <div class="col-lg-2">
                         <label for="txtname">Telpon Perusahaan</label>
                       </div>
                       <div class="col-lg-4" style="margin-bottom: 10px">
                         <input type="text" class="form-control" id="profil_telp" name="profil_telp" value="<?php echo $rowdata->profil_telp ?>">     </div>
                         <div class="col-lg-2">
                         <label for="txtname">FAQ Perusahaan</label>
                       </div>
                       <div class="col-lg-4" style="margin-bottom: 10px">
                         <input type="text" class="form-control" id="profil_faq" name="profil_faq" value="<?php echo $rowdata->profil_faq ?>">     </div>

                       <div class="col-lg-2">
                         <label for="txtname">Alamat Pabrik</label>
                       </div>
                       <div class="col-lg-4" style="margin-bottom: 10px">
                         <input type="text" class="form-control" id="profil_alamat_pabrik" name="profil_alamat_pabrik" value="<?php echo $rowdata->profil_alamat_pabrik ?>">
                         </div>

                       <div class="col-lg-2">
                         <label for="txtname">Alamat Registrasi</label>
                       </div>
                       <div class="col-lg-4" style="margin-bottom: 10px">
                         <input type="text" class="form-control" id="profil_alamat_register" name="profil_alamat_register" value="<?php echo $rowdata->profil_alamat_register ?>">
                         </div>
                         <div class="col-lg-2">
                         <label for="txtname">Sertifikat Perusahaan</label>
                       </div>
                       <div class="col-lg-4" style="margin-bottom: 10px">
                         <input type="text" class="form-control" id="profil_sertifikat" name="profil_sertifikat" value="<?php echo $rowdata->profil_sertifikat ?>">                        
                       </div>

                       <div class="col-lg-2">
                         <label for="txtname">Grade Perusahaan</label>
                       </div>
                       <div class="col-lg-4" style="margin-bottom: 10px">
                         <input type="text" class="form-control" id="profil_grade" name="profil_grade" value="<?php echo $rowdata->profil_grade ?>">
                         </div>
                         <div class="col-lg-2">
                         <label for="txtname">Material</label>
                       </div>
                       <div class="col-lg-4" style="margin-bottom: 10px">
                         <input type="text" class="form-control" id="profil_material" name="profil_material" value="<?php echo $rowdata->profil_material ?>">                        
                       </div>
                       <div class="col-lg-2">
                         <label for="txtname">Kapasitas</label>
                       </div>
                       <div class="col-lg-4" style="margin-bottom: 10px">
                         <input type="text" class="form-control" id="profil_kapasitas" name="profil_kapasitas" value="<?php echo $rowdata->profil_kapasitas ?>">
                         </div>
                         <div class="col-lg-2">
                         <label for="txtname">Packing</label>
                       </div>
                       <div class="col-lg-4" style="margin-bottom: 10px">
                         <input type="text" class="form-control" id="profil_packing" name="profil_packing" value="<?php echo $rowdata->profil_packing?>">                        
                       </div>
                       <div class="col-lg-2">
                         <label for="txtname">WEB</label>
                       </div>
                       <div class="col-lg-4" style="margin-bottom: 10px">
                         <input type="text" class="form-control" id="profil_web" name="profil_web" value="<?php echo $rowdata->profil_web ?>">
                         </div>
                    <?php } ?>

                    </form>                    
                  </div>
                </div>
              </div>
            </div>
          </div> <!-- container -->

        </div> <!-- content -->

      </div>

    </div>
    <!-- END wrapper -->
