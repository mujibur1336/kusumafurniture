<!DOCTYPE html>
 <html lang="en-US">
 
 <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!--<meta name="description" content="furniture minimalis, Teak furniture, Teak garden furniture, Teak outdoor furniture, teak patio furniture, teak, furniture, garden, patio, indonesia teak outdoor furniture">-->
        <!--| Teak garden furniture manufacture Indonesia with best quality on competitive prices. Feel safe buy furniture from us with our guarantee for all products.-->
    <meta name="author" content="Wahyu">
    
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-P5FP4FQS');</script>
    <!-- End Google Tag Manager -->
    <?php foreach($katalog as $key){
                        $katalog_judul = $key->meta_judul;
                        $katalog_deskripsi = $key->meta_deskripsi;
                        $katalog_seo = $key->katalog_seo;
                    } ?>
    
    <script>window._wca = window._wca || [];</script>
    <link rel="icon" href="<?php echo base_url() ?>assets/frontend/img/icon.png" type="image/png"> 
    <title><?= $katalog_judul ?></title>
	<meta name="description" content= "<?= $katalog_deskripsi ?>" />
	<link rel="alternate" hreflang="en-us"  href = "https://www.kusumafurniture.com/teak-furniture/product/<?= $katalog_seo ?>"/>
	<link rel="alternate" hreflang="x-default" href = "https://www.kusumafurniture.com/teak-furniture/product/<?= $katalog_seo ?>"/>
	<link rel="canonical" href="https://www.kusumafurniture.com/teak-furniture/product/<?= $katalog_seo ?>"/>
    <meta property="og:locale" content="en_US" />
	<meta property="og:type" content="website" />
	<meta property="og:title" content= "<?= $katalog_judul ?>" />
	<meta property="og:description" content= "<?= $katalog_deskripsi ?>" />
	<meta property="og:url" content="https://www.kusumafurniture.com/" />
	<meta property="og:site_name" content="Kusuma Furniture" />
	<meta property="article:modified_time" content="2022-12-07T04:07:04+00:00" />
	<meta name="keywords" content="teak furniture , teak garden furniture , teak outdoor furniture , teak patio furniture, teak, furniture, garden, patio, teak furniture garden patio , indonesia teak outdoor furniture , furniture minimalis"/>
	<!--<meta property="og:image" content="https://indonesia-furnitures.com/wp-content/uploads/2022/06/web-svlk-1024x661.jpg" />-->
	<meta name="twitter:card" content="summary_large_image" />
	<meta name="twitter:label1" content="Est. reading time" />
	<meta name="twitter:data1" content="5 minutes" />
    <script type="application/ld+json" class="yoast-schema-graph">{"@context":"https://schema.org","@graph":[{"@type":"WebPage","@id":"https://kusumafurniture.com/","url":"https://kusumafurniture.com/","name":"Kusuma Furnitures | Teak Garden Furniture - Teak Outdoor Furniture","isPartOf":{"@id":"https://www.kusumafurniture.com/"},"about":{"@id":"https://kusumafurniture.com/about"},"primaryImageOfPage":{"@id":"https://www.kusumafurniture.com/"},"image":{"@id":"https://www.kusumafurniture.com/"},"description":"teak furniture , teak garden furniture , teak outdoor furniture , teak patio furniture, teak, furniture, garden, patio, indonesia teak outdoor furniture","breadcrumb":{"@id":"https://www.kusumafurniture.com/"},"inLanguage":"en-US","potentialAction":[{"@type":"ReadAction","target":["https://www.kusumafurniture.com/"]}]},{"@type":"ImageObject","inLanguage":"en-US","@id":"https://www.kusumafurniture.com/","url":"https://kusumafurniture.com/assets/frontend/img/logo-kusuma-2.png","contentUrl":"https://www.kusumafurniture.com/assets/frontend/img/logo-kusuma-2.png","width":1205,"height":778},{"@type":"BreadcrumbList","@id":"https://www.kusumafurniture.com/","itemListElement":[{"@type":"ListItem","position":1,"name":"Home"}]},{"@type":"WebSite","@id":"https://kusumafurniture.com/about","url":"https://kusumafurniture.com/","name":"Kusuma Furniture","description":"teak furniture , teak garden furniture , teak outdoor furniture , teak patio furniture , teak , furniture , garden , patio , indonesia teak outdoor furniture","publisher":{"@id":"https://www.kusumafurniture.com/"},"potentialAction":[{"@type":"SearchAction","target":{"@type":"EntryPoint","urlTemplate":"https://kusumafurniture.com/?s={search_term_string}"},"query-input":"required name=search_term_string"}],"inLanguage":"en-US"},{"@type":"Organization","@id":"https://www.kusumafurniture.com/","name":"Kusuma Furniture","url":"https://kusumafurniture.com/","logo":{"@type":"ImageObject","inLanguage":"en-US","@id":"https://kusumafurniture.com/assets/frontend/img/","url":"https://kusumafurniture.com/assets/frontend/img/logo-kusuma-2.png","contentUrl":"https://kusumafurniture.com/assets/frontend/img/logo-kusuma-2.png","width":959,"height":226,"caption":"Kusuma Furniture"},"image":{"@id":"https://www.kusumafurniture.com/"}}]}</script>
    <meta name="google-site-verification" content="VjlpqhpJAUFzIwm9Xjwg0E0usFClG3Xj5R4QFbWZZQc"/>
    
   <meta name="robots" content="follow, index, max-snippet:-1, max-video-preview:-1, max-image-preview:large"/>
     
    <!-- site Favicon -->
    <link rel="icon" href="<?php echo base_url() ?>assets/frontend/img/icon1.png" sizes="32x32" />
    <link rel="apple-touch-icon" href="<?php echo base_url() ?>assets/frontend/img/icon1.png" />
    <meta name="msapplication-TileImage" content="<?php echo base_url() ?>assets/frontend/img/icon1.png" />

    <!-- css Icon Font -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/frontend_baru/css/vendor/ecicons.min.css" />

    <!-- css All Plugins Files -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/frontend_baru/css/plugins/animate.css" />
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/frontend_baru/css/plugins/swiper-bundle.min.css" />
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/frontend_baru/css/plugins/jquery-ui.min.css" />
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/frontend_baru/css/plugins/countdownTimer.css" />
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/frontend_baru/css/plugins/slick.min.css" />
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/frontend_baru/css/plugins/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/frontend_baru/css/plugins/nouislider.css" />
 

    <!-- Main Style -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/frontend_baru/css/demo3.css" />
    <!-- <link rel="stylesheet" href="<?php echo base_url() ?>assets/frontend_baru/css/style.css" /> -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/frontend_baru/css/responsive.css" /> 
 
     <!-- Background css -->
    <link rel="stylesheet" id="bg-switcher-css" href="<?php echo base_url() ?>assets/frontend_baru/css/backgrounds/bg-4.css">
    
    <script type="application/ld+json">
        {
          "@context": "https://schema.org/", 
          "@type": "Product", 
          "name": "<?= $katalog_judul ?>",
          "image": "<?php echo base_url() ?>assets/frontend_baru/images/logo/logo-kusuma-2.png",
          "description": "<?= $katalog_deskripsi ?>",
          "brand": {
            "@type": "Brand",
            "name": "Kusuma Furniture"
          },
          "sku": "1",
          "offers": {
            "@type": "Offer",
            "url": "<?php echo base_url().'teak-furniture/product/'.$katalog_seo ?>",
            "priceCurrency": "USD",
            "price": "0"
          },
          "aggregateRating": {
            "@type": "AggregateRating",
            "ratingValue": "5",
            "bestRating": "5",
            "worstRating": "1",
            "ratingCount": "89"
          }
        }
        </script>
    
</head>

<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P5FP4FQS"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    
    <!-- Header start  -->
    <header class="ec-header">
        <!--Ec Header Top Start -->
        <nav class="navbar fixed-top justify-content-center navbar-expand-lg" style="margin-bottom: 230px; background-color:#fff;"> 
        <div class="header-top">
            <div class="container">
             <!--<nav class="navbar fixed-top bg-light" > -->
                <div class="row align-items-center">
                    <div class="col header-top-res d-lg-none">
                        <div class="ec-header-bottons">
                            <!-- Header menu Start -->
                            <a href="#ec-mobile-menu" title="Teak Furniture" class="ec-header-btn ec-side-toggle ec-d-l d-lg-none">
                                <i class="ecicon eci-bars"></i>
                            </a>
                            <!-- Header menu End -->
                        </div>
                    </div>
                    <!-- Header Top responsive Action -->
                </div>
             <!--</nav> -->
            </div>
            <div class="ec-header-bottom d-lg-none">
                <div class="container position-relative">
                    <div class="row ">
                        <!-- Ec Header Logo Start -->
                        <div class="col">
                            <div class="header-logo">
                                <a href="<?php echo base_url() ?>" title="Teak Furniture" ><img src="<?php echo base_url() ?>assets/frontend_baru/images/logo/logo-kusuma-2.png" alt="Teak Furniture" title="Teak Furniture" style="width:50%;" />
                                    <img class="dark-logo" src="<?php echo base_url() ?>assets/frontend_baru/images/logo/logo-kusuma-2.png" alt="teak outdoor furniture" title="teak outdoor furniture" style="display: none; width:100%;" /></a>
                            </div>
                        </div>
                        <!-- Ec Header Logo End -->
                    </div>
                </div>
            </div>
        </div>
        </nav>
        <!-- EC Main Menu Start -->
        <nav class="navbar fixed-top bg-light justify-content-center navbar-expand-lg" style="margin-bottom: 45px;"> 
        <div id="ec-main-menu-desk" class="sticky-nav">
            <div class="container position-relative ">
                <div class="row">
                    <div class="col-sm-12 ec-main-menu-block align-self-center d-none d-lg-block">
                        <div class="ec-main-menu">
                            <ul> 
                                <li><a href="<?php echo base_url() ?>" title="Teak Furniture" ><img src="<?php echo base_url() ?>assets/frontend_baru/images/logo/logo-kusuma-2.png" style="width:60%;" alt="teak furniture" title="teak furniture"><img class="dark-footer-logo" src="assets/frontend_baru/images/logo/logo-kusuma-2.png" alt="teak garden furniture" title="teak garden furniture" style="display: none; width:40%;" /></a></li>
                                <li><a href="<?php echo base_url() ?>" title="Teak Furniture" >Home</a></li>
                                <li><a href="<?php echo base_url() ?>about" title="Teak Furniture" >About</a></li>
                                <li class="dropdown"><a href="#" title="Teak Garden Furniture" >Product</a>
                                    <ul class="sub-menu">
                                    <?php 
                                        foreach ($data1 as $rowdata) {              
                                                ?> 
                                                    <li class="nav-item"><a class="nav-link" href="<?php echo base_url().'teak-furniture/product/'.$rowdata->katalog_seo ?>" title="Teak Garden Furniture" ><span style="font-size: 13px; font-weight: bold;"><?php echo $rowdata->katalog_nama ?><b> (<?php echo $rowdata->total ?>)</b></span></a></li>
                                    <?php } ?>
                                    </ul>
                                </li>
                                <li><a href="<?php echo base_url() ?>special-offer" title="Teak Patio Furniture" >Special Offer</a></li>
                                <li><a href="<?php echo base_url() ?>article/page" title="Teak Outdoor Furniture" >News</a>
                                </li>
                                <li><a href="<?php echo base_url() ?>contact" title="Teak Furniture" >Contact Us</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </nav> 
        <!-- Ec Main Menu End -->
          <!-- Ekka Menu Start -->
          <div id="ec-mobile-menu" class="ec-side-cart ec-mobile-menu" style="margin-top: 100px;">
            <div class="ec-menu-title">
                <span class="menu_title">My Menu</span>
                <button class="ec-close">×</button>
            </div>
            <div class="ec-menu-inner">
                <div class="ec-menu-content">
                    <ul>
                        <li><a href="<?php echo base_url() ?>" title="Teak Furniture" >Home</a></li>
                        <li><a href="<?php echo base_url() ?>about" title="Teak Furniture" >About</a></li>
                        <li class="dropdown"><a href="#" title="Teak Furniture" >Product</a>
                            <ul class="sub-menu">
                                <?php foreach ($data1 as $rowdata) {?> 
                                    <li class="nav-item"><a class="nav-link" href="<?php echo base_url().'teak-furniture/product/'.$rowdata->katalog_seo ?>" title="Teak Patio Furniture" ><span style="font-size: 13px; font-weight: bold;"><?php echo $rowdata->katalog_nama ?><b> (<?php echo $rowdata->total ?>)</b></span></a></li>
                                <?php } ?>
                            </ul>
                        </li>
                        <li><a href="<?php echo base_url() ?>special-offer" title="Teak Furniture" >Special Offer</a></li>
                        <li><a href="<?php echo base_url() ?>article/page" title="Teak Garden Furniture" >News</a></li>
                        <li><a href="<?php echo base_url() ?>contact" title="Teak Patio Furniture" >Contact Us</a></li>
                    </ul>  
                </div>
                <!--<div class="header-res-lan-curr">-->
                <!--    <div class="header-res-social">-->
                <!--        <div class="header-top-social">-->
                <!--            <ul class="mb-0">-->
                <!--                <li class="list-inline-item"><a href="#" title="Teak Furniture" ><i class="ecicon eci-facebook"></i></a></li>-->
                <!--                <li class="list-inline-item"><a href="#" title="Teak Furniture" ><i class="ecicon eci-twitter"></i></a></li>-->
                <!--                <li class="list-inline-item"><a href="#" title="Teak Furniture" ><i class="ecicon eci-instagram"></i></a></li>-->
                <!--                <li class="list-inline-item"><a href="#" title="Teak Furniture" ><i class="ecicon eci-linkedin"></i></a></li>-->
                <!--            </ul>-->
                <!--        </div>-->
                <!--    </div>-->
                <!--</div>-->
            </div>
        </div>
  
        <!-- Ekka Menu End -->
        
    </header>
    <!-- Header End  -->
