<!-- Footer Start -->
<footer class="ec-footer">
    <div class="footer-container">
        <div class="footer-offer">
            <div class="container">
                <div class="row">
                    <div class="text-center footer-off-msg">
                        <marquee><a href="https://www.kusumafurniture.com" title="Teak Furniture">Kusuma Furniture Indonesia </a><?php foreach($data1 as $rowdata){?><a href= "<?php echo base_url().'teak-furniture/product/'.$rowdata->katalog_seo ?>" title="Teak Furniture"> <?= $rowdata->katalog_nama ?></a><?php } ?></marquee>
                         <!--<a href="https://www.kusumafurniture.com" title="Teak Furniture">Teak Furniture</a>  | <a href="https://www.kusumafurniture.com/" title="Teak Garden Furniture">Teak Garden Furniture</a> | <a href="https://www.kusumafurniture.com/" title="Teak Outdoor Furniture">Teak Outdoor Furniture</a> | <a href="https://www.kusumafurniture.com/" title="Teak Patio Furniture">Teak Patio Furniture</a>-->
                        <!-- <a href="#" target="_blank">View
                                Detail</a> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-top section-space-footer-p">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-lg-2 ec-footer-contact">
                        <div class="ec-footer-widget">
                            <div class="ec-footer-logo"><a href="https://www.kusumafurniture.com" title="Teak Furniture"><img src="<?php echo base_url() ?>assets/frontend_baru/images/logo/logo-kusuma-2.png" alt="teak garden furniture" title="teak garden furniture"><img class="dark-footer-logo" src="<?php echo base_url() ?>assets/frontend_baru/images/logo/logo-kusuma-2.png" alt="teak furniture" title="teak furniture" style="display: none;" /></a></div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-4 ec-footer-service">
                        <div class="ec-footer-widget">
                            <h4 class="ec-footer-heading">Location</h4>
                            <div class="ec-footer-links">
                                <ul class="align-items-center">
                                    <li class="ec-footer-link">Registered : Jl. Dr. Susanto No. 81 Pati, Indonesia.</li>
                                    <li class="ec-footer-link">Operational : Jl. Penjawi 459A Pati, Indonesia</li>
                                    <li class="ec-footer-link">Factory : Ds. Ngabul RT.2 / RW.7 Ngabul Jepara, Indonesia</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-3 ec-footer-news">
                        <div class="ec-footer-widget">
                            <h4 class="ec-footer-heading">Contact Us</h4>
                            <div class="ec-footer-links">
                                <ul class="align-items-center">
                                    <li class="ec-footer-link">Office Phone : (62) 295 383411</li>
                                    <li class="ec-footer-link">Factory Phone : (62) 291 4260088</li>
                                    <a href="https://api.whatsapp.com/send?phone=6289679724000&text=Hello%20Admin%20I%20want%20ask....." title="Teak Furniture" target="_blank"><li class="ec-footer-link">Whatsapp Phone : (62) 896 7972 4000</li></a>
                                    <!--<li class="ec-footer-link">Web : <a href="https://www.kusumafurniture.com/" title="Teak Furniture">https://www.kusumafurniture.com/</a></li>-->
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-lg-3 ec-footer-info">
                        <div class="ec-footer-widget">
                            <h4 class="ec-footer-heading">Information</h4>
                            <div class="ec-footer-links">
                                <ul class="align-items-center">
                                    <li class="ec-footer-link"><a href="<?php echo base_url() ?>article/page" title="Teak Furniture">News</a></li>
                                    <li class="ec-footer-link"><a href="<?php echo base_url() ?>special-offer" title="Teak Garden Furniture">Special Offer</a></li>
                                    <li class="ec-footer-link"><a href="<?php echo base_url() ?>about" title="Teak Outdoor Furniture">About us</a></li>
                                    <li class="ec-footer-link"><a href="<?php echo base_url() ?>about" title="Teak Patio Furniture">Frequently Asked</a></li>
                                    <!--<li class="ec-footer-link"><a href="<?php echo base_url() ?>about" title="Teak Outdoor Furniture">Payment</a>-->
                                    <li class="ec-footer-link"><a href="<?php echo base_url() ?>contact" title="Teak Patio Furniture">Contact us</a>
                                    </li>
                                </ul>
                                <div class="col-sm-12 col-lg-6 ec-footer-social">
                                    <div class="ec-footer-widget">
                                        <div class="ec-footer-links">
                                            <ul class="align-items-center">
                                                <li class="list-inline-item"><a href="https://www.facebook.com/kusumafurnitureindonesia" title="Teak Furniture"><i class="ecicon eci-facebook"></i></a>
                                                </li>
                                                <li class="list-inline-item"><a href="https://twitter.com/KusumaFurniture" title="Teak Garden Furniture"><i class="ecicon eci-twitter"></i></a>
                                                </li>
                                                <li class="list-inline-item"><a href="https://www.instagram.com/kusuma.furniture/" title="Teak Outdoor Furniture"><i class="ecicon eci-instagram"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="row align-items-center">
                    <!-- Footer Copyright Start -->
                    <div class="col footer-copy">
                        <div class="footer-bottom-copy text-center ">
                            <div class="ec-copy">Copyright © 2024 <a class="site-name text-upper" href="https://www.kusumafurniture.com" title="Teak Furniture">Kusuma Furniture</a>. Indonesia</div>
                        </div>
                    </div>
                    <!-- Footer Copyright End -->
                    <!-- Footer payment -->
                    <!-- <div class="col footer-bottom-right">
                        <div class="footer-bottom-payment d-flex justify-content-end">
                            <div class="payment-link">
                                <img src="<?php echo base_url() ?>assets/frontend_baru/images/icons/payment.png" alt="">
                            </div>

                        </div>
                    </div> -->
                    <!-- Footer payment -->
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Footer Area End -->



<!-- FB Chat Messenger -->
<div class="ec-fb-style fb-right-bottom">

    <!-- Start Floating Panel Container -->
    <div class="fb-panel">
        <!-- Panel Content -->
        <div class="fb-header">
            <img src="<?php echo base_url() ?>assets/frontend_baru/images/user/1.jpg" alt="teak furniture" title="teak furniture"/>
            <!--<h2>Kusuma Furniture</h2>-->
            <p style="font-size: 22px">Kusuma Furniture</p>
            <p>Admin</p>
        </div>
        <div class="fb-body">
            <p><b>Hey there &#128515;</b></p>
            <p>Need help ? just send me a message.</p>
        </div>
        <div class="fb-footer">

            <a href="https://api.whatsapp.com/send?phone=6289679724000&text=Hello%20Admin%20I%20want%20ask....." title="Teak Furniture" target="_blank" rel="noopener noreferrer" class="fb-msg-button">
                <span>Send Message</span>
                <svg width="13px" height="10px" viewBox="0 0 13 10">
                    <path d="M1,5 L11,5"></path>
                    <polyline points="8 1 12 5 8 9"></polyline>
                </svg>
            </a>

        </div>
    </div>
    <!--/ End Floating Panel Container -->

    <!-- Start Right Floating Button -->
    <div class="fb-button fb-right-bottom">
        <img class="fa-whatsapp" src="<?php echo base_url() ?>assets/frontend_baru/images/icons/whatsapp.png" alt="teak outdoor furniture" title="teak outdoor furniture" />
    </div>
    <!--/ End Right Floating Button -->

</div>
<!-- FB Chat Messenger end -->

<!-- Newsletter Modal Start -->
<!-- <div id="ec-popnews-bg"></div>
<div id="ec-popnews-box">
    <div id="ec-popnews-close"><i class="ecicon eci-close"></i></div>
    <div id="ec-popnews-box-content">
        <h1>Subscribe Newsletter</h1>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        <form id="ec-popnews-form" action="#" method="post">
            <input type="email" name="newsemail" placeholder="Email Address" required />
            <button type="button" class="btn btn-secondary" name="subscribe">Subscribe</button>
        </form>
    </div>
</div> -->
<!-- Newsletter Modal end -->
 <!-- Feature tools -->
 <div class="ec-tools-sidebar-overlay"></div>
    <div class="ec-tools-sidebar">
        <div class="tool-title">
            <h3>Features</h3>
        </div>
        <a href="#" title="Teak Furniture" class="ec-tools-sidebar-toggle in-out">
            <img alt="teak outdoor furniture" src="<?php echo base_url() ?>assets/frontend_baru/images/common/language.png" title="teak outdoor furniture"/>
        </a>
        <div class="ec-tools-detail">
            
           
            <div class="ec-tools-sidebar-content">
                <h3>Language</h3>
                <div class="ec-change-lang">
                    <span id="google_translate_element"></span>
                </div>
            </div>
            
        </div>
    </div>
    <!-- Feature tools end -->

<!-- Vendor JS -->
<script src="<?php echo base_url() ?>assets/frontend_baru/js/vendor/jquery-3.5.1.min.js"></script>
<script src="<?php echo base_url() ?>assets/frontend_baru/js/vendor/popper.min.js"></script>
<script src="<?php echo base_url() ?>assets/frontend_baru/js/vendor/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/frontend_baru/js/vendor/jquery-migrate-3.3.0.min.js"></script>
<script src="<?php echo base_url() ?>assets/frontend_baru/js/vendor/modernizr-3.11.2.min.js"></script>


<!--Plugins JS-->
<script src="<?php echo base_url() ?>assets/frontend_baru/js/plugins/swiper-bundle.min.js"></script>
<script src="<?php echo base_url() ?>assets/frontend_baru/js/plugins/countdownTimer.min.js"></script>
<script src="<?php echo base_url() ?>assets/frontend_baru/js/plugins/scrollup.js"></script>
<script src="<?php echo base_url() ?>assets/frontend_baru/js/plugins/jquery.zoom.min.js"></script>
<script src="<?php echo base_url() ?>assets/frontend_baru/js/plugins/slick.min.js"></script>
<script src="<?php echo base_url() ?>assets/frontend_baru/js/plugins/infiniteslidev2.js"></script>
<script src="<?php echo base_url() ?>assets/frontend_baru/js/plugins/fb-chat.js"></script>
<script src="<?php echo base_url() ?>assets/frontend_baru/js/vendor/jquery.magnific-popup.min.js"></script>
<script src="<?php echo base_url() ?>assets/frontend_baru/js/plugins/jquery.sticky-sidebar.js"></script>
<script src="<?php echo base_url() ?>assets/frontend_baru/js/plugins/nouislider.js"></script>
<!--Plugins JS-->

<!-- Google translate Js -->
<!--<script src="<?php echo base_url() ?>assets/frontend_baru/js/vendor/google-translate.js"></script>-->
<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<script>
    function googleTranslateElementInit() {
        new google.translate.TranslateElement({
            pageLanguage: 'en'
        }, 'google_translate_element');
    }
    // function googleTranslateElementInit1() {
    //     new google.translate.TranslateElement({
    //         pageLanguage: 'en'
    //     }, 'google_translate_element1');
    // }
</script>
<!-- Main Js -->
<script src="<?php echo base_url() ?>assets/frontend_baru/js/vendor/index.js"></script>
<script src="<?php echo base_url() ?>assets/frontend_baru/js/demo-3.js"></script>
</body>

</html>