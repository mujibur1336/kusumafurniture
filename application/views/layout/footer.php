<footer class="footer text-right">
    <strong style="font-family: sans-serif;"> Copyright &copy; 2020, TIM IT KUSUMA FURNITURE </strong>
</footer>
<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/detect.js"></script>
<script src="<?php echo base_url() ?>assets/js/fastclick.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.blockUI.js"></script>
<script src="<?php echo base_url() ?>assets/js/waves.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.slimscroll.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/switchery/switchery.min.js"></script>

<!-- Datatable  -->
<script src="<?php echo base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/datatables/buttons.bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/datatables/jszip.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/datatables/pdfmake.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/datatables/vfs_fonts.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/datatables/buttons.print.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/datatables/dataTables.keyTable.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/datatables/dataTables.scroller.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/datatables/dataTables.colVis.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/datatables/dataTables.fixedColumns.min.js"></script>

<!-- init -->
<script src="<?php echo base_url() ?>assets/pages/jquery.datatables.init.js"></script>

<!-- Counter js  -->
<script src="<?php echo base_url() ?>assets/plugins/waypoints/jquery.waypoints.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/counterup/jquery.counterup.min.js"></script>

<!--Morris Chart-->
<script src="<?php echo base_url() ?>assets/plugins/morris/morris.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/raphael/raphael-min.js"></script>

<!-- Dashboard init -->
<script src="<?php echo base_url() ?>assets/pages/jquery.dashboard.js"></script>

<!-- App js -->
<script src="<?php echo base_url() ?>assets/js/jquery.core.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.app.js"></script>

<!-- Modal-Effect -->
<script src="<?php echo base_url() ?>assets/plugins/custombox/js/custombox.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/custombox/js/legacy.min.js"></script>

<!--Form Wizard-->
<script src="<?php echo base_url() ?>assets/plugins/jquery.steps/js/jquery.steps.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<!--wizard initialization-->
<script src="<?php echo base_url() ?>assets/pages/jquery.wizard-init.js"></script>

<!-- Jquery ui js -->
<script src="<?php echo base_url() ?>assets/plugins/jquery-ui/jquery-ui.min.js"></script>

<script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
<script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/js/dataTables.checkboxes.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
 <!--Wysiwig js-->
<script src="<?php echo base_url() ?>assets/plugins/tinymce/tinymce.min.js"></script>
<!-- Datatable Kelas -->
<script>
            $(document).ready(function () {
                if($("#elm1").length > 0){
                    tinymce.init({
                        selector: "textarea#elm1",
                        theme: "modern",
                        height:300,
                        plugins: [
                            "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                            "save table contextmenu directionality emoticons template paste textcolor"
                        ],
                        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
                        style_formats: [
                            {title: 'Bold text', inline: 'b'},
                            {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                            {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                            {title: 'Example 1', inline: 'span', classes: 'example1'},
                            {title: 'Example 2', inline: 'span', classes: 'example2'},
                            {title: 'Table styles'},
                            {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
                        ]
                    });
                }
            });
        </script>
         <script>
    $(document).ready(function() {
        $('#summernote').summernote();
    });
  </script>
<script>
    $(function () {
                // sortable
                $(".sortable").sortable({
                    connectWith: '.sortable',
                    items: '.card-draggable',
                    revert: true,
                    placeholder: 'card-draggable-placeholder',
                    forcePlaceholderSize: true,
                    opacity: 0.77,
                    cursor: 'move'
                });
            });
    $(document).ready(function () {
        $('table.display').dataTable({
            "language": {
                "lengthMenu": 'Tampilkan _MENU_ Baris',
                "search": '_INPUT_',
                "searchPlaceholder": 'Pencarian . . .'
            },
            "aLengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "All"]],
            "pageLength": 5,
            dom: 
            "<'row'<'col-md-8'l><'col-md-2 pull-right'f>>" +
            "<'row'<'col-md-12't>>" +
            "<'row'<'col-md-5'i><'col-md-7'p>>"
        });
           $('#tb_spesial').DataTable({
            "language": {
                "lengthMenu": 'Tampilkan _MENU_ Baris',
                "search": '_INPUT_',
                "searchPlaceholder": 'Pencarian . . .'
            },
            dom: 
            "<'row'<'col-md-8'l><'col-md-2 pull-right'f> <'col-md- pull-right' B>>" +
            "<'row'<'col-md-12't>>" +
            "<'row'<'col-md-5'i><'col-md-7'p>>",
            buttons: [
            {
                "text":'<i class="fa fa-plus"></i> TAMBAH PRODUK',"className": 'btn btn-success btn-sm',
                action: function ( e, dt, node, config ) {
                    var selected = dt.row( { selected: true } ).data();
                    $('#ModalAdd').modal('show');
                }
            }
            ]
        });
          $('#tb_info').DataTable({
            "language": {
                "lengthMenu": 'Tampilkan _MENU_ Baris',
                "search": '_INPUT_',
                "searchPlaceholder": 'Pencarian . . .'
            },
            dom: 
            "<'row'<'col-md-8'l><'col-md-2 pull-right'f> <'col-md- pull-right' B>>" +
            "<'row'<'col-md-12't>>" +
            "<'row'<'col-md-5'i><'col-md-7'p>>",
            buttons: [
            {
                "text":'<i class="fa fa-plus"></i> TAMBAH PROFIL INFO',"className": 'btn btn-success btn-sm',
                action: function ( e, dt, node, config ) {
                    var selected = dt.row( { selected: true } ).data();
                    $('#ModalAdd').modal('show');
                }
            }
            ]
        });
         $('#tb_list').DataTable({
            "language": {
                "lengthMenu": 'Tampilkan _MENU_ Baris',
                "search": '_INPUT_',
                "searchPlaceholder": 'Pencarian . . .'
            }            
        });
          $('#tb_blog').DataTable({
            "language": {
                "lengthMenu": 'Tampilkan _MENU_ Baris',
                "search": '_INPUT_',
                "searchPlaceholder": 'Pencarian . . .'
            }
        });
        $('#tb_kategori_blog').DataTable({
            "language": {
                "lengthMenu": 'Tampilkan _MENU_ Baris',
                "search": '_INPUT_',
                "searchPlaceholder": 'Pencarian . . .'
            },
            dom: 
            "<'row'<'col-md-8'l><'col-md-2 pull-right'f> <'col-md- pull-right' B>>" +
            "<'row'<'col-md-12't>>" +
            "<'row'<'col-md-5'i><'col-md-7'p>>",
            buttons: [
            {
                "text":'<i class="fa fa-plus"></i> TAMBAH KATEGORI',"className": 'btn btn-success btn-sm',
                action: function ( e, dt, node, config ) {
                    var selected = dt.row( { selected: true } ).data();
                    $('#ModalAdd').modal('show');
                }
            }
            ]
        });
        $('#tb_kategori').DataTable({
            "language": {
                "lengthMenu": 'Tampilkan _MENU_ Baris',
                "search": '_INPUT_',
                "searchPlaceholder": 'Pencarian . . .'
            }/*,
            dom: 
            "<'row'<'col-md-8'l><'col-md-2 pull-right'f> <'col-md- pull-right' B>>" +
            "<'row'<'col-md-12't>>" +
            "<'row'<'col-md-5'i><'col-md-7'p>>",
            buttons: [
            {
                "text":'<i class="fa fa-plus"></i> TAMBAH KATEGORI',"className": 'btn btn-success btn-sm',
                action: function ( e, dt, node, config ) {
                    var selected = dt.row( { selected: true } ).data();
                    $('#ModalAdd').modal('show');
                }
            }
            ]*/
        });
         $('#tb_produk').DataTable({
            "language": {
                "lengthMenu": 'Tampilkan _MENU_ Baris',
                "search": '_INPUT_',
                "searchPlaceholder": 'Pencarian . . .'
            },
            dom: 
            "<'row'<'col-md-8'l><'col-md-2 pull-right'f> <'col-md- pull-right' B>>" +
            "<'row'<'col-md-12't>>" +
            "<'row'<'col-md-5'i><'col-md-7'p>>",
            buttons: [
            {
                "text":'<i class="fa fa-plus"></i> TAMBAH PRODUK',"className": 'btn btn-success btn-sm',
                action: function ( e, dt, node, config ) {
                    var selected = dt.row( { selected: true } ).data();
                    $('#ModalAdd').modal('show');
                }
            }
            ]
        });

        $('#tb_katalog').DataTable({
            "language": {
                "lengthMenu": 'Tampilkan _MENU_ Baris',
                "search": '_INPUT_',
                "searchPlaceholder": 'Pencarian . . .'
            },
            dom: 
            "<'row'<'col-md-8'l><'col-md-2 pull-right'f> <'col-md- pull-right' B>>" +
            "<'row'<'col-md-12't>>" +
            "<'row'<'col-md-5'i><'col-md-7'p>>",
            buttons: [
            {
                "text":'<i class="fa fa-plus"></i> KATALOG',"className": 'btn btn-success btn-sm',
                action: function ( e, dt, node, config ) {
                    var selected = dt.row( { selected: true } ).data();
                    $('#ModalAdd').modal('show');
                }
            }
            ]
        });
        $('#tb_soal').DataTable({
            "language": {
                "lengthMenu": 'Tampilkan _MENU_ Baris',
                "search": '_INPUT_',
                "searchPlaceholder": 'Pencarian . . .'
            },
            dom: 
            "<'row'<'col-md-8'l><'col-md-2 pull-right'f>>" +
            "<'row'<'col-md-12't>>" +
            "<'row'<'col-md-5'i><'col-md-7'p>>"
        });
        $('#tb_pengguna').DataTable({
            "language": {
                "lengthMenu": 'Tampilkan _MENU_ Baris',
                "search": '_INPUT_',
                "searchPlaceholder": 'Pencarian . . .'
            },
            dom: 
            "<'row'<'col-md-8'l><'col-md-2 pull-right'f> <'col-md- pull-right' B>>" +
            "<'row'<'col-md-12't>>" +
            "<'row'<'col-md-5'i><'col-md-7'p>>",
            buttons: [
            {
                "text":'<i class="fa fa-users"></i> PENGGUNA',"className": 'btn btn-success btn-sm',
                action: function ( e, dt, node, config ) {
                    var selected = dt.row( { selected: true } ).data();
                    $('#ModalAdd').modal('show');
                }
            }
            ]
        });
        $('#tb_trainer').DataTable({
            "language": {
                "lengthMenu": 'Tampilkan _MENU_ Baris',
                "search": '_INPUT_',
                "searchPlaceholder": 'Pencarian . . .'
            },
            dom: 
            "<'row'<'col-md-8'l><'col-md-2 pull-right'f> <'col-md- pull-right' B>>" +
            "<'row'<'col-md-12't>>" +
            "<'row'<'col-md-5'i><'col-md-7'p>>",
            buttons: [
            {
                "text":'<i class="fa fa-users"></i> TRAINER',"className": 'btn btn-success btn-sm',
                action: function ( e, dt, node, config ) {
                    var selected = dt.row( { selected: true } ).data();
                    $('#ModalAdd').modal('show');
                }
            }
            ]
        });
        $('#tb_materi_peserta').DataTable({
            "language": {
                "lengthMenu": 'Tampilkan _MENU_ Baris',
                "search": '_INPUT_',
                "searchPlaceholder": 'Pencarian . . .'
            },
            dom: 
            "<'row'<'col-md-8'l><'col-md-2 pull-right'f>>" +
            "<'row'<'col-md-12't>>" +
            "<'row'<'col-md-5'i><'col-md-7'p>>"
           
        });
    });
TableManageButtons.init();

</script>
</body>
</html>

<!-- https://jsfiddle.net/gyrocode/snqw56dw/ -->