<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Teak garden furniture manufacture Indonesia with best quality on competitive prices. Feel safe buy furniture from us with our guarantee for all products.">
    <meta name="keywords" content="teak furniture, teak garden furniture, teak outdoor furniture, teak patio furniture, teak, furniture, garden, patio, indonesia teak outdoor furniture">
    <meta name="author" content="Wahyu">
    <link rel="icon" href="<?php echo base_url() ?>assets/frontend/img/icon.png" type="image/png">
    <title>Kusuma Furniture | Teak Furniture Manufacture and Wholesale Exporter</title>
    <!-- Jquery Ui -->
    <link href="<?php echo base_url() ?>assets/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/morris/morris.css">
    <!--Form Wizard-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/plugins/jquery.steps/css/jquery.steps.css" />
    <!-- App css -->
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/css/menu.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/switchery/switchery.min.css">

    <!-- DataTables -->
    <link href="<?php echo base_url() ?>assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() ?>assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() ?>assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() ?>assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() ?>assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() ?>assets/plugins/datatables/dataTables.colVis.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() ?>assets/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() ?>assets/plugins/datatables/fixedColumns.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/switchery/switchery.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">   
    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/css/dataTables.checkboxes.css" rel="stylesheet" />     
    <script src="<?php echo base_url() ?>assets/js/modernizr.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>

    <!--Summernote-->
      <!--<link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet">-->
      <!--<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>-->
      <!--<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>-->
      <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
      <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
      

</head>


<body class="fixed-left">

    <!-- Begin page -->
    <div id="wrapper">

        <!-- Top Bar Start -->
        <div class="topbar">

            <!-- LOGO -->
            <div class="topbar-left">
                <a href="index.html" class="logo"><span style="font-size:22px">Kusuma<span style="font-size:22px">Furniture</span></span><i></i></a>
                
                <!-- Image logo -->
                <a href="index.html" class="logo">
                    <span>
                        <img src="<?php echo base_url() ?>assets/images/logo/logo-putih.png" alt="" height="90">
                    </span>
                    <i>
                        <img src="<?php echo base_url() ?>assets/images/logo/logo-putih-sm.png" alt="" height="40">
                    </i>
                </a>
            </div>

            <!-- Button mobile view to collapse sidebar menu -->
            <div class="navbar navbar-default" role="navigation">
                <div class="container">

                    <!-- Navbar-left -->
                    <ul class="nav navbar-nav navbar-left">
                        <li>
                            <button class="button-menu-mobile open-left waves-effect">
                                <i class="mdi mdi-tune"></i>
                            </button>
                        </li>
                    </ul>
                    <!-- Right(Notification) -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- <?php
                        $tanggal = Date('Y-m-d');
                        $day = date('D', strtotime($tanggal));
                        $tgl = date('d', strtotime($tanggal));
                        $bulan = date('F', strtotime($tanggal));
                        $tahun = date('Y', strtotime($tanggal));
                        $dayList = array(
                            'Sun' => 'Minggu',
                            'Mon' => 'Senin',
                            'Tue' => 'Selasa',
                            'Wed' => 'Rabu',
                            'Thu' => 'Kamis',
                            'Fri' => 'Jumat',
                            'Sat' => 'Sabtu'
                        );
                        ?>
                        <li>
                            <button class="button-menu-mobile">
                                <i class="mdi mdi-calendar-clock"><?php echo " ".$dayList[$day].", ".$tgl." ".$bulan." ".$tahun;?></i>
                            </button>
                        </li> -->
                        <li class="dropdown user-box">
                            <a href="" class="dropdown-toggle waves-effect user-link" data-toggle="dropdown" aria-expanded="true">
                                <img src="<?php echo base_url() ?>assets/images/users/user-1.jpg" alt="user-img" class="img-circle user-img">
                            </a>

                            <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right user-list notify-list">
                                <li>
                                    <h5>Hi, <?php echo ucwords($this->session->userdata('username')) ?></h5>
                                </li>
                                    <!-- <li><a href="javascript:void(0)"><i class="ti-user m-r-5"></i> Profile</a></li>
                                    <li><a href="javascript:void(0)"><i class="ti-settings m-r-5"></i> Settings</a></li>
                                    <li><a href="javascript:void(0)"><i class="ti-lock m-r-5"></i> Lock screen</a></li> -->
                                    <li><a href="<?php echo base_url().'Login/logout'?>"><i class="ti-power-off m-r-5"></i> Logout</a></li>
                                </ul>
                            </li>

                        </ul> <!-- end navbar-right -->

                    </div><!-- end container -->
                </div><!-- end navbar -->
            </div>
            <!-- Top Bar End -->


            