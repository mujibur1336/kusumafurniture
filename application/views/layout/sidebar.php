<!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu" style="padding-top: 8%">
    <div class="sidebar-inner slimscrollleft">
        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <ul>
                <?php if ($this->session->userdata('level')=='1') { ?>
               <li>
                    <a href="<?php echo base_url() ?>dashboard" class="waves-effect"><i class="mdi mdi-view-dashboard"></i> <span> Dashboard </span></a>
                </li>
                
                <!-- <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class=" mdi mdi-account-box"></i><span> Pengguna </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo base_url() ?>pengguna">User</a></li>
                        <li><a href="<?php echo base_url() ?>trainer">Trainer</a></li>
                    </ul>
                </li> -->
                <li>
                    <a href="<?php echo base_url() ?>Produk" class="waves-effect"><i class="mdi mdi-format-list-numbers"></i><span> List Produk</span></a>
                </li>
                <li>
                    <a href="<?php echo base_url() ?>Katalog" class="waves-effect"><i class="glyphicon  glyphicon-tags"></i><span> Produk Katalog </span></a>
                </li>
                <li>
                    <a href="<?php echo base_url() ?>Produk/spesial" class="waves-effect"><i class="mdi mdi-bookmark-plus"></i><span> Spesial Offer</span></a>
                </li>
                <li>
                    <a href="<?php echo base_url() ?>Blog/kategori" class="waves-effect"><i class="mdi mdi-book-open-page-variant"></i><span> Artikel Kategori</span></a>
                </li>
                <li>
                    <a href="<?php echo base_url() ?>Blog/tampil" class="waves-effect"><i class="mdi mdi-book-open-page-variant"></i><span> Artikel</span></a>
                </li>
                  <li>
                    <a href="<?php echo base_url() ?>Blog/upload" class="waves-effect"><i class="mdi mdi-upload"></i><span> Gambar Artikel</span></a>
                </li>
                <li>
                    <a href="<?php echo base_url() ?>Pesan" class="waves-effect"><i class="mdi mdi-email"></i><span> Pesan Masuk</span></a>
                </li>
                <li>
                    <a href="<?php echo base_url() ?>Info" class="waves-effect"><i class="mdi mdi-information"></i><span> Profil Info </span></a>
                </li>
                <li>
                    <a href="<?php echo base_url() ?>Kategori" class="waves-effect"><i class="mdi mdi-settings"></i><span> Profil Kategori </span></a>
                </li>
                <li>
                    <a href="<?php echo base_url() ?>kategori/profil" class="waves-effect"><i class="glyphicon glyphicon-home"></i><span> Profil Perusahaan </span></a>
                </li>
                <li>
                    <a href="<?php echo base_url() ?>user" class="waves-effect"><i class="mdi mdi-account-settings"></i><span> Manajemen User </span></a>
                </li>

                <?php } else { ?>
                   <!-- <li>
                     <a href="<?php echo base_url() ?>dashboard_peserta" class="waves-effect"><i class="mdi mdi-view-dashboard"></i> <span> Dashboard </span></a>
                </li>
                <li>
                    <a href="<?php echo base_url() ?>kelas/kelas_materi" class="waves-effect"><i class="mdi mdi-library-books"></i> <span> Materi </span></a>
                </li>
                <li>
                    <a href="<?php echo base_url() ?>kelas/kelas_test" class="waves-effect"><i class="mdi mdi-library-books"></i> <span> Ujian </span></a>
                </li>
                <li>
                    <a href="<?php echo base_url() ?>kelas/kelas_nilai" class="waves-effect"><i class="mdi mdi-library-books"></i> <span> Nilai Kelas </span></a>
                </li>-->
                <!-- <li>
                    <a href="<?php echo base_url() ?>modul" class="waves-effect"><i class="mdi mdi-book-variant"></i><span> Daftar Modul </span></a>
                </li>
                <li>
                    <a href="<?php echo base_url() ?>soal" class="waves-effect"><i class="mdi mdi-book-open-page-variant"></i><span> Bank Soal </span> </a>
                </li>
                <li>
                    <a href="<?php echo base_url() ?>video" class="waves-effect"><i class="mdi mdi-message-video"></i><span> Database Video </span></a>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class=" mdi mdi-account-box"></i><span> Pengguna </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo base_url() ?>pengguna">User</a></li>
                        <li><a href="<?php echo base_url() ?>pengguna">Trainer</a></li>
                    </ul>
                </li> -->
            <?php } ?>
            </ul>
        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>
    </div>
    <!-- Sidebar -left -->
</div>
            <!-- Left Sidebar End