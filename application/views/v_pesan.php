            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
              <!-- Start content -->
              <div class="content">
                <div class="container">
                  <div class="row">
                    <div class="col-xs-12">

                     <div class="page-title-box">
                        <h4 class="page-title">DAFTAR PESAN MASUK </h4>
                        <ol class="breadcrumb p-0 m-0">
                          <li>
                            <a href="#">Dashboard </a>
                          </li>
                          <li class="active">
                            Daftar Pesan Masuk
                          </li>
                        </ol>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                  
        <!-- end row -->
        <div class="row">          
          <div class="col-sm-12">
              <!--<button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Reload Table</button>            -->
            <div class="card-box table-responsive">
              <table id="pesan" class="table table-striped dt-responsive">
                <thead>
                  <tr>

                    <th style="text-align: center; width: 1%">No</th>
                    <th style="text-align: center; width: 10%">Waktu</th>
                    <th style="text-align: center; width: 10%">Alamat IP</th>
                    <th style="text-align: center; width: 10%">Alamat</th>
                    <th style="text-align: center; width: 5%">Nama</th>
                    <th style="text-align: center; width: 5%">Email</th>
                    <th style="text-align: center; width: 20%">Judul</th>
                    <th style="text-align: center; width: 30%">Isi Pesan</th>
                    <th style="text-align: center; width: 1%">aksi</th>
                  </tr>
                </thead>
              <tbody>                                                                                                                                                                  
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div> <!-- container -->
  </div>
  </div> <!-- content -->

<!-- END wrapper -->
<script type="text/javascript">
var save_method; //for save method string
var table;
var base_url = '<?php echo base_url();?>';
    $(document).ready(function() {
    table = $('#pesan').DataTable({ 
 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('pesan/ajax_list')?>",
            "type": "POST"
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ]
    });
  
});

function delete_person(id)
{
    if(confirm('Are you sure delete this data?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo site_url('pesan/ajax_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                $('#modal_form').modal('hide');
                window.location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });
 
    }
}
</script>

