    <!-- Ec Contact Us page -->
    <section class="ec-page-content section-space-p" style="margin-top:90px;">
        <div class="container">
            <div class="row">
            <div class="col-md-12 section-title-block">
                <div class="section-title">
                    <h1 class="ec-title">Contact Us</h1>
                </div>
                <!-- <div class="section-btn">
                        <ul class="ec-pro-tab-nav nav">
                            <li class="nav-item"><a class="nav-link active" data-bs-toggle="tab"
                                    href="#tab-pro-new-arrivals">New Arrivals</a></li>
                            <li class="nav-item"><a class="nav-link" data-bs-toggle="tab"
                                    href="#tab-pro-special-offer">Special Offer</a></li>
                            <li class="nav-item"><a class="nav-link" data-bs-toggle="tab"
                                    href="#tab-pro-best-sellers">Best Sellers</a></li>
                        </ul>
                    </div> -->
            </div>
                <div class="ec-common-wrapper">
                    <div class="ec-contact-leftside">
                        <div class="ec_contact_map">
                            <div class="ec_map_canvas">
                                <iframe id="ec_map_canvas"
                                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d809.3820170082578!2d110.71189856984684!3d-6.658363867414685!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e70e00aa84d323d%3A0x279db0b73442deff!2sCv%20Kusuma%20Furniture!5e0!3m2!1sid!2sid!4v1591262168582!5m2!1sid!2sid"></iframe>
                                <!-- <a href="https://sites.google.com/view/maps-api-v2/mapv2"></a> -->
                            </div>
                        </div>
                        <div class="ec_contact_info">
                        <?php foreach ($data2 as $rowdata) {?>
                            <!-- <h1 class="ec_contact_info_head">Contact us</h1> -->
                            <ul class="align-items-center">
                                <li class="ec-contact-item"><i class="fi fi-rr-home" aria-hidden="true"></i>
                                <span><b><?php echo $rowdata->profil_nama ?></b> </br><?php echo $rowdata->profil_grade ?> </span>
                                </li>
                                <li class="ec-contact-item"><i class="fi fi-rs-diploma" aria-hidden="true"></i>
                                <span><b><?php echo $rowdata->profil_sertifikat ?></b> </br> Certificate </span>
                                </li>
                                <li class="ec-contact-item"><i class="fi fi-rs-box open" aria-hidden="true"></i>
                                <span><b><?php echo $rowdata->profil_packing ?></b></br> Packing System </span>
                                </li>
                                <li class="ec-contact-item"><i class="fi fi-rs-box open" aria-hidden="true"></i>
                                <span><b><?php echo $rowdata->profil_packing ?></b> </br>Packing System </span>
                                </li>
                                <li class="ec-contact-item"><i class="fi fi-rr-tree deciduous" aria-hidden="true"></i>
                                <span><b><?php echo $rowdata->profil_material ?></b> </br>Material  </span>
                                </li>
                                <li class="ec-contact-item align-items-center"><i class="ecicon eci-phone"
                                        aria-hidden="true"></i><span>Call Us :</span><a href="#" title="Teak Furniture">(62) 295 383411</a></li>
                                <li class="ec-contact-item align-items-center"><i class="ecicon eci-envelope"
                                        aria-hidden="true"></i><span>Email :</span><a
                                        href="mailto:kusuma.furniture@gmail.com" title="Teak Patio Furniture">kusuma.furniture@gmail.com</a></li>
                                <li class="ec-contact-item"><i class="fi fi-rr-hastag" aria-hidden="true"></i>
                                <span><b>Teak furniture , Teak garden furniture , Teak outdoor furniture , Teak patio furniture</b> </span>
                                </li>
                       
                            </ul>
                            <?php }?>
                        </div>
                    </div>
                    <div class="ec-contact-rightside">
                        <div class="ec-contact-container">
                            <div class="ec-contact-form">
                            <form class="row contact_form" action="<?php echo site_url('Home/input');?>" enctype='multipart/form-data' method="post" id="contactForm" onsubmit="return validateForm()">
                                    <span class="ec-contact-wrap">
                                        <label>Your Name*</label>
                                        <input type="text" id="pesan_nama" name="pesan_nama" placeholder="Enter your name" required />
                                    </span>
                                    <span class="ec-contact-wrap">
                                        <label>Email*</label>
                                        <input type="email" id="pesan_email" name="pesan_email" placeholder="Enter your email address"
                                            required />
                                    </span>
                                    <span class="ec-contact-wrap">
                                        <label>Subject*</label>
                                        <input type="text" id="pesan_subject" name="pesan_subject" placeholder="Enter your subject"
                                            required />
                                    </span>
                                    <span class="ec-contact-wrap">
                                        <label>Message*</label>
                                        <textarea name="pesan_isi" id="pesan_isi"
                                            placeholder="Enter Message"></textarea>
                                    </span>
                                    <span class="ec-contact-wrap">
                                    <label>Captcha</label></br>
                                    <?php echo $image; ?>
                                    <?php
                                    $captcha_session= $_SESSION['mycaptcha']; 
                                    // echo $captcha_session;
                                    ?><br> <br>
                                        <input type="text" class="form-control" name="security_code" id="security_code"  placeholder="Enter captcha"></p>
                                    </span>
                                    <span class="ec-contact-wrap ec-contact-btn">
                                        <button id="btnsimpan" name="btnsimpan" class="btn btn-primary" type="submit">Submit</button>
                                    </span>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
             function validateForm() {
                var session         = '<?php echo $captcha_session ?>';
                var pesan_nama      = document.getElementById("pesan_nama").value;
                var pesan_email     = document.getElementById("pesan_email").value;
                var pesan_subject   = document.getElementById("pesan_subject").value;
                var pesan_isi       = document.getElementById("pesan_isi").value;
                var captcha_input   = document.getElementById("security_code").value;
                if(pesan_nama != "" && pesan_email != "" && pesan_subject != "" && pesan_isi != ""){
                    if (session == captcha_input) {
                    document.getElementById("contactForm").submit();
                }else{
                    alert('Captcha is Wrong');
                    return false;
                }
                }else{
                    alert('Complete the form !');
                    return false;
                }        
            }
        </script>
    </section>
    <!--================Contact Success and Error message Area =================-->
<div id="success" class="modal modal-message fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-close"></i>
                </button>
                <p style="font-size: 12px; color:black;">Thank you</p>
                <p>Your message is successfully sent...</p>
            </div>
        </div>
    </div>
</div>

<!-- Modals error -->

<div id="error" class="modal modal-message fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-close"></i>
                </button>
                <p style="font-size: 12px; color:black;">Sorry !</p>
                <p> Something went wrong </p>
            </div>
        </div>
    </div>
</div>
<!--================End Contact Success and Error message Area =================-->
