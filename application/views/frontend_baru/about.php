<section class="ec-bnr-detail margin-bottom-5 section-space-pt" style="margin-top:90px; margin-bottom:5px;">
    <div class="ec-page-detail">
        <div class="container">
            <div class="section-title text-center" >
                <h1 class="ec-title">About Us</h1>
                <!-- <p class="sub-title">Check out our exclusive products.</p> -->
            </div>
            <br>
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="ec-cat-bnr">
                        <img src="<?php echo base_url() ?>assets/frontend_baru/images/logo/logo-kusuma-2.png" class="ec-footer-logo" alt="Teak Garden Patio Outdoor Furniture" />

                        <div class="ec-page-description">
                            <?php foreach ($data4 as $rowdata) { ?>
                                <h2 class="coupon-title"><?php echo $rowdata->info_judul ?></h2>
                                <p style="white-space: pre-wrap; word-wrap: break-word; align:justify" class="m-0"><?php echo $rowdata->info_isi ?></p>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="ec-cat-bnr">
                        <?php foreach ($data6 as $rowdata) { ?>
                            <img src="<?php echo base_url() ?>assets/upload_gambar/<?php echo $rowdata->info_img ?>" width=76% style="align:center" alt="Teak Garden Patio Outdoor Furniture" />

                            <div class="ec-page-description">

                                <h2 class="coupon-title"><?php echo $rowdata->info_judul ?></h2>
                                <p class="m-0"><?php echo $rowdata->info_isi ?></p>
                            <?php } ?>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Ec FAQ page -->
<section class="ec-page-content section-space-p">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title text-center">
                    <!-- <h2 class="ec-bg-title">FAQ</h2> -->
                    <p class="ec-title" style ="fontsize:20pt;">FAQ</p>
                    <p class="sub-title mb-3">Customer service management</p>
                </div>
            </div>
            <div class="ec-faq-wrapper">
                <div class="ec-faq-container">
                    <p class="ec-faq-heading">What is Kusuma services?</p>
                    <div id="ec-faq">
                        <?php foreach ($data5 as $rowdata) { ?>
                            <div class="col-sm-12 ec-faq-block">
                                <p class="ec-faq-title"><?php echo $rowdata->info_judul ?></p>
                                <div class="ec-faq-content">
                                    <p><?php echo $rowdata->info_isi ?>. </p>
                                </div>
                            </div>
                        <?php } ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<section class="ec-bnr-detail margin-bottom-30 section-space-pt">
    <div class="ec-page-detail">
        <div class="container">
            <div class="section-title text-center">
                <p class="ec-title">We Receive Payment By</p>
                <!-- <p class="sub-title">Check out our exclusive products.</p> -->
            </div>
            <br>
            <div class="row">
                <?php foreach ($data3 as $rowdata) {?>
                <div class="col-lg-4 col-md-12">
                    <div class="ec-cat-bnr">
                        <!-- <img src="<?php echo base_url() ?>assets/frontend_baru/images/about/ban-01-01.jpg" class="ec-footer-logo" alt="" /> -->

                        <div class="ec-page-description">
                       
                                <p><i class="fi fi-rs-receipt"></i> <?php echo $rowdata->info_judul ?></p>
                                <p style="white-space: pre-wrap; word-wrap: break-word; align:justify" class="m-0"><?php echo $rowdata->info_isi ?></p>
                           
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>