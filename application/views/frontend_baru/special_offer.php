<!-- Start Offer section -->
<section class="labels section-space-p" style="margin-top:90px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title">
                        <!-- <h2 class="ec-bg-title">Style 1</h2> -->
                        <h1 class="ec-title">Special Offer</h1>
                        <p class="sub-title">Special Offer for You!</p>
                        <br>
                    </div>
                </div>
            </div>
            <div class="row">
            <?php 
         foreach ($data as $rowdata) {              
            ?> 
                <div class="col-lg-4 col-md-6 col-sm-12 col-12 margin-b-30">
                    <div class="ec-offer-coupon">
                        <div class="ec-cpn-brand">
                            <img class="ec-brand-img" src="<?php echo base_url().'assets/upload_gambar/'.$rowdata->spesial_img ?>" alt="Teak Garden Patio Outdoor Furniture" />
                        </div>
                        <div class="ec-cpn-title">
                            <h2 class="coupon-title"><?php echo $rowdata->spesial_judul ?></h2>
                        </div>
                        <div class="ec-cpn-desc">
                            <p class="coupon-text" style="white-space: pre-wrap; word-wrap: break-word;"><?php echo $rowdata->spesial_isi ?></p>
                        </div>
                        <div class="ec-cpn-code">
                            <a href="<?php echo base_url() ?>contact" class="btn btn-secondary" tittle="Teak Furniture">| Contact Us</a>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
        
    </section>
    <!-- End Offer section -->