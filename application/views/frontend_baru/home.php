<!-- Main Slider Start -->
<div class="ec-main-slider section section-space-mb" style="margin-top: 100px;">
    <div class="ec-slider">
        <div class="ec-slide-item d-flex slide-1">
            <img src="<?php echo base_url() ?>assets/frontend_baru/images/main-slider-banner/banner1.webp" class="main_banner_arrow_img" alt="Teak Patio Furniture" title="Teak Patio Furniture"/>
            <div class="container align-self-center">
                <div class="row">
                    <div class="col-xl-6 col-lg-7 col-md-7 col-sm-7 align-self-center">
                        <div class="ec-slide-content slider-animation">
                            <br>
                            <h1 class="ec-title" style:"color:#006007;">TOP QUALITY INDONESIAN TEAK OUTDOOR FURNITURE</h1>
                            <h2 class="ec-slide-title">LUXURY AND RELIABILITY</h2>
                            <p class="ec-slide-disc" style="font-size: 12px; color:black;"><a href="https://www.kusumafurniture.com" title="Teak Furniture">Teak furniture, Teak garden furniture, Teak outdoor furniture, Teak patio furniture</a> </p>
                            <span class="ec-slide-disc" style="font-size: 12px; color:black;">High-end Indonesia teak furniture for your retail store, projects or contract business at wholesale price.</span>
                            <a href="<?php echo base_url() ?>special-offer" title="Teak Furniture" class="btn btn-lg btn-secondary">Special Offer</a>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>
<!-- Main Slider End -->

<!--  category Section Start -->
<section class="section ec-category-section section-space-p">
    <div class="container">
        <div class="row">
            <div class="col-md-12 section-title-block">
                <div class="section-title">
                    <h2 class="ec-title">MOST POPULAR FURNITURES</h2>
                    <p class="sub-title">Check out our furniture products.</p>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="ec_cat_slider">
                <?php
                foreach ($data_pop as $rowdata) {
                ?>
                    <div class="ec_cat_content">
                        <div class="ec_cat_inner">
                            <div class="ec-cat-image">
                                <a href="<?php echo base_url() . 'teak-furniture/product/' . $rowdata->katalog_seo ?>" title="Teak Outdoor Furniture">
                                    <img class="img-fluid" src="<?php echo base_url() . 'assets/upload_gambar/' . $rowdata->produk_img ?>" alt="Teak Outdoor Furniture" title="Teak Outdoor Furniture">
                                </a>
                            </div>
                            <div class="ec-cat-desc">
                                <span class="ec-section-btn"><a href="<?php echo base_url() . 'teak-furniture/product/' . $rowdata->katalog_seo ?>" title="Teak Patio Furniture" class="btn-primary"><?php echo $rowdata->katalog_nama ?></a></span>
                            </div>
                        </div>

                    </div>
                <?php } ?>
            </div>

        </div>

    </div>
</section>
<!--category Section End -->



<!--  offer Section Start -->
<section class="section ec-offer-section section-space-mt section-space-mb" style="padding:40px;">
    <!--<h2 class="d-none">Offer</h2>-->
    <!-- <img src="assets/images/offer-image/offer_bg.jpg" alt="Teak Garden Patio Outdoor Furniture" class="offer_bg" /> -->
    <div class="container">
        <div class="row">
            <div class="ec-offer-inner">
                <div class="col-sm-4 ec-offer-content">
                    <!--<h6 class="ec-offer-stitle">WELCOME TO</h6>-->
                    <p class="ec-offer-stitle" style="font-size:18px;">WELCOME TO</p>
                    <p class="ec-offer-title" style="font-size:30px;">KUSUMA FURNITURE INDONESIA</hp>
                    <span class="ec-offer-desc">All Modern Furnitures</span>
                    <p>"we confidence that our <a href="https://kusumafurniture.com" title="Teak Garden Furniture">teak furniture</a> can be competitive and recognized in world market and
                        becoming one of the best choice for <a href="https://kusumafurniture.com" title="Teak Furniture">teak furniture</a> products"
                    </p>
                    <!--<p> Teak furniture , Teak garden furniture , Teak outdoor furniture , Teak patio furniture , Teak furniture garden patio  </p>-->
                    <p style="color:#006007">Wahyu - Kusuma Furniture Owner - </p>
                    <!-- <div class="countdowntimer"><span id="ec-offer-count"></span></div> -->
                    <!-- <span class="ec-offer-btn"><a href="#" title="Teak Furniture" class="btn btn-lg btn-secondary">Shop Now</a></span> -->
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section ec-services-section section-space-p">
    <!--<h2 class="d-none">Services</h2>-->
    <div class="container">
        <div class="row mb-minus-30 text- justify">
            <div class="ec_ser_content ec_ser_content_1 col-sm-12 col-md-4">
                <div class="ec_ser_inner">
                    <div class="ec-service-image">
                        <i class="fi fi-rr-user"></i>
                    </div>
                    <div class="ec-service-desc">
                        <h2>BROAD SELECTION</h2>
                        <p>We believe that our ability to offer you broad range of <a href="https://www.kusumafurniture.com/teak-furniture/product/teak-set" title="Teak Furniture">teak garden furniture</a> choice are critical aspect to our success. We offer various <a href="https://kusumafurniture.com" title="Teak Garden Furniture">teak garden furniture</a> model thus permit you to choose suitable product base on your specific needs.</p>
                        <!--<p style="font-size: 11px;"> Teak furniture , Teak garden furniture , Teak outdoor furniture , Teak patio furniture , Teak furniture garden patio  </p>-->

                    </div>
                </div>
            </div>
            <div class="ec_ser_content ec_ser_content_2 col-sm-12 col-md-4">
                <div class="ec_ser_inner">
                    <div class="ec-service-image">
                        <i class="fi fi-rr-diploma"></i>
                    </div>
                    <div class="ec-service-desc">
                        <h2>GUARANTEE</h2>
                        <p>We are committed to making sure you are satistfied with our purchase. If you found problems with our <a href="https://www.kusumafurniture.com/teak-furniture/product/bench" title="Teak Furniture">teak garden furniture</a>, let we know and we will do our best to solve the problem. </p>
                        <!--<p style="font-size: 11px;"> Teak furniture , Teak garden furniture , Teak outdoor furniture , Teak patio furniture , Teak furniture garden patio  </p>-->

                    </div>
                </div>
            </div>
            <div class="ec_ser_content ec_ser_content_3 col-sm-12 col-md-4">
                <div class="ec_ser_inner">
                    <div class="ec-service-image">
                        <i class="fi fi-rr-badge-check"></i>
                    </div>
                    <div class="ec-service-desc">
                        <h2>GREAT QUALITY</h2>
                        <p >We make quality become one of our higest goals. Not only a broken products will cost us to replace but they are also give bad reputation for us, which is even more costly than the product replacement.</p>
                        <!--<p style="font-size: 11px;"> Teak furniture , Teak garden furniture , Teak outdoor furniture , Teak patio furniture , Teak furniture garden patio  </p>-->

                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>
    <div class="container">
        <div class="row mb-minus-30">
            <div class="ec_ser_content ec_ser_content_1 col-sm-12 col-md-4">
                <div class="ec_ser_inner">
                    <div class="ec-service-image">
                        <i class="fi fi-rr-rocket-lunch"></i>
                    </div>
                    <div class="ec-service-desc">
                        <h2>COMPETITIVE PRICE</h2>
                        <p>We offer you a very competitive price, without sacrificing quality for our <a href="https://www.kusumafurniture.com/teak-furniture/product/bench" title="Teak Outdoor Furniture">teak garden furniture</a>. Special offer for high order and longterm customers. Our lower cost production makes it possible to operate for less than other manufacturers.</p>
                        <!--<p style="font-size: 11px;"> Teak furniture , Teak garden furniture , Teak outdoor furniture , Teak patio furniture , Teak furniture garden patio  </p>-->

                    </div>
                </div>
            </div>
            <div class="ec_ser_content ec_ser_content_2 col-sm-12 col-md-4">
                <div class="ec_ser_inner">
                    <div class="ec-service-image">
                        <i class="fi fi-rr-social-network"></i>
                    </div>
                    <div class="ec-service-desc">
                        <h2>HIGHLY RECOMENDED</h2>
                        <p>Our products are made from the highest quality of <a href="https://www.kusumafurniture.com/teak-furniture/product/accesories" title="Teak Patio Furniture">teak woods furniture</a>. We took the materials from Indonesia Forestry Company which famous for the quality of the <a href="https://kusumafurniture.com/teak-furniture/product/accesories" title="Teak Furniture">teak wood furniture</a>. We made perfect furniture product by employing high skilled craftsmen and using modern machines.</p>
                        <!--<p>Our products are made from the highest quality of <a href="https://www.kusumafurniture.com" title="Teak Patio Furniture">teak woods furniture</a>. We took the materials from Indonesia Forestry Company which famous for the quality of the <a href="https://kusumafurniture.com" title="Teak Furniture">teak wood furniture</a>. We made perfect furniture product by employing high skilled craftsmen and using modern machines.</p>-->
                        <!--<p style="font-size: 11px;"> Teak furniture , Teak garden furniture , Teak outdoor furniture , Teak patio furniture , Teak furniture garden patio  </p>-->

                    </div>
                </div>
            </div>
            <div class="ec_ser_content ec_ser_content_3 col-sm-12 col-md-4">
                <div class="ec_ser_inner">
                    <div class="ec-service-image">
                        <i class="fi fi-rr-comment-heart"></i>
                    </div>
                    <div class="ec-service-desc">
                        <h2>POSITIVE REVIEWS</h2>
                        <p>Our products are reaching through in many countries since they have well quality and price. At the moment, our products are reaching through Europe, America, Mediterranean and Asia.</p>
                        <!--<p style="font-size: 11px;"> Teak furniture , Teak garden furniture , Teak outdoor furniture , Teak patio furniture , Teak furniture garden patio  </p>-->

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--services Section End -->

<!-- ec testimonial Start -->
<section class="section ec-test-section section-space-ptb-100 section-space-mt section-space-mb">
    <div class="container">
        <div class="row">
            <div class="col-md-12 section-title-block">
                <div class="section-title">
                    <!--<h2 class="ec-title">News</h2>-->
                    <p class="ec-title" style="font-size: 22px">News</p>
                    <p class="sub-title">Update news!</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="ec-test-outer">
               <ul id="ec-testimonial-slider">
                    <?php
                foreach ($data as $rowdata) {
                ?>    
                        <li class="ec-test-item">
                            <div class="ec-test-inner">
                                <div class="ec-test-img"><a href="#" title="Teak Furniture">
                                    <img class="blog-image" src="<?php echo base_url() . 'assets/upload_gambar/' . $rowdata->blog_img ?>" alt="Teak Furniture" title="Teak Furniture" style="object-fit: fill;" /></a>
                                </div>
                                <div class="ec-test-content">
                                    <div class="ec-test-icon"><i class="fi-rr-quote-right"></i></div>
                                    <small><?php echo $rowdata->blog_tgl ?></small>
                                    <div class="ec-test-desc"><h3 class="ec-blog-title"><a href="#" title="Teak Furniture"><?php echo $rowdata->blog_judul ?></a></h3>
                                <div class="ec-test-desc"><?php echo word_limiter($rowdata->blog_isi, 25) ?>.</div>
                                  <div class="ec-blog-btn"><a href="<?php echo base_url() . 'article/furniture/' . $rowdata->blog_seo ?>" title="Teak Garden Furniture" class="btn btn-primary">To see all of our news, click Here</a></div>
                                </div>
                            </div>
                        </li>
                    <?php } ?>    
                    </ul>
                
            </div>
        </div>
    </div>
</section>
<!-- ec testimonial end -->

<!-- Ec Instagram Start -->
<section class="section ec-instagram-section section-space-p">
    <div class="container">
        <div class="row">
            <div class="col-md-12 section-title-block">
                <div class="section-title">
                    <!--<h2 class="ec-title">Instagram <span>#Thestore</span></h2>-->
                    <p class="ec-title" style="font-size: 20px">Instagram <span>#Thestore</span></p>
                    <p class="sub-title">Add Stories on instagram and tell us how impact our product in your life.
                    </p>
                </div>
                <div class="section-btn">
                    <span class="ec-section-btn"><a class="btn-secondary" href="https://www.instagram.com/kusuma.furniture/" title="Teak Furniture">Follow Us</a></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="ec-insta-wrapper">
                <div class="ec-insta-outer">
                    <div class="insta-auto">
                        <!-- instagram item -->
                        <?php
                        foreach ($data_pop as $rowdata) {
                        ?>
                            <div class="ec-insta-item">
                                <div class="ec-insta-inner">
                                    <a href="https://www.instagram.com/kusuma.furniture/" target="_blank" rel="noreferrer" title="Teak Furniture">
                                        <img class="img-fluid" src="<?php echo base_url() . 'assets/upload_gambar/' . $rowdata->produk_img ?>" alt="Teak Garden Furniture" title="Teak Garden Furniture">


                                    </a>
                                </div>
                            </div>
                        <?php } ?>
                        <!-- instagram item -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Ec Instagram End -->