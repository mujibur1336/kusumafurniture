<section class="ec-page-content section-space-p" style="margin-top:50px;">
        <div class="container">
            <div class="row">
                <div class="ec-blogs-rightside col-lg-8 col-md-12">
                    <!-- Blog content Start -->
                    <div class="ec-blogs-content">
                        <div class="ec-blogs-inner">
                            <?php 
                            foreach ($data as $rowdata) {              
                            ?> 
                                <div class="ec-blog-main-img">
                                    <img class="blog-image" src="<?php echo base_url() . 'assets/upload_gambar/' . $rowdata->blog_img ?>" alt="Teak Garden Patio Outdoor Furniture" />
                                </div>
                                <div class="ec-blog-date">
                                    <p class="date"><?php echo $rowdata->blog_tgl ?> </p>
                                </div>
                                <div class="ec-blog-detail">
                                    <h1 class="ec-blog-title"><?php echo $rowdata->blog_judul ?></h1>
                                    <p><?php echo $rowdata->blog_isi ?></p>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <!--Blog content End -->
                </div>
                <!-- Sidebar Area Start -->
                <div class="ec-blogs-leftside col-lg-4 col-md-12">
                 <div class="ec-sidebar-wrap">
                     <!-- Sidebar Recent Blog Block -->
                     <div class="ec-sidebar-block ec-sidebar-recent-blog">
                         <div class="ec-sb-title ">
                             <h3 class=" btn btn-primary" style="color: #fff;">Recent Articles</h3>
                         </div>
                         <?php
                            foreach ($data3 as $rowdata) {
                            ?>
                             <div class="ec-sb-block-content">
                                 <div class="ec-sidebar-block-item">
                                     <?php if ($rowdata->blog_img != null) { ?>
                                         <img src="<?php echo base_url() . 'assets/upload_gambar/' . $rowdata->blog_img ?>" width="25%" alt="Teak Garden Patio Outdoor Furniture">
                                     <?php } else { ?>

                                     <?php } ?>
                                     <h5 class="ec-blog-title"><a href="<?php echo base_url() . 'article/furniture/' . $rowdata->blog_seo ?>" title="Teak Furniture"><?php echo $rowdata->blog_judul ?></a></h5>
                                     <div class="ec-blog-date"><?php echo $rowdata->blog_tgl ?></div>
                                 </div>

                             </div>
                         <?php } ?>
                     </div>

                     <!-- Sidebar Recent Blog Block -->

                 </div>
             </div>
            </div>
        </div>
    </section>