<section class="section ec-product-tab section-space-p" style="margin-top:90px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 section-title-block">
                <div class="section-title">
                  
                    <?php foreach($katalog as $key){
                        $katalog_nama = $key->katalog_nama;
                        $katalog_deskripsi = $key->katalog_quote;
                    } ?>
                    <h1 class="ec-title"><?php echo $katalog_nama; ?></h1>
                    
                </div>
                <!-- <div class="section-btn">
                        <ul class="ec-pro-tab-nav nav">
                            <li class="nav-item"><a class="nav-link active" data-bs-toggle="tab"
                                    href="#tab-pro-new-arrivals">New Arrivals</a></li>
                            <li class="nav-item"><a class="nav-link" data-bs-toggle="tab"
                                    href="#tab-pro-special-offer">Special Offer</a></li>
                            <li class="nav-item"><a class="nav-link" data-bs-toggle="tab"
                                    href="#tab-pro-best-sellers">Best Sellers</a></li>
                        </ul>
                    </div> -->
            </div>

        </div>
        <div class="row">
            <div class="col">
                <div class="tab-content">
                    <!-- 1st Product tab start -->
                    <div class="tab-pane fade show active" id="tab-pro-new-arrivals">
                        <div class="row">

                            <div class="ec-pro-tab-slider">
                                <?php
                                foreach ($data as $rowdata) {
                                ?>
                                    <div class="col-lg-4 col-md-8 col-sm-8 col-xs-8 ec-product-content">
                                        <div class="ec-product-inner">
                                            <div class="ec-pro-image-outer">
                                                <div class="ec-pro-image">
                                                    <a href="#" title="Teak Furniture" class="image">
                                                        <img class="main-image" src="<?php echo base_url() . 'assets/upload_gambar/' . $rowdata->produk_img ?>" alt="Teak Furniture" title="Teak Furniture" />
                                                        <img class="hover-image" src="<?php echo base_url() . 'assets/upload_gambar/' . $rowdata->produk_img ?>" alt="Teak Furniture" title="Teak Furniture" />
                                                    </a>
                                                    <div class="ec-pro-actions">
                                                        <a href="#" title="Teak Furniture" class="ec-btn-group quickview" data-link-action="quickview" title="Quick view" data-bs-toggle="modal" data-bs-target="#ec_quickview_modal<?php echo $rowdata->produk_id; ?>"><i class="fi-rr-eye"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ec-pro-content" style="text-align: center;">
                                                <h6 class="ec-pro-title" style="font-size: 12px;">(<?php echo $rowdata->produk_kode ?>)</h6>
                                                <h6 class="ec-quickview-desc" style="font-size: 15px; color:#006007;"><b><?php echo $rowdata->produk_nama ?></b></h6>
                                                <h6 class="ec-pro-title" style="font-size: 12px;"><?php echo $rowdata->produk_size ?></h6>
                                                <div class="col-md-12">
                                                <a href="<?php echo base_url() ?>contact" title="Teak Garden Furniture" class="btn btn-sm btn-secondary"><i class="fa fa-envelope-o" style="font-size: 10px;"></i> | Contact Us</a>
                                                <a href="#" title="Teak Outdoor Furniture" class="btn btn-sm btn-primary" data-link-action="quickview" title="Quick view" data-bs-toggle="modal" data-bs-target="#ec_quickview_modal<?php echo $rowdata->produk_id; ?>"><i class="fi fi-rr-zoom-in"></i></a>
                                                </div>
                                                <!-- <button data-toggle="modal" data-target="#modal<?php echo $rowdata->produk_id; ?>"class="btn btn-sm btn-info"><i class="fa fa-search-plus"></i></button> -->
                                                <!-- <span class="ec-price">
                                                    <span class="old-price">$1549</span>
                                                    <span class="new-price">$1400</span>
                                                </span> -->
                                            </div>
                                        </div>
                                    </div>

                                <?php } ?>
                            </div>


                        </div>
                    </div>
                    <!-- ec 1st Product tab end -->
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <p class="sub-title"><?php echo $katalog_deskripsi; ?></p>
        </div>
    </div>
    <!-- Modal -->
    <?php
    foreach ($data as $rowdata) {
    ?>
        <div class="modal fade" id="ec_quickview_modal<?php echo $rowdata->produk_id; ?>" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <button type="button" class="btn-close qty_close" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-md-12 col-sm-8 col-xs-8" style="text-align: center;">
                                <!-- Swiper -->
                                <div class="qty-product-cover">
                                    <div class="qty-slide">
                                        <img class="img-responsive" src="<?php echo base_url() . 'assets/upload_gambar/' . $rowdata->produk_img ?>" alt="Teak Garden Patio Outdoor Furniture">
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <!-- Modal end -->
</section>