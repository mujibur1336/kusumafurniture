<!-- Ec Blog page -->
<section class="section ec-test-section section-space-ptb-100 section-space-mt section-space-mb">
    <div class="container">
        <div class="row">
            <div class="col-md-12 section-title-block">
                <div class="section-title">
                    <!--<h2 class="ec-title">News</h2>-->
                    <h1 class="ec-title">News</h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="ec-test-outer">
               <ul id="ec-testimonial-slider">
                    <?php
                foreach ($data as $rowdata) {
                ?>    
                        <li class="ec-test-item">
                            <div class="ec-test-inner">
                                <div class="ec-test-img"><a href="<?php echo base_url() . 'article/furniture/' . $rowdata->blog_seo ?>" title="Teak Furniture">
                                    <img class="blog-image" src="<?php echo base_url() . 'assets/upload_gambar/' . $rowdata->blog_img ?>" alt="Teak Furniture" title="Teak Furniture" style="object-fit: fill;" /></a>
                                </div>
                                <div class="ec-test-content">
                                    <div class="ec-test-icon"><i class="fi-rr-quote-right"></i></div>
                                    <small><?php echo $rowdata->blog_tgl ?></small>
                                    <div class="ec-test-desc"><h2 class="ec-blog-title"><a href="<?php echo base_url() . 'article/furniture/' . $rowdata->blog_seo ?>" title="Teak Furniture"><?php echo $rowdata->blog_judul ?></a></h2>
                                    <div class="ec-test-desc"><?php echo word_limiter($rowdata->blog_isi, 25) ?></div>
                                  <div class="ec-blog-btn"><a href="<?php echo base_url() . 'article/furniture/' . $rowdata->blog_seo ?>" title="Teak Garden Furniture" class="btn btn-primary">Continue reading…</a></div>
                                </div>
                            </div>
                        </li>
                    <?php } ?>    
                    </ul>
                
            </div>
        </div>
    </div>
</section>
