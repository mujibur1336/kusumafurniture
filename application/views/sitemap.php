<?php
  header('Content-type: application/xml; charset="ISO-8859-1"',true);  
  $datetime1 = new DateTime(date('Y-m-d H:i:s'));
?>
 
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
  <url>
    <loc><?= base_url() ?></loc>
    <lastmod><?= $datetime1->format(DATE_ATOM); ?></lastmod>
    <changefreq>daily</changefreq>
    <priority>0.1</priority>
  </url>
  
      <url>
      <loc>https://www.kusumafurniture.com/about</loc>
      <lastmod>2024-02-27T02:16:15+00:00</lastmod>
      <priority>0.1</priority>
    </url>
    <url>
      <loc>https://www.kusumafurniture.com/article/page</loc>
      <lastmod>2024-02-27T02:16:15+00:00</lastmod>
      <priority>0.1</priority>
    </url>
    <url>
      <loc>https://www.kusumafurniture.com/special-offer</loc>
      <lastmod>2024-02-27T02:16:15+00:00</lastmod>
      <priority>0.1</priority>
    </url>
    
    <url>
      <loc>https://www.kusumafurniture.com/contact</loc>
      <lastmod>2024-02-27T02:16:15+00:00</lastmod>
      <priority>0.1</priority>
    </url>
    
   <?php foreach($produk as $item2) { ?>
    <url>
        <loc><?= base_url().'teak-furniture/product/'.($item2['katalog_seo']) ?></loc>
        <lastmod>2021-02-27T02:16:15+00:00</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
    </url>
    <?php } ?>


   <?php foreach($post as $item) { $datetime = new DateTime($item['blog_tgl']);?>
    <url>
        <loc><?= base_url().'article/furniture/'.($item['blog_seo']) ?></loc>
        <lastmod><?= $datetime->format(DATE_ATOM); ?></lastmod>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
    </url>
    <?php } ?>
    
</urlset>