            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">DAFTAR Profil Info</h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Dashboard </a>
                                        </li>
                                        <li class="active">
                                            Daftar Profil Info
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <form method="post" action="<?php echo site_url('Info/input');?>" enctype='multipart/form-data'>
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel">TAMBAH PROFIL</h4>
                              </div>
                              <div class="modal-body">
                                <div class="col-lg-4">
                                    <label for="txtname">Nama Profil</label>
                                </div>
                                <div class="col-lg-8" style="margin-bottom: 10px">
                                    <input type="text" class="form-control" id="info_judul" name="info_judul" placeholder="Masukkan Nama Profil">
                                    <input type="hidden" class="form-control" id="info_status" name="info_status" value="1">
                                </div>
                                <div class="col-lg-4">
                                    <label for="txtname">Isi Profil</label>
                                </div>
                                <div class="col-lg-8" style="margin-bottom: 10px">
                                    <!--<input type="text" class="form-control" id="info_isi" name="info_isi" placeholder="Masukkan Isi Profil">-->
                                    <textarea class="form-control" id="info_isi" name="info_isi" rows="4"></textarea>
                                </div>
                                <div class="col-lg-4">
                                    <label for="txtname">Kategori</label>
                                </div>
                                <div class="col-lg-8" style="margin-bottom: 10px">
                                    <select class="form-control" name="kategori_id">
                                    <option>-- PILIH --</option>
                                      <?php foreach ($kategori as $rowdata) { ?>                                        
                                        <option value="<?php echo $rowdata->kategori_id; ?>"><?php echo $rowdata->kategori_id; ?>| <?php echo $rowdata->kategori_nama; ?></option>
                                      <?php } ?>
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <label for="txtname">File</label>
                                </div>
                                <div class="col-lg-8" style="margin-bottom: 10px">
                                    <input type="file" class="form-control" name="userfile" >
                                </div>
                         </div>
                         <div class="modal-footer" style="margin-top: 32%">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="submit" id="btnsimpan" name="btnsimpan" class="btn btn-success">Save</button>
                      </div>
                  </form>
              </div>
          </div>
      </div>
      <!-- end row -->
      <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <table id="tb_info" class="table table-striped table-bordered dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th style="text-align: center; width: 1%">No</th>
                            <th style="text-align: center; width: 10%">Nama</th>                                                    
                            <th style="text-align: center; width: 10%">Isi</th>
                            <th style="text-align: center; width: 10%">Gambar</th>
                            <th style="text-align: center; width: 10%">Profil Kategori</th>
                            <th style="text-align: center; width: 10%">Status</th>
                            <th style="text-align: center; width: 5%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 0 ;
                        foreach ($data as $rowdata) {   
                            $no++;              
                            ?> 
                            <tr>
                                <td style="text-align: center;"><?php echo $no ?></td>
                                <td style="white-space: pre-wrap; word-wrap: break-word;"><?php echo $rowdata->info_judul ?></td>
                                <td style="white-space: pre-wrap; word-wrap: break-word;"><?php echo $rowdata->info_isi ?></td>
                                 <td><img src="<?php echo base_url();?>assets/upload_gambar/<?php echo $rowdata->info_img;?>" width=40% high=40% alt="Kusuma Furniture">                                                                
                              </td>
                              <td><?php echo $rowdata->kategori_nama; ?></td>
                                <td><?php if ($rowdata->info_status == 1) {?>
                                  Non Aktif
                                <?php }else{?>
                                  Aktif
                                  <?php } ?></td>
                                <td align="center">
                                    <span class="btn btn-info btn-sm" style="font-family: serif;" data-toggle="modal" data-target="#ModalEdit<?php echo $rowdata->info_id; ?>"><i class="fa fa-pencil">| Edit</i></span>
                              <?php if ($rowdata->info_status==1) { ?>                                
                              <button type="button" class="btn btn-primary btn-sm " data-toggle="modal" data-target="#ModalStatus<?php echo $rowdata->info_id; ?>"><span class="glyphicon glyphicon-check" aria-hidden="true"></span> | Aktif</button>
                                <?php } else { ?>                                            
                                <button type="button" class="btn btn-danger btn-sm " data-toggle="modal" data-target="#ModalStatus2<?php echo $rowdata->info_id; ?>"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> | Non Aktif</button>
                                <?php } ?> 
                                    <!-- <button type="button" class="btn btn-danger btn-sm " data-toggle="modal" data-target="#ModalDelete<?php echo $rowdata->kategori_id; ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button> -->
                                </td>
                                <!-- Modal Delete-->
                                <!-- <div class="modal fade" id="ModalDelete<?php echo $rowdata->user_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                      <div class="modal-content">
                                        <form method="post" action="<?php echo site_url('user/delete');?>">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel" style="font-family: sans-serif;">Konfirmasi Hapus Data</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                              <input type="hidden" id="user_id" name="user_id" value="<?php echo $rowdata->user_id; ?>">
                                              <label style="font-family: sans-serif; font-size: 16px;">Apakah anda yakin, akan menghapus data : <?php echo "'<u>".$rowdata->username."</u>'";?> ?</label>
                                          </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                        <button type="submit" id="btndelete" name="btndelete" class="btn btn-danger">Hapus</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div> -->
                     <!-- Modal Status-->
                              <div class="modal fade" id="ModalStatus<?php echo $rowdata->info_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <form method="post" action="<?php echo site_url('info/status_info');?>">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel" style="font-family: sans-serif;">Konfirmasi Aktif</h4>
                                      </div>
                                      <div class="modal-body">
                                        <div class="form-group">
                                          <input type="hidden" id="info_id" name="info_id" value="<?php echo $rowdata->info_id; ?>">
                                          <label style="font-family: sans-serif; font-size: 16px;">Apakah anda yakin, akan mempublish : <?php echo "'<u>".$rowdata->info_judul."</u>'";?> ?</label>
                                        </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                        <button type="submit" id="btnstatus" name="btnstatus" class="btn btn-primary">Publish</button>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                              <!-- Modal Status Cancel-->
                              <div class="modal fade" id="ModalStatus2<?php echo $rowdata->info_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <form method="post" action="<?php echo site_url('Info/status_info2');?>">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel" style="font-family: sans-serif;">Konfirmasi Non Aktif</h4>
                                      </div>
                                      <div class="modal-body">
                                        <div class="form-group">
                                          <input type="hidden" id="info_id" name="info_id" value="<?php echo $rowdata->info_id; ?>">
                                          <label style="font-family: sans-serif; font-size: 16px;">Apakah anda yakin, akan membatalkan info : <?php echo "'<u>".$rowdata->info_judul."</u>'";?> ?</label>
                                        </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                        <button type="submit" id="btnstatus" name="btnstatus" class="btn btn-warning">Ya</button>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                    <!-- Modal Edit-->
                    <div class="modal fade" id="ModalEdit<?php echo $rowdata->info_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <form method="post" action="<?php echo site_url('Info/edit');?>" enctype='multipart/form-data'>
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Edit Profil</h4>
                              </div>
                              <div class="modal-body">
                            <div class="col-lg-4">
                                    <label for="txtname">Nama Profil</label>
                                </div>
                                <div class="col-lg-8" style="margin-bottom: 10px">
                                    <input type="text" class="form-control" id="info_judul" name="info_judul" value="<?php echo $rowdata->info_judul; ?>">
                                    <input type="hidden" class="form-control" id="info_id" name="info_id" value="<?php echo $rowdata->info_id; ?>">
                                </div>
                                <div class="col-lg-4">
                                    <label for="txtname">Isi Profil</label>
                                </div>
                                <div class="col-lg-8" style="margin-bottom: 10px">
                                    <!--<input type="text" class="form-control" id="info_isi" name="info_isi" value="<?php echo $rowdata->info_isi; ?>">-->
                                    <textarea class="form-control" id="info_isi" name="info_isi" rows="4"><?php echo $rowdata->info_isi; ?></textarea>
                                </div>
                                <div class="col-lg-4">
                                    <label for="txtname">Kategori</label>
                                </div>
                                <div class="col-lg-8" style="margin-bottom: 10px">
                                    <select class="form-control" name="kategori_id">
                                    <option><?php echo $rowdata->kategori_id; ?>| <?php echo $rowdata->kategori_nama; ?></option>
                                      <?php foreach ($kategori as $rowdata) { ?>                                        
                                        <option value="<?php echo $rowdata->kategori_id; ?>"><?php echo $rowdata->kategori_id; ?>| <?php echo $rowdata->kategori_nama; ?></option>
                                      <?php } ?>
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <label for="txtname">File</label>
                                </div>
                                <div class="col-lg-8" style="margin-bottom: 10px">
                                    <input type="file" class="form-control" name="userfile" >
                                </div>                           
                                <br><br>
                       </div>
                       <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                        <button type="submit" id="btnedit" name="btnedit" class="btn btn-success">Simpan</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
</div>
</div>
</div> <!-- container -->

</div> <!-- content -->

</div>

</div>
<!-- END wrapper -->
