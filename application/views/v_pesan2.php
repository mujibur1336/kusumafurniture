            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
              <!-- Start content -->
              <div class="content">
                <div class="container">
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="page-title-box">
                        <h4 class="page-title">DAFTAR PESAN MASUK </h4>
                        <ol class="breadcrumb p-0 m-0">
                          <li>
                            <a href="#">Dashboard </a>
                          </li>
                          <li class="active">
                            Daftar Pesan Masuk
                          </li>
                        </ol>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                <!-- end row -->
                <div class="row">
                  <div class="col-sm-12">
                    <div class="card-box table-responsive">
                      <table id="tb_list" class="table table-striped table-bordered dt-responsive nowrap">
                        <thead>
                          <tr>
                            <th style="text-align: center; width: 1%">No</th>
                            <th style="text-align: center; width: 10%">Waktu</th>
                            <th style="text-align: center; width: 10%">Alamat IP</th>
                            <th style="text-align: center; width: 10%">Alamat</th>
                            <th style="text-align: center; width: 10%">Nama</th>
                            <th style="text-align: center; width: 10%">Email</th>
                            <th style="text-align: center; width: 20%">Judul</th>
                            <th style="text-align: center; width: 30%">Isi Pesan</th>
                            <th style="text-align: center; width: 1%">aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          $no = 0 ;
                          foreach ($data as $rowdata) {   
                            $no++;              
                            ?> 
                            <tr>
                              <td style="text-align: center;"><?php echo $no ?></td>
                              <td style="white-space: pre-wrap; word-wrap: break-word;"><?php echo $rowdata->pesan_waktu ?></td>
                              <td style="white-space: pre-wrap; word-wrap: break-word;"><?php echo $rowdata->pesan_ip ?></td>
                              <td style="white-space: pre-wrap; word-wrap: break-word;"><?php echo $rowdata->pesan_alamat ?></td>
                              <td style="white-space: pre-wrap; word-wrap: break-word;"><?php echo $rowdata->pesan_nama ?></td>
                              <td style="white-space: pre-wrap; word-wrap: break-word;"><?php echo $rowdata->pesan_email ?></td>
                              <td style="white-space: pre-wrap; word-wrap: break-word;"><?php echo $rowdata->pesan_subject ?></td>
                              <td style="white-space: pre-wrap; word-wrap: break-word;"><?php echo $rowdata->pesan_isi ?></td>
                               <td><button type="button" class="btn btn-danger btn-sm " data-toggle="modal" data-target="#ModalDelete<?php echo $rowdata->pesan_id; ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button></td>
                                  <!-- Modal Delete-->
                              <div class="modal fade" id="ModalDelete<?php echo $rowdata->pesan_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <form method="post" action="<?php echo site_url('Pesan/delete');?>">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel" style="font-family: sans-serif;">Konfirmasi Hapus Data</h4>
                                      </div>
                                      <div class="modal-body">
                                        <div class="form-group">
                                          <input type="hidden" id="pesan_id" name="pesan_id" value="<?php echo $rowdata->pesan_id; ?>">
                                          <label style="font-family: sans-serif; font-size: 16px;">Apakah anda yakin, akan menghapus pesan : <?php echo "'<u>".$rowdata->pesan_subject."</u>'";?> ?</label>
                                        </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                        <button type="submit" id="btndelete" name="btndelete" class="btn btn-danger">Hapus</button>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                              </tr>     
                            <?php } ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div> <!-- container -->
              </div> <!-- content -->

            </div>

          </div>
          <!-- END wrapper -->
          <script type="text/javascript">
            $(document).ready(function(){
              $('#btnsimpan').click(function() {
                $('#spinner').show();
              });
            });
          </script>
