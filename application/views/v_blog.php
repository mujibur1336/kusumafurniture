            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
              <!-- Start content -->
              <div class="content">
                <div class="container">
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="page-title-box">
                        <h4 class="page-title">DAFTAR ARTIKEL </h4>
                        <ol class="breadcrumb p-0 m-0">
                          <li>
                            <a href="#">Dashboard </a>
                          </li>
                          <li class="active">
                            Daftar Artikel
                          </li>
                        </ol>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                  <div class="modal fade" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-xl" role="document">
                      <div class="modal-content">
                        <form method="post" action="<?php echo site_url('blog/input');?>" enctype='multipart/form-data'>
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">TAMBAH Artikel</h4>
                          </div>
                          <div class="modal-body modal-xl">
                           <!-- <?php date_default_timezone_set('Asia/Jakarta');
                           $date = new DateTime('now');?>
                           <input type="hidden" name="tgl_upload" class="form-control" value="<?php echo $date->format('Y-m-d H:i:s') ?>"> -->
                           <div class="col-lg-12">
                            <label for="txtname">Judul Artikel</label>
                          </div>                          
                          <div class="col-lg-12" style="margin-bottom: 10px">
                            <input type="text" class="form-control" id="blog_judul" name="blog_judul" placeholder="Masukkan Judul Artikel" required>
                            <input type="hidden" class="form-control" id="blog_seo" name="blog_seo">
                            <input type="hidden" class="form-control" id="blog_status" name="blog_status" value="1">
                          </div>
                          <div class="col-lg-12">
                            <label for="txtname">Artikel Kategori</label>
                          </div>
                          <div class="col-lg-12" style="margin-bottom: 10px">
                                <select class="form-control" name="blog_kategori_id">
                                  <option>-- Pilih --</option>
                                  <?php foreach ($kategori as $rowdata) {?>
                                    <option value="<?php echo $rowdata->blog_kategori_id; ?>"><?php echo $rowdata->blog_kategori_id; ?>| <?php echo $rowdata->blog_kategori_nama; ?></option>
                                  <?php } ?>
                                </select>                             
                          </div>                                                  
                        <div class="col-lg-12">
                            <label for="txtname">ISI Artikel</label>
                          </div>
                          
                          <div class="col-lg-12" style="margin-bottom: 10px">
                                <textarea id="elm1" name="blog_isi"></textarea>
                          </div>
                      <div id="spinner" class="col-lg-8 spinner" style="display:none; margin-bottom: 15px">
                        <img id="img-spinner" src="<?php echo base_url() ?>assets/images/loading.gif" alt="Loading" height='15px' width='345px'/>
                      </div>
                    </div>
                    <div class="modal-footer" style="margin-top: 50%">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button type="submit" id="btnsimpan" name="btnsimpan" class="btn btn-success">Save</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
                <!-- end row -->
                <div class="row">
                  <div class="col-sm-12">
                    <div class="card-box table-responsive">
                    <a href="<?php echo base_url() ?>Blog/v_input" class="btn btn-success btn-sm"><i class="glyphicon glyphicon-plus"></i> | TAMBAH ARTIKEL</a>
                    <br>
                    <br>
                      <table id="tb_blog" class="table table-striped table-bordered dt-responsive nowrap">
                        <thead>
                          <tr>
                            <th style="text-align: center; width: 1%">No</th>
                            <th style="text-align: center; width: 5%">Gambar blog</th> 
                            <th style="text-align: center; width: 10%">Judul Artikel</th>
                            <!-- <th style="text-align: center; width: 10%">Isi Artikel</th> -->
                            <th style="text-align: center; width: 5%">Kategori Artikel</th>
                            <!--<th style="text-align: center; width: 10%">Status Artikel</th>  -->                          
                            <th style="text-align: center; width: 5%">Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          $no = 0 ;
                          foreach ($data as $rowdata) {   
                            $no++;              
                            ?> 
                            <tr>
                              <td style="text-align: center;"><?php echo $no ?></td>
                             <td><img src="<?php echo base_url();?>assets/upload_gambar/<?php echo $rowdata->blog_img;?>" width=25% high=25% alt="Kusuma Furniture"></td> 
                              <td style="white-space: pre-wrap; word-wrap: break-word;"><?php echo $rowdata->blog_judul ?></td>                              
                              <!-- <td style="white-space: pre-wrap; word-wrap: break-word;"><?php echo $rowdata->blog_isi ?></td> -->
                              <td><?php echo $rowdata->blog_kategori_nama ?></td>
                            <!--  <td> 
                                <?php if ($rowdata->blog_status==1) { ?>
                                DRAFT
                                <?php } else { ?>
                                PUBLISH
                                <?php } ?> 
                              </td>     -->                         
                              <td align="center">
                              <!--<?php if ($rowdata->blog_status==1) { ?>                                
                              <button type="button" class="btn btn-primary btn-sm " data-toggle="modal" data-target="#ModalStatus<?php echo $rowdata->blog_id; ?>"><span class="glyphicon glyphicon-check" aria-hidden="true"></span> | Publish</button>
                                <?php } else { ?>                                            
                                <button type="button" class="btn btn-warning btn-sm " data-toggle="modal" data-target="#ModalStatus2<?php echo $rowdata->blog_id; ?>"><span class="glyphicon glyphicon-check" aria-hidden="true"></span> | Cancel</button>
                                <?php } ?> -->
                                <!--<button type="button" class="btn btn-info btn-sm " data-toggle="modal" data-target="#ModalIsi<?php echo $rowdata->blog_id; ?>"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></button>-->

                                <a href="<?php echo base_url() ?>Blog/v_edit/<?php echo $rowdata->blog_id; ?>"><span class="btn btn-warning btn-sm" style="font-family: serif;"><i class="fa fa-pencil"></i></span></i></a>
                                                              
                                <button type="button" class="btn btn-danger btn-sm " data-toggle="modal" data-target="#ModalDelete<?php echo $rowdata->blog_id; ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
                              </td>
                            <!-- Modal Status-->
                              <div class="modal fade" id="ModalStatus<?php echo $rowdata->blog_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <form method="post" action="<?php echo site_url('Blog/status_blog');?>">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel" style="font-family: sans-serif;">Konfirmasi Publis Artikel</h4>
                                      </div>
                                      <div class="modal-body">
                                        <div class="form-group">
                                          <input type="hidden" id="blog_id" name="blog_id" value="<?php echo $rowdata->blog_id; ?>">
                                          <label style="font-family: sans-serif; font-size: 16px;">Apakah anda yakin, akan mempublish : <?php echo "'<u>".$rowdata->blog_judul."</u>'";?> ?</label>
                                        </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                        <button type="submit" id="btnstatus" name="btnstatus" class="btn btn-primary">Publish</button>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                              <!-- Modal Status Cancel-->
                              <div class="modal fade" id="ModalStatus2<?php echo $rowdata->blog_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <form method="post" action="<?php echo site_url('Blog/status_blog2');?>">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel" style="font-family: sans-serif;">Konfirmasi Publis Artikel</h4>
                                      </div>
                                      <div class="modal-body">
                                        <div class="form-group">
                                          <input type="hidden" id="blog_id" name="blog_id" value="<?php echo $rowdata->blog_id; ?>">
                                          <label style="font-family: sans-serif; font-size: 16px;">Apakah anda yakin, akan membatalkan artikel : <?php echo "'<u>".$rowdata->blog_judul."</u>'";?> ?</label>
                                        </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                        <button type="submit" id="btnstatus" name="btnstatus" class="btn btn-warning">Ya</button>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                              <!-- Modal Delete-->
                              <div class="modal fade" id="ModalDelete<?php echo $rowdata->blog_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <form method="post" action="<?php echo site_url('blog/delete');?>">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel" style="font-family: sans-serif;">Konfirmasi Hapus Data</h4>
                                      </div>
                                      <div class="modal-body">
                                        <div class="form-group">
                                          <input type="hidden" id="blog_id" name="blog_id" value="<?php echo $rowdata->blog_id; ?>">
                                          <label style="font-family: sans-serif; font-size: 16px;">Apakah anda yakin, akan menghapus data : <?php echo "'<u>".$rowdata->blog_judul."</u>'";?> ?</label>
                                        </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                        <button type="submit" id="btndelete" name="btndelete" class="btn btn-danger">Hapus</button>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                              <!-- Modal Lihat Isi-->
                              <!--<div class="modal fade" id="ModalIsi<?php echo $rowdata->blog_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">-->
                              <!--  <div class="modal-dialog modal-lg" role="document">-->
                              <!--    <div class="modal-content">-->
                              <!--      <form method="post" action="<?php echo site_url('blog/delete');?>">-->
                              <!--        <div class="modal-header">-->
                              <!--          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                              <!--          <label class="modal-title" id="myModalLabel" style="font-family: sans-serif; font-size: 16px;"><?php echo $rowdata->blog_judul ?></label>-->
                              <!--        </div>-->
                              <!--        <div class="modal-body">-->
                              <!--          <div class="form-group">-->
                              <!--            <input type="hidden" id="blog_id" name="blog_id" value="<?php echo $rowdata->blog_id; ?>">-->
                              <!--            <h5 style="font-family: sans-serif; font-size: 12px;"> <?php echo $rowdata->blog_isi ?></h5>-->
                              <!--          </div>-->
                              <!--        </div>-->
                              <!--        <div class="modal-footer">-->
                              <!--          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                                        <!--<button type="submit" id="btndelete" name="btndelete" class="btn btn-danger">Hapus</button>-->
                              <!--        </div>-->
                              <!--      </form>-->
                              <!--    </div>-->
                              <!--  </div>-->
                              <!--</div>-->
                              <!-- Modal EDIT-->
                              <div class="modal fade" id="ModalEdit<?php echo $rowdata->blog_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <form method="post" action="<?php echo site_url('blog/edit');?>" enctype='multipart/form-data'>
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">EDIT DATA ARTIKEL</h4>
                                      </div>
                                      <div class="modal-body">                                       
                                       <input type="hidden" id="blog_id" name="blog_id" value="<?php echo $rowdata->blog_id; ?>">
                                       <div class="col-lg-12">
                                       <label for="txtname">Judul Artikel</label>
                                      </div>
                                      <div class="col-lg-12" style="margin-bottom: 10px">
                                        <input type="text" class="form-control" id="blog_judul" name="blog_judul" value="<?php echo $rowdata->blog_judul; ?>">
                                        <input type="hidden" class="form-control" id="blog_seo" name="blog_seo" value="<?php echo $rowdata->blog_seo; ?>">                                        
                                      </div>
                                      <div class="col-lg-12">
                                        <label for="txtname">Artike Kategori</label>
                                      </div>
                                      <div class="col-lg-12" style="margin-bottom: 10px">
                                        <select class="form-control" name="blog_kategori_id">
                                          <option><?php echo $rowdata->blog_kategori_nama; ?></option>
                                          <?php foreach ($kategori as $data) {?>
                                            <option value="<?php echo $data->blog_kategori_id; ?>"><?php echo $data->blog_kategori_id; ?>| <?php echo $data->blog_kategori_nama; ?></option>
                                          <?php } ?>
                                          
                                        </select>
                                        </div>
                                      <div class="col-lg-12">
                                        <label for="txtname">Isi Artikel</label>
                                      </div>
                                      <div class="col-lg-12" style="margin-bottom: 10px">
                                        <textarea id="elm1" name="blog_isi"><?php echo $rowdata->blog_isi; ?></textarea>
                                      </div>
                                      <!-- <div class="col-lg-4">
                                        <label for="txtname">File Gambar</label>
                                      </div>
                                      <div class="col-lg-8" style="margin-bottom: 10px">
                                        <input type="file" class="form-control" name="userfile" value="">
                                      </div>                           -->
                                      <div class="modal-footer" style="margin-top: 95%">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="submit" id="btnedit" name="btnedit" class="btn btn-success">Simpan</button>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>                                                        
                            </tr>
                            <?php } ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div> <!-- container -->
              </div> <!-- content -->

            </div>

          </div>
          <!-- END wrapper -->
          <script type="text/javascript">
            $(document).ready(function(){
              $('#btnsimpan').click(function() {
                $('#spinner').show();
              });
            });
          </script>

        <script>
          $(document).ready(function () {
          if($("#elm1").length > 0){
              tinymce.init({
                  selector: "textarea#elm1",
                  theme: "modern",
                  height:300,
                  plugins: [
                      "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                      "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                      "save table contextmenu directionality emoticons template paste textcolor"
                  ],
                  toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
                  style_formats: [
                      {title: 'Bold text', inline: 'b'},
                      {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                      {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                      {title: 'Example 1', inline: 'span', classes: 'example1'},
                      {title: 'Example 2', inline: 'span', classes: 'example2'},
                      {title: 'Table styles'},
                      {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
                  ]
              });
          }
      });
        </script>


