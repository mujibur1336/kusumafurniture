            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">DAFTAR PENGGUNA </h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Dashboard </a>
                                        </li>
                                        <li class="active">
                                            Daftar Pengguna
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <form method="post" action="<?php echo site_url('user/input');?>" enctype='multipart/form-data'>
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel">TAMBAH PENGGUNA</h4>
                              </div>
                              <div class="modal-body">
                                <div class="col-lg-4">
                                    <label for="txtname">Nama Lengkap</label>
                                </div>
                                <div class="col-lg-8" style="margin-bottom: 10px">
                                    <input type="text" class="form-control" id="user_nama" name="user_nama" placeholder="Masukkan Nama Lengkap">
                                </div>
                                <div class="col-lg-4">
                                    <label for="txtname">Username</label>
                                </div>
                                <div class="col-lg-8" style="margin-bottom: 10px">
                                    <input type="text" class="form-control" id="username" name="username" placeholder="Masukkan Username">
                                </div>                                
                                <div class="col-lg-4">
                                    <label for="txtname">Password</label>
                                </div>
                                <div class="col-lg-8" style="margin-bottom: 10px">
                                    <input type="password" class="form-control" id="password" name="password" placeholder="Masukkan Password">
                                    <input type="hidden" class="form-control" id="level" name="level" value="1">
                                </div>                 
                         </div>
                         <div class="modal-footer" style="margin-top: 22%">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="submit" id="btnsimpan" name="btnsimpan" class="btn btn-success">Save</button>
                      </div>
                  </form>
              </div>
          </div>
      </div>
      <!-- end row -->
      <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <table id="tb_pengguna" class="table table-striped table-bordered dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th style="text-align: center; width: 1%">No</th>
                            <th style="text-align: center; width: 20%">Nama Lengkap</th>
                            <th style="text-align: center; width: 10%">Username</th>                            
                            <th style="text-align: center; width: 5%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 0 ;
                        foreach ($data as $rowdata) {   
                            $no++;              
                            ?> 
                            <tr>
                                <td style="text-align: center;"><?php echo $no ?></td>
                                <td><?php echo $rowdata->user_nama ?></td>
                                <td><?php echo $rowdata->username ?></td>                                
                                <td align="center">
                                    <span class="btn btn-warning btn-sm" style="font-family: serif;" data-toggle="modal" data-target="#ModalEdit<?php echo $rowdata->user_id; ?>"><i class="fa fa-pencil"></i></span>
                                    <button type="button" class="btn btn-danger btn-sm " data-toggle="modal" data-target="#ModalDelete<?php echo $rowdata->user_id; ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
                                </td>
                                <!-- Modal Delete-->
                                <div class="modal fade" id="ModalDelete<?php echo $rowdata->user_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                      <div class="modal-content">
                                        <form method="post" action="<?php echo site_url('user/delete');?>">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel" style="font-family: sans-serif;">Konfirmasi Hapus Data</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                              <input type="hidden" id="user_id" name="user_id" value="<?php echo $rowdata->user_id; ?>">
                                              <label style="font-family: sans-serif; font-size: 16px;">Apakah anda yakin, akan menghapus data : <?php echo "'<u>".$rowdata->username."</u>'";?> ?</label>
                                          </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                        <button type="submit" id="btndelete" name="btndelete" class="btn btn-danger">Hapus</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- Modal Edit-->
                    <div class="modal fade" id="ModalEdit<?php echo $rowdata->user_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <form method="post" action="<?php echo site_url('user/edit');?>" enctype='multipart/form-data'>
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Edit Pengguna</h4>
                              </div>
                              <div class="modal-body">
                                 <div class="col-lg-12">
                               <div class="col-lg-4" align="left">
                                <label for="txtemail">Nama Lengkap</label>
                              </div>
                              <div class="col-lg-8">
                                <input type="text" class="form-control" id="user_nama" name="user_nama" required="required" value="<?php echo $rowdata->user_nama; ?>">                                
                              </div>
                            </div>
                            <br><br><br>
                                <div class="col-lg-12">
                                  <div class="col-lg-4" align="left">
                                    <label for="txtname">Username</label>
                                  </div>
                                  <div class="col-lg-8">                            
                                    <input type="hidden" id="user_id" name="user_id" value="<?php echo $rowdata->user_id; ?>">
                                    <input type="text" class="form-control" id="username" name="username" required="required" value="<?php echo $rowdata->username; ?>">
                                  </div>
                                </div>
                                <br><br><br>
                                <div class="col-lg-12">
                                  <div class="col-lg-4" align="left">
                                    <label for="txtname">Password</label>
                                  </div>
                                  <div class="col-lg-8">                            
                                    <input type="password" class="form-control" id="password" name="password" placeholder="Masukkan Password" required>
                                    <input type="hidden" class="form-control" id="level" name="level" value="1">
                                  </div>
                                </div>
                                <br><br>
                       </div>
                       <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                        <button type="submit" id="btnedit" name="btnedit" class="btn btn-success">Simpan</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
</div>
</div>
</div> <!-- container -->

</div> <!-- content -->

</div>

</div>
<!-- END wrapper -->
