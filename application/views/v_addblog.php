            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
              <!-- Start content -->
              <div class="content">
                <div class="container">
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="page-title-box">
                        <h4>TAMBAH ARTIKEL</h4>
                        <ol class="breadcrumb p-0 m-0">
                          <li>
                            <a href="#">Dashboard </a>
                          </li>
                          <li class="active">
                            Tambah Artikel
                          </li>
                        </ol>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                      <div class="modal-content">
                        <form method="post" action="<?php echo site_url('blog/input');?>" enctype='multipart/form-data'>
                          <div class="modal-body modal-xl">
                           <!-- <?php date_default_timezone_set('Asia/Jakarta');
                           $date = new DateTime('now');?>
                           <input type="hidden" name="tgl_upload" class="form-control" value="<?php echo $date->format('Y-m-d H:i:s') ?>"> -->
                          <div class="col-lg-12">
                            <label for="txtname">Judul Artikel</label>
                          </div>                          
                          <div class="col-lg-12" style="margin-bottom: 10px">
                            <input type="text" class="form-control" id="blog_judul" name="blog_judul" placeholder="Masukkan Judul Artikel" required>
                            <input type="hidden" class="form-control" id="blog_seo" name="blog_seo">
                            <input type="hidden" class="form-control" id="blog_status" name="blog_status" value="1">
                          </div>
                          <div class="col-lg-12">
                             <label for="txtname">Meta Deskripsi Artikel</label>
                          </div>
                          <div class="col-lg-12" style="margin-bottom: 10px">
                                <textarea class="form-control" id="blog_meta" name="blog_meta" col="2"></textarea>
                          </div>  
                         <div class="col-lg-12">
                            <label for="txtname">Artikel Kategori</label>
                          </div>
                          <div class="col-lg-12" style="margin-bottom: 10px">
                                <select class="form-control" name="blog_kategori_id">
                                  <option>-- Pilih --</option>
                                  <?php foreach ($kategori as $rowdata) {?>
                                    <option value="<?php echo $rowdata->blog_kategori_id; ?>"> <?php echo $rowdata->blog_kategori_nama; ?></option>
                                  <?php } ?>
                                </select>                             
                          </div>
                           <div class="col-lg-12">
                            <label for="txtname">File Gambar</label>
                          </div>                          
                          <div class="col-lg-12" style="margin-bottom: 10px">
                            <input type="file" class="form-control" name="userfile" value="">
                          </div> 
                        <div class="col-lg-12">
                            <label for="txtname">ISI Artikel</label>
                          </div>
                          <div class="col-lg-12" style="margin-bottom: 10px">
                                <textarea id="summernote" name="blog_isi"></textarea>
                          </div>                  
                          
                    </div>
                    <div class="modal-footer" style="margin-top: 50%">
                      <a href="<?php echo base_url() ?>Blog" class="btn btn-default">Kembali</a>
                      <button type="submit" id="btnsimpan" name="btnsimpan" class="btn btn-success">Save</button>
                    </div>
                  </form>
                </div>
                
                <script>
            $(document).ready(function() {
                $('#summernote').summernote({
                    height: 400
                });
            });
          </script>
   <script>
            $(document).ready(function () {
                if($("#elm1").length > 0){
                    tinymce.init({
                        selector: "textarea#elm1",
                        theme: "modern",
                        height:300,
                        plugins: [
                            "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                            "save table contextmenu directionality emoticons template paste textcolor"
                        ],
                        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
                        style_formats: [
                            {title: 'Bold text', inline: 'b'},
                            {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                            {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                            {title: 'Example 1', inline: 'span', classes: 'example1'},
                            {title: 'Example 2', inline: 'span', classes: 'example2'},
                            {title: 'Table styles'},
                            {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
                        ]
                    });
                }
            });
        </script>

