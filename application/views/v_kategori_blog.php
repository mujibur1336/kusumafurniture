            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">DAFTAR ARTIKEL Kategori</h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Dashboard </a>
                                        </li>
                                        <li class="active">
                                            Daftar Artikel Kategori
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <form method="post" action="<?php echo site_url('blog/inputkategori');?>" enctype='multipart/form-data'>
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel">Tambah Artikel Kategori</h4>
                              </div>
                              <div class="modal-body">
                                <div class="col-lg-4">
                                    <label for="txtname">Nama Artikel Kategori</label>
                                </div>
                                <div class="col-lg-8" style="margin-bottom: 10px">
                                    <input type="text" class="form-control" id="blog_kategori_nama" name="blog_kategori_nama" placeholder="Masukkan Nama Kategori">
                                    <input type="hidden" class="form-control" id="blog_kategori_seo" name="blog_kategori_seo">
                                </div>                                           
                         </div>
                         <div class="modal-footer" style="margin-top: 12%">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="submit" id="btnsimpan" name="btnsimpan" class="btn btn-success">Save</button>
                      </div>
                  </form>
              </div>
          </div>
      </div>
      <!-- end row -->
      <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <table id="tb_kategori_blog" class="table table-striped table-bordered dt-responsive nowrap">
                      <?php echo  $this->session->flashdata('pesan'); ?>                    
                    <thead>
                        <tr>
                            <th style="text-align: center; width: 1%">No</th>
                            <th style="text-align: center; width: 20%">Nama Artikel Kategori</th>                                                    
                            <th style="text-align: center; width: 5%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 0 ;
                        foreach ($data as $rowdata) {   
                            $no++;              
                            ?> 
                            <tr>
                                <td style="text-align: center;"><?php echo $no ?></td>
                                <td><?php echo $rowdata->blog_kategori_nama ?></td>
                                <td align="center">
                                    <span class="btn btn-warning btn-sm" style="font-family: serif;" data-toggle="modal" data-target="#ModalEdit<?php echo $rowdata->blog_kategori_id; ?>"><i class="fa fa-pencil"></i></span>
                                    <!-- <button type="button" class="btn btn-danger btn-sm " data-toggle="modal" data-target="#ModalDelete<?php echo $rowdata->kategori_id; ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button> -->
                                </td>
                                <!-- Modal Delete-->
                                <!-- <div class="modal fade" id="ModalDelete<?php echo $rowdata->user_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                      <div class="modal-content">
                                        <form method="post" action="<?php echo site_url('user/delete');?>">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel" style="font-family: sans-serif;">Konfirmasi Hapus Data</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                              <input type="hidden" id="user_id" name="user_id" value="<?php echo $rowdata->user_id; ?>">
                                              <label style="font-family: sans-serif; font-size: 16px;">Apakah anda yakin, akan menghapus data : <?php echo "'<u>".$rowdata->username."</u>'";?> ?</label>
                                          </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                        <button type="submit" id="btndelete" name="btndelete" class="btn btn-danger">Hapus</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div> -->
                    <!-- Modal Edit-->
                    <div class="modal fade" id="ModalEdit<?php echo $rowdata->blog_kategori_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <form method="post" action="<?php echo site_url('blog/editkategori');?>" enctype='multipart/form-data'>
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Edit Kategori</h4>
                              </div>
                              <div class="modal-body">
                                 <div class="col-lg-12">
                               <div class="col-lg-4" align="left">
                                <label for="txtemail">Nama Artikel Kategori</label>
                              </div>
                              <div class="col-lg-8">
                                <input type="text" class="form-control" id="blog_kategori_nama" name="blog_kategori_nama" required="required" value="<?php echo $rowdata->blog_kategori_nama; ?>">                                
                                <input type="hidden" class="form-control" id="blog_kategori_seo" name="blog_kategori_seo">
                                <input type="hidden" class="form-control" id="blog_kategori_id" name="blog_kategori_id" value="<?php echo $rowdata->blog_kategori_id; ?>">
                              </div>
                            </div>                            
                                <br><br>
                       </div>
                       <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                        <button type="submit" id="btnedit" name="btnedit" class="btn btn-success">Simpan</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
</div>
</div>
</div> <!-- container -->

</div> <!-- content -->

</div>

</div>
<!-- END wrapper -->
